# WebUI_057 - "dtapi-tester"

* Install Gulp and plugins
```
    $ npm install
```
* Install AngularJS, Bootstrap and others
```
    $ bower install
```
*  Build project and start Gulp watcher
```
    $ gulp

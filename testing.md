#Before 
For the testing we need the latest version of NodeJS on the virtual machine
Install the latest version of NodeJS:
```
	# apt-get install curl
	# curl -sL https://deb.nodesource.com/setup_0.12 | sudo -E bash -
	# apt-get install -y nodejs
```
Install bower and gulp:
```
	# npm install -g bower
	# npm install -g gulp
```

Install karma command line interface:
```
	# npm install -g karma-cli
```

# Deployment procedure and basic installation
For the testing we need to deploy these directories and files on the virtual 
machine:
```
	\dist
	\src
	\test
	
	bower.json
	gulpfile.js
	karma.conf.js
	package.json
```

In the project directory we need to install plugins and tools:
```
	$ npm install
	$ bower install
```
Build the project
```
	$ gulp build
```

# Testing process
Writing tests and save them into:
```
	\test\unit
```
Build the project
Start testing
```
	$ gulp unit-test
	or
	$ karma start
```

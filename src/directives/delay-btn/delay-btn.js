angular.module('delayBtn')
	.directive('delayBtn', ['$timeout', '$interval', '$filter', '$rootScope',
		function($timeout, $interval, $filter, $rootScope){
		return {
			restrict: 'A',
			scope: {
				onClick: '&'
			},
			link: function(scope, element, attrs) {
				var confirmTranslate = attrs.confirmTxt || 'ssTest.controls.indeed';
				var getTranslate = function(text){
					return $filter('translate')(text);
				};
				$rootScope.$on('$translateChangeSuccess', function(){
					confirmTxt.text(getTranslate(confirmTranslate));
				});
				var progress = angular.element('<div class="click-progress-bar"></div>');
				var txtCont = angular.element('<div class="btn-text-container"></div>');
				var confirmTxt = angular.element('<div class="confirm-text">' + getTranslate(confirmTranslate) + '</div>');
				var txt = angular.element('<div class="btn-text"></div>');
				var animatingClass = 'animating';
				var delay = parseInt(attrs.delayBtn);

				var draw = function (timePassed) {
					progress.css({
						width: 100 * timePassed / delay + '%'
					});
				};

				var secondClick = function(){
					scope.onClick();
					reset();
				};

				var setSecondClick = function(){
					element.on('click', secondClick);
				};

				var reset = function(){
					$interval.cancel(timer);
					timer = null;
					element.removeClass(animatingClass).off('click', secondClick);
					progress.css({
						width: 0
					});
				};

				var timer;

				txt.append(element.contents());
				txtCont.append(txt);
				txtCont.append(confirmTxt);
				element.append(progress);
				element.append(txtCont);

				element
					.addClass('delay-btn')
					.on('click', function(e){
						if (timer) return;
						e.preventDefault();
						element.addClass(animatingClass);
						var start = Date.now();
						timer = $interval(function() {
							var timePassed = Date.now() - start;
							if (timePassed >= delay) {
								reset();
								return;
							}
							draw(timePassed);
						}, 20);
						setSecondClick();
					});

			}

		};
	}]);
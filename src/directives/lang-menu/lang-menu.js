angular
	.module('ssTest')
	.directive('langMenu', ['$translate', '$rootScope', function($translate, $rootScope) {
		return {
			restrict: 'EA',
			templateUrl: 'directives/lang-menu/lang-menu.html',
			scope: {},
			controller: function($scope) {
				$scope.langs = ['uk', 'en', 'de', 'by', 'it','ch'];
				$scope.changeLanguage = function (langKey) {
					$translate.use(langKey);
					$scope.currentLang = langKey;
					$rootScope.$broadcast('localeChange', $scope.currentLang);
				};
				$scope.currentLang = $translate.use();
			}
		};
	}]);
angular.module('gridster')
	.controller('widgetCalculatorInstallCtrl', ['$scope', 'data', '$modalInstance', function($scope, data, $modalInstance){
		var update = !!data.widgetData.data;
		$scope.formState = {};

		$scope.widget = data.widgetData;


		$scope.send = function(){
			$scope.formState.formSubmited = true;
		};
		$scope.cancel = function(){
			$modalInstance.close();
		};

		$scope.save = function(){
			data.onDone($scope.widget);
			$modalInstance.close();
		};
	}]);

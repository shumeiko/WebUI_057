angular
	.module('admin')
	.directive('tableVersions', [function() {
		return {
			restrict: 'EA',
			templateUrl: 'directives/table-versions/table-versions.html',
			scope: {
				cols: '='
			},
			controller: function($scope) {
				$scope.print =  function() {
					window.print();
				};

				$scope.toPdf = function() {
					if ($scope.cols.length === 1) {
						$scope.createPdfTable($scope.cols[0]);
					} else {
						$scope.createPdfTable($scope.cols[0], $scope.cols[1]);
					}
				};
			},
			link: function($scope) {
				$scope.createPdfTable = function(titles, additions) {
					var table = document.querySelector('.general-list-table');
					var docDefinition = {};
					var header = [];
					var body = [];
					var width = ['auto'];
					var cols;
					var title;
					var cell;
					var reg1 = /(?:<.*?\n?>)|\n/g; //regex for replacing tags and line breaks
					var reg2 = /(^\s*)|(\s*$)/g; //regexp for replacing spaces at the begin and end

					cols = additions ? titles + additions : titles;

					for (var i = 0; i <= cols; i++) {
						cell = table.rows[0].cells[i].innerHTML.replace(reg1, '').replace(reg2, '');
						title = {text: cell, style: 'header'};
						header.push(title);
					}

					body.push(header);

					for (i = 1; i < table.rows.length; i++) {
						var tableRow = [];

						for (var j = 0; j <= cols; j++) {
							cell = table.rows[i].cells[j].innerHTML.replace(reg1, '')
								.replace(reg2, '');
							tableRow.push(cell);
						}

						body.push(tableRow);
					}

					for (var k = 0; k < titles; k++) {
						width.push('*');
					}

					if (additions) {
						for (var l = 0; l < additions; l++) {
							width.push('auto');
						}
					}

					docDefinition.content = [{
						table: {
							headerRows: 1,
							widths: width,
							body: body
						}
					}];

					docDefinition.styles = {
						header: {
							alignment: 'center',
							bold: true
						}
					};

					if (document.documentMode) {
						pdfMake.createPdf(docDefinition).download('table.pdf');
					} else {
						pdfMake.createPdf(docDefinition).open();
					}
				}
			}
		};
	}]);
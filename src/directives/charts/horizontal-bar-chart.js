angular.module('charts')
	.directive('horizontalBarChart', ['$window', '$timeout', function($window, $timeout) {
		return {
			restrict: 'EA',
			require: '^?gridsterBox',
			scope: {
				data: '='
			},
			link: function(scope, element) {
				var d3 = $window.d3;
				var placeholder = d3.select(element[0]);
				var chartArea;
				var area;
				var sample;
				var item;
				var legend;
				var containerBar;
				var bar;
				var title;
				var rateNumber;
				var triangle;
				var initData = scope.data;
				var widthLegend;
				var sortOrder = true;

				function init(data, parentData) {
					if (parentData) {
						initData = parentData;
					} else {
						initData = scope.data;
					}

					chartArea = placeholder
						.append('div')
						.attr('class', 'horizontal-bar-chart');

					title = chartArea.append('div')
						.attr('class', 'title')
						.style('display', function() {
							if (!data.options.title) {
								return 'none';
							}
						});

					title.append('a')
						.style('display', function() {
							if (!data.options.backButton) {
								return 'none'
							}
						})
						.append('span')
						.attr('class', 'glyphicon glyphicon-chevron-left')
						.on('click', function() {
							chartArea.remove();
							init(initData);
						});

					area = chartArea.append('div')
						.attr('class', 'area');

					sample = area.selectAll('div')
						.data(data.chartMarks.sort(function(a, b) {
							return +b.mark - +a.mark;
						}));

					item = sample.enter()
						.append('div')
						.attr('class', 'item-bar');

					legend = item.append('div')
						.attr('class', 'legend');

					legend.append('span')
						.text(function(d) {
							return d.name;
						});

					containerBar = item.append('div')
						.attr('class', function(d) {
							if (d.childData) {
								return 'container-bar full';
							} else {
								return 'container-bar';
							}
						})
						.on('click', function(d) {
							if (d.childData && !data.options.hasParent) {
								chartArea.remove();
								init(d.childData);
							}
							if (d.childData && data.options.hasParent) {
								chartArea.remove();
								init(d.childData, data);
							}
						});

					bar = containerBar.append('div')
						.attr('class', 'bar');

					rateNumber = bar.append('span')
						.attr('class', 'rate');

					bar.style({
						'width': 0,
						'background-color': 'rgba(70, 129, 174, 0)'
					})
						.transition()
						.duration(1250)
						.style({
							'width': function(d) {
								return d.mark + '%';
							},
							'background-color': function(d, i) {
								return 'rgba(70, 129, 174, ' + (d.mark / 100) + ')';
							}
						});

					triangle = containerBar.append('div')
						.attr('class', 'triangle')
						.style('border-left-color', 'rgba(70, 129, 174, 0)');

					widthLegend = getWidthLegend(legend[0]);

					var sortBars = function() {
						var time = data.chartMarks.length * 100 + 300;
						sortOrder = !sortOrder;
						var sortItems = function sortItems(a, b) {
							if (sortOrder) {
								return +b.mark - +a.mark;
							}
							return +a.mark - +b.mark
						};

						item.transition()
							.delay(function(d, i) {
								return i * 100;
							})
							.duration(300)
							.style('opacity', '0');

						item.transition()
							.delay(function(d, i) {
								return time + i * 100;
							})
							.duration(time * 2)
							.style('opacity', '100');

						titleText.attr('class', function() {
							if (sortOrder) {
								return 'text ascending';
							}
							return 'text descending';
						});

						$timeout(function() {
							item.sort(sortItems);
						}, time);

					};

					var titleText = title.append('span')
						.attr('class', 'text ascending');

					titleText.text(function() {
						if (data.options.title) {
							return data.options.title;
						}
					})
						.on('click', sortBars);

					render();

					triangle.transition()
						.duration(1250)
						.style('border-left-color', function(d, i) {
							return 'rgba(70, 129, 174, ' + (d.mark / 100) + ')';
						});

					rateNumber.transition()
						.duration(1250)
						.tween('text', function(d) {
							var i = d3.interpolateRound(0, d.mark);
							return function(t) {
								this.textContent = i(t) + '%';
							};
						});
				}

				function render() {
					var legendPercentWidth = getWidthPercent(widthLegend);

					var widthList = {
						containerBar: containerBar[0]
					};

					legend.style({
						'width': function() {
							return legendPercentWidth + '%';
						},
						'text-align': function() {
							return legendPercentWidth !== 100 ? 'right' : 'left';
						}
					});

					containerBar.style({
						'width': function() {
							return legendPercentWidth !== 100 ? 100 - legendPercentWidth + '%' : 100 + '%';
						},
						'margin-top': function() {
							return legendPercentWidth !== 100 ? '15px' : '0';
						}
					});

					rateNumber.style({
						'left': function(d, i) {
							if (d.mark === 0) return '10px';
							return selectValue(d.mark, widthList.containerBar[i], '35px', '-10px');
						},
						'color': function(d, i) {
							return selectValue(d.mark, widthList.containerBar[i], '#757575', '#fff');
						}
					});

					triangle.style('display', function(d, i) {
						return selectValue(d.mark, widthList.containerBar[i], '', 'none');
					})
				}

				function selectValue(barWidth, data, val1, val2) {
					if (data) {
						var containerWidth = data.getBoundingClientRect().width;
						var width = (100 - barWidth) * containerWidth / 100;
					}
					if (width > 37) {
						return val1;
					} else {
						return val2;
					}
				}

				function getWidthLegend(legend) {
					var i;
					var widthItems = [];
					var legendWidth;

					for (i = 0; i < legend.length; i++) {
						widthItems.push(legend[i].getBoundingClientRect().width);
					}
					legendWidth = Math.max.apply(Math, widthItems);
					return legendWidth + 15;
				}

				function getWidthPercent(widthLegend) {
					var areaWidth = chartArea[0][0].getBoundingClientRect().width;
					var percent;

					if (areaWidth > 600) {
						percent = Math.ceil(widthLegend / areaWidth * 100);
						return percent < 50 ? percent : 100;
					} else {
						return 100;
					}
				}

				init(initData);

				var timer;
				var handler = function() {
					clearTimeout(timer);
					timer = setTimeout(render, 100);
				};
				scope.$on('gridster-item-resized', function() {
					console.log(1);
				});
				angular.element($window).bind('resize', handler);
				scope.$on('$destroy', function() {
					angular.element($window).unbind('resize', handler);
				});
			}
		}
	}]);

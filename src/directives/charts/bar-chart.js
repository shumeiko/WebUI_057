angular.module('charts')
	.directive('barChart', ['$window', function($window) {
		return {
			restrict: 'EA',
			scope: {
				data: '='
			},
			link: function(scope, element) {
				var win = angular.element($window);
				var d3 = $window.d3;

				var placeholder = d3.select(element[0]);

				var canvasData = {
					axisWidth: 40,
					padding: 10
				};

				var marks, width, plotWidth, plotHeight, colorScale, indices, xScale, yScale, axisScale, canvas, plot, axis, axisBox, bars, text, points;

				function init() {

					colorScale = d3.scale.linear()
						.domain([marks.marksData.minVal, marks.marksData.maxVal])
						.range([marks.marksData.minValColor, marks.marksData.maxValColor]);

					indices = d3.range(0, marks.marksArray.length);

					xScale = d3.scale.ordinal()
						.domain(indices);

					yScale = d3.scale.linear()
						.domain([marks.marksData.minVal, marks.marksData.maxVal]);

					axisScale = d3.scale.linear()
						.domain([marks.marksData.minVal, marks.marksData.maxVal]);

					canvas = placeholder
						.append("svg")
						.style('width', '100%');

					plot = canvas
						.append("g")
						.attr({
							transform: "translate(" + canvasData.axisWidth + ", " + canvasData.padding + ")"
						});

					axis = d3.svg.axis()
						.scale(axisScale)
						.orient("left");

					axisBox = plot
						.append("g")
						.call(axis);

					bars = plot.selectAll("rect")
						.data(marks.marksArray)
						.enter()
						.append("rect")
						.attr({
							fill: 'white'
						});

					bars
						.transition()
						.delay(function(d, i) {
							return i * 100;
						})
						.duration(1000)
						.attr({
							fill: function(d, i) {
								return colorScale(d.mark);
							}
						});

					text = plot.selectAll("text.subject")
						.data(marks.marksArray)
						.enter()
						.append("text")
						.text(function(d) {
							return d.subject;
						})
						.attr({
							"text-anchor": "middle",
							"font-family": "sans-serif",
							"font-size": "10px",
							"fill": "black",
							"class": "subject"
						});

					points = plot.selectAll("text.mark")
						.data(marks.marksArray)
						.enter()
						.append("text")
						.text(function(d) {
							return d.mark;
						})
						.attr({
							"text-anchor": "middle",
							"font-family": "sans-serif",
							"font-size": "11px",
							"fill": "white",
							"class": "mark"
						});

					render();
				}

				function render() {

					width = placeholder.node().getBoundingClientRect().width;

					plotWidth = width - canvasData.axisWidth - canvasData.padding;
					plotHeight = width - canvasData.axisWidth - canvasData.padding;

					xScale
						.rangeBands([0, plotWidth], 0.01, 0.01);

					yScale
						.range([0, plotHeight]);

					axisScale
						.range([plotHeight, 0]);

					canvas
						.attr({
							width: width,
							height: width
						});

					axis
						.scale(axisScale);

					axisBox
						.call(axis);

					bars
						.attr({
							width: xScale.rangeBand(),
							height: function(d, i) {
								return yScale(d.mark);
							},
							x: function(d, i) {
								return (xScale(i));
							},
							y: function(d, i) {
								return plotHeight - yScale(d.mark);
							}
						});

					text
						.attr({
							"x": function(d, i) {
								return xScale(i) + xScale.rangeBand() / 2;
							},
							"y": (plotHeight + canvasData.padding + 10)
						});

					points
						.attr({
							"x": function(d, i) {
								return xScale(i) + xScale.rangeBand() / 2;
							},
							"y": function(d) {
								return plotHeight - yScale(d.mark) + canvasData.padding;
							}
						});
				}

				marks = scope.data;
				init();

				var timer;
				var handler = function(){
					clearTimeout(timer);
					timer = setTimeout(render, 100);
				};
				win.bind('resize', handler);
				scope.$on('$destroy', function(){
					win.unbind('resize', handler);
				});

			}
		};

	}]);
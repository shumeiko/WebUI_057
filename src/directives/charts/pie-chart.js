angular.module('charts')
	.directive('pieChart', ['$window', '$filter', function($window, $filter) {
		return {
			restrict: 'EA',
			scope: {
				data: '='
			},
			link: function(scope, element) {
				var win = angular.element($window);
				var d3 = $window.d3;

				var canvasData = {
					padding: 0.02
				};

				var placeholder = d3.select(element[0]);

				var width, container, legendArea, legendTable, legendHead, radius, canvas, pieChart, pie, arc, outlineArc, colorScale, colorScale2, outerPaths, outerPathsNodes, paths, pathsNodes, textPlaceholder, outerText, text, legendItem;

				function init() {
					var i;
					var sumScore = 0;
					for (i = 0; i < data.length; i++) {
						sumScore += data[i].qCost * data[i].qQnty;
					}

					container = placeholder.append("div")
						.attr("class", "pie-chart-area");

					legendArea = placeholder.append("div")
						.attr("class", "legend");

					canvas = container
						.append("svg")
						.style({
							'width': '100%'
						});

					pieChart = canvas
						.append("g");

					pie = d3.layout.pie()
						.sort(null)
						.value(function(d) {
							return d.qCost * d.qQnty;
						});

					arc = d3.svg.arc();

					outlineArc = d3.svg.arc();

					colorScale = d3.scale.linear()
						.domain([d3.min(data, function(d) {
							return d.level;
						}), d3.max(data, function(d) {
							return d.level;
						})])
						.range(["#B2EBF2", "#0097A7"]);

					colorScale2 = d3.scale.linear()
						.domain([d3.min(data, function(d) {
							return d.level;
						}), d3.max(data, function(d) {
							return d.level;
						})])
						.range(["#FFE0B2", "#F57C00"]);

					outerPaths = pieChart.selectAll(".outlineArc")
						.data(pie(data))
						.enter()
						.append("g");

					legendTable = legendArea.append("table");

					legendHead = legendTable.append("tr");

					legendHead.append("th");
					legendHead.append("th")
						.text(function() {
							return $filter('translate')('ssTest.components.pieChart.allScore');
						});
					legendHead.append("th")
						.text(function() {
							return $filter('translate')('ssTest.components.pieChart.score');
						});

					legendItem = legendTable.selectAll(".legend-item")
						.data(data)
						.enter()
						.append("tr");

					legendItem.append("td")
						.text(function(d) {
							return $filter('translate')('ssTest.components.resultDetail.level') + ' ' + d.level;
						});

					legendItem.append("td")
						.append("div")
						.attr("class", "circle")
						.style({
							"background": function(d) {
								return colorScale2(d.level);
							}
						})
						.text(function(d) {
							return Math.round(d.qCost * d.qQnty / sumScore * 100) + '%';
						});

					legendItem.append("td")
						.append("div")
						.attr("class", "circle")
						.style({
							"background": function(d) {
								return colorScale(d.level);
							}
						})
						.text(function(d) {
							return Math.round(d.qCost * d.qRight / sumScore * 100) + '%';
						});

					outerPathsNodes = outerPaths.append("path")
						.attr("fill", function(d) {
							return colorScale2(d.data.level);
						})
						.attr("stroke", "white")
						.attr("stroke-width", 0.5)
						.attr("class", "outlineArc");

					paths = pieChart.selectAll(".solidArc")
						.data(pie(data))
						.enter()
						.append("g");

					pathsNodes = paths.append("path")
						.attr("fill", function(d) {
							return colorScale(d.data.level);
						})
						.attr("class", "solidArc")
						.attr("stroke", "white")
						.attr("stroke-width", 0.5);

					textPlaceholder = pieChart.selectAll(".arcText")
						.data(pie(data))
						.enter()
						.append("g")
						.attr({
							class: 'arcText'
						});

					outerText = textPlaceholder.append('text')
						.attr({
							"text-anchor": "middle"
						})
						.text(function(d) {
							return d.data.qCost * d.data.qQnty + "б.";
						});

					text = textPlaceholder.append('text')
						.attr({
							"text-anchor": "middle"
						})
						.text(function(d) {
							return d.data.qRight * d.data.qCost + "б.";
						});

					render();
				}

				function render() {
					width = container[0][0].getBoundingClientRect().width;

					radius = width / 2 * (1 - canvasData.padding * 2);

					canvas
						.attr({
							width: width,
							height: width
						});

					pieChart
						.attr("transform", "translate(" + width / 2 + "," + width / 2 + ")");

					arc
						.innerRadius(0)
						.outerRadius(function(d) {
							return radius * d.data.qRight / d.data.qQnty;
						});

					outlineArc
						.innerRadius(function(d) {
							return radius * d.data.qRight / d.data.qQnty;
						})
						.outerRadius(radius);

					outerPathsNodes
						.attr("d", outlineArc);

					pathsNodes
						.attr("d", arc);

					outerText
						.attr({
							transform: function(d) {
								return "translate(" + outlineArc.centroid(d) + ")";
							},
							"text-anchor": "middle"
						});

					text
						.attr({
							transform: function(d) {
								return "translate(" + arc.centroid(d) + ")";
							}
						});
				}

				data = scope.data;
				init();

				var timer;
				var handler = function(){
					clearTimeout(timer);
					timer = setTimeout(render, 100);
				};
				win.bind('resize', handler);
				scope.$on('$destroy', function(){
					win.unbind('resize', handler);
				});
			}
		};

	}]);
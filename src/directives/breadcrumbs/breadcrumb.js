angular.module('admin')
	.directive('breadcrumbs', ['$interpolate', '$state', '$rootScope', '$sessionStorage',
		function($interpolate, $state, $rootScope, $sessionStorage) {
		return {
			restrict: 'EA',
			templateUrl: "directives/breadcrumbs/breadcrumb.html",
			scope: {},
			controller: function($scope) {
				$scope.breadcrumbs = [];
				if (!$sessionStorage.breadcrumbs) {
					$sessionStorage.breadcrumbs = {};
				}

				if ($state.$current.name !== '') {
					updateBreadcrumbsArray();
				}

				$scope.$on('$stateChangeSuccess', function() {
					updateBreadcrumbsArray();
				});

				$rootScope.$on('loadedEntity', function() {
					updateBreadcrumbsArray();
				});

				function updateBreadcrumbsArray() {
					var title;
					var entityId;
					var breadcrumbs = [];
					var currentState = $state.$current;
					var context = $sessionStorage.breadcrumbs;

					while (currentState && currentState.name !== '') {
						title = $interpolate(currentState.breadcrumb.label)(context);
						if (currentState.breadcrumb.id) {
							entityId = $interpolate(currentState.breadcrumb.id)(context);
						}
						if (title !== false) {
							breadcrumbs.push({
								title: title,
								state: currentState.name,
								entityId: entityId
							});
						}
						currentState = $state.get(currentState.breadcrumb.parent);
					}
					breadcrumbs.reverse();
					$scope.breadcrumbs = breadcrumbs;
				}
			}
		}
	}]);
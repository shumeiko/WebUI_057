angular.module('gridster')
	.controller('widgetTestsInstallCtrl', ['$scope', 'AdminServices', 'data', '$modalInstance',
		function($scope, AdminServices, data, $modalInstance){
			$scope.formState = {};

			$scope.widget = data.widgetData;$scope.update = data.update;

			$scope.subjects = false;

			AdminServices.getSubjects().then(function(subjects) {
				$scope.subjects = subjects;
			});

			$scope.send = function(){
				$scope.formState.formSubmited = true;
			};
			$scope.cancel = function(){
				$modalInstance.close();
			};

			$scope.save = function(){
				data.onDone($scope.widget);
				$modalInstance.close();
			};
		}]);

angular.module('gridster')
	.directive('widgetTests', ['$sessionStorage', 'ModalService', 'AdminServices',
		function($sessionStorage, ModalService, AdminServices){

		return {
			restrict: 'AE',
			scope: {
				widgetData: "="
			},
			templateUrl: "directives/widget-tests/widget-tests.html",
			link: function (scope, element, attrs) {

			},
			controller: function($scope){
				var subjectId = $scope.widgetData.subjectId;

				AdminServices.getTestsBySubject(subjectId).then(function(tests) {
					if (Array.isArray(tests[0])) {
						$scope.tests = false;
					} else {
						$scope.tests = tests;
					}
					$scope.loaded = true;
				});
				AdminServices.getSubjects(subjectId).then(function(subject){
					$scope.subjectName = subject[0].subject_name;

					$sessionStorage.breadcrumbs.subject = {
						title: $scope.subjectName,
						id: subjectId
					};
				});
			}

		};
	}]);

angular.module('gridster')
	.directive('widgetStudentsRating', ['$q', 'AdminServices', '$filter', 'StudentProvider',
		function($q, AdminServices, $filter, StudentProvider) {

			return {
				restrict: 'AE',
				scope: {
					widgetData: "="
				},
				templateUrl: "directives/widget-students-rating/widget-students-rating.html",
				link: function(scope, element, attrs) {

				},
				controller: function($scope) {
					var groupId = $scope.widgetData.groupId;
					AdminServices.getStudentsByGroup(groupId).then(function(students) {
						var resultsPromises = [];
						$scope.students = students;

						angular.forEach(students, function(student) {
							resultsPromises.push(StudentProvider.getResultsDataByStudent(student.user_id));
						});
						return $q.all(resultsPromises);

					}).then(function(results) {
						AdminServices.getEntityValues('Subject', getSubjectsIdsList(results)).then(function(subjects) {
							var i;
							var j;
							var chartMark;
							var subjectId;
							var studentName;
							var subjectsObj;
							var subjectChartData;
							var studentChartData;
							var subjectsChartData;
							var chartMarks = [];
							var subjectsNames = {};

							for (j = 0; j < subjects.length; j++) {
								subjectsNames[subjects[j].subject_id] = subjects[j].subject_name;
							}

							for (i = 0; i < $scope.students.length; i++) {
								if (results[i]) {
									studentName = $scope.students[i].student_surname + ' ' + $scope.students[i].student_name;
									subjectsObj = results[i].scoreListsBySubjects;
									subjectsChartData = {};

									for (subjectId in subjectsObj) {
										subjectChartData = {
											options: {
												title: subjectsNames[subjectId],
												backButton: true
											},
											chartMarks: StudentProvider.getChartMarks(subjectsObj[subjectId])
										};
										subjectsChartData[subjectId] = (subjectChartData);
										subjectsObj[subjectId] = StudentProvider.getRating(subjectsObj[subjectId]);
									}

									studentChartData = {
										options: {
											title: studentName,
											backButton: true,
											hasParent: true
										},
										chartMarks: StudentProvider.getChartMarks(subjectsObj, subjectsNames, subjectsChartData)
									};

									chartMark = {
										name: studentName,
										mark: StudentProvider.getRating(subjectsObj),
										childData: studentChartData
									};
									chartMarks.push(chartMark);
								}
							}

							AdminServices.getGroups(groupId).then(function(group) {
								$scope.groupName = group[0].group_name;
								$scope.chartData = {
									options: {
										title: $filter('translate')('ssTest.components.widgets.widgetStudentsRating.ratingOfGroup') + ' ' + $scope.groupName
									},
									chartMarks: chartMarks
								};
								$scope.loaded = true;
							});
						});
					});

					function getSubjectsIdsList(results) {
						var i;
						var subjectId;
						var subjectsIdsList = [];

						for (i = 0; i < results.length; i++) {
							if (results[i]) {
								for (subjectId in results[i].scoreListsBySubjects) {
									if (subjectsIdsList.indexOf(subjectId) === -1) {
										subjectsIdsList.push(subjectId);
									}
								}
							}
						}
						return subjectsIdsList;
					}

				}
			};
		}]);

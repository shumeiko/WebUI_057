angular.module('gridster')
	.controller('widgetStudentsRatingInstallCtrl', ['$scope', 'AdminServices', 'data', '$modalInstance',
		function($scope, AdminServices, data, $modalInstance){
		$scope.formState = {};

		$scope.widget = data.widgetData;$scope.update = data.update;

		$scope.groups = false;

		AdminServices.getGroups().then(function(groups) {
			$scope.groups = groups;
		});

		$scope.send = function(){
			$scope.formState.formSubmited = true;
		};
		$scope.cancel = function(){
			$modalInstance.close();
		};

		$scope.save = function(){
			data.onDone($scope.widget);
			$modalInstance.close();
		};
	}]);

angular
	.module('calendar')
	.controller('MonthsCtrl', ['$scope', function($scope) {
		var months = [];
		var index = 1;

		for (var i = 0; i < 3; i++) {
			months[i] = [];
			for (var j = 0; j < 4; j++) {
				months[i][j] = {title: 'ssTest.calendar.months.shorthand.' + index, index: index};
				index++;
			}
		}

		$scope.months = months;
		$scope.calendar.showAnimation = false;
	}]);
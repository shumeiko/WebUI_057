angular
	.module('admin')
	.controller('InformCtrl', ['$scope', 'AdminServices', '$q', 'ModalService', '$filter',
		function($scope, AdminServices, $q, ModalService, $filter) {
			var eventDate;

			$scope.getTestEvent = function(index) {
				$scope.readyInform = false;
				$scope.calendar.showInformSpinner = true;
				var data = $scope.calendar.eventDates;
				var date = data[index].event_date.split('-').reverse().join('/');

				eventDate = data[index].event_date;
				$scope.currentEvent = index;
				$scope.event = {event_date: date};

				$q.all([
					AdminServices.getEntityValues('Subject', [data[index].subject_id]),
					AdminServices.getEntityValues('Group', [data[index].group_id]),
					AdminServices.getTestsBySubject(data[index].subject_id),
					AdminServices.getStudentsByGroup(data[index].group_id)
				]).then(function(data) {
					$scope.event.subject_name = data[0][0].subject_name;
					$scope.event.group_name = data[1][0].group_name;
					$scope.event.tests = [];

					for (var i = 0; i < data[2].length; i++) {
						if (data[2][i].enabled === '1') {
							$scope.event.tests.push(data[2][i].test_name);
						}
					}

					if (data[3].length <= 1 && data[3][0][1] === 'null') {
						$scope.event.students = 0;
					} else {
						$scope.event.students = data[3].length;
					}

					$scope.calendar.showSpinner = false;
					$scope.calendar.showInformSpinner = false;
					$scope.readyInform = true;
					$scope.calendar.informAnimation = true;
				});
			};

			$scope.calendar.getTestEvent = $scope.getTestEvent;

			$scope.importGoogleCalendar = function() {
				// Your Client ID can be retrieved from your project in the Google
				// Developer Console, https://console.developers.google.com
				var CLIENT_ID = '636898457530-ltqqg7goe7ih77tal3pmbm23pb6ghkq7.apps.googleusercontent.com';
				var SCOPES = ["https://www.googleapis.com/auth/calendar"];

				$scope.calendar.showInformSpinner = true;

				// Check if current user has authorized this application.
				function checkAuth(immediate) {
					gapi.auth.authorize(
						{
							'client_id': CLIENT_ID,
							'scope': SCOPES,
							'immediate': immediate
						}, handleAuthResult);
				}

				// Handle response from authorization server
				function handleAuthResult(authResult) {
					if (authResult && !authResult.error) {
						// Hide auth UI, then load client library
						loadCalendarApi();
					} else {
						// Show auth UI
						checkAuth(false);
					}
				}

				// Load Google Calendar client library. Insert event once client library is loaded
				function loadCalendarApi() {
					gapi.client.load('calendar', 'v3', insertEvent);
				}

				function insertEvent() {
					var d = eventDate.split('-');
					var year = d[0];
					var month = d[1];
					var date = d[2];
					var nextDate = new Date(+year, +month - 1, +date + 1);
					var event;

					year = nextDate.getFullYear();
					month = nextDate.getMonth() + 1;
					date = nextDate.getDate();

					if (month < 10) month = '0' + month;

					if (date < 10) date = '0' + date;

					nextDate = [year, month, date].join('-');
					event = {
						'summary': $filter('translate')('ssTest.calendar.events.plannedEvent'),
						'description':
							$filter('translate')('ssTest.calendar.events.subject') + ' ' +
							$scope.event.subject_name + '.\n' +
							$filter('translate')('ssTest.calendar.events.group') + ' ' +
							$scope.event.group_name + '.\n' +
							$filter('translate')('ssTest.calendar.events.tests') + ' ' +
							$scope.event.tests.join(', ') + '.',
						'start': {
							'date': eventDate
						},
						'end': {
							'date': nextDate
						}
					};

					var request = gapi.client.calendar.events.insert({
						'calendarId': 'primary',
						'resource': event
					});

					request.execute(function() {
						$scope.calendar.showInformSpinner = false;

						ModalService.open({
							data: {
								title: "ssTest.SUCCESS",
								log: {
									logMsg: "ssTest.calendar.addedMsg"
								}
							}
						});
					});
				}

				checkAuth(true);
			};
		}
	]);
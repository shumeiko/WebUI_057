angular
	.module('calendar')
	.controller('YearsCtrl', ['$scope', function($scope) {
		function createYears() {
			if ($scope.calendar.year < ($scope.calendar.minYear + 1)) {
				$scope.calendar.year++;
			}

			var years = [];
			var num = $scope.calendar.year + '';
			var index = -1;

			num = +(num.slice(0, 3) + '0');

			for (var i = 0; i < 3; i++) {
				years[i] = [];

				for (var j = 0; j < 4; j++) {
					years[i][j] = {title: num + index};

					if (index === -1 || index === 10) {
						years[i][j].anotherYear = true;
					}

					index++;
				}
			}

			$scope.years = years;
			$scope.firstYear = years[0][1].title;
			$scope.lastYear = years[2][2].title;
			$scope.prevYears = $scope.firstYear - 1;
			$scope.nextYears = $scope.lastYear + 1;
			$scope.calendar.showAnimation = false;
		}

		$scope.calendar.createYears = createYears;

		createYears();
	}]);
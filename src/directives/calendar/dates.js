angular
	.module('calendar')
	.controller('DatesCtrl', ['$scope', '$translate', 'AdminServices',
		function($scope, $translate, AdminServices) {
			var daysInWeek = 7;
			var getDay;

			function getDayFromMon(date) { // get day number of week, from 0 (mon) to 6 (sun)
				var day = date.getDay();
				if (day === 0) day = daysInWeek;
				return day - 1;
			}

			function getDayFromSun(date) { // get day number of week, from 0 (sun) to 6 (sat)
				return date.getDay();
			}

			function defineLocale(locale) {
				if (locale === 'en' || locale === 'ch') {
					getDay = getDayFromSun;
				} else {
					getDay = getDayFromMon;
				}
			}

			function createDates(year, month) {
				var now = new Date();
				var today = new Date(now.getFullYear(), now.getMonth(), now.getDate());
				var nextMonth = new Date(today.getFullYear(), today.getMonth() + 1);
				var eventDates;
				var isArray;

				function buildDates() {
					var d = new Date(year, month);
					var weeks = [];
					var week = 0;
					var prevMonth = new Date($scope.calendar.year,
											 $scope.calendar.date.getMonth() - 1);
					var countPrevDays;
					var eventDate;
					var eventMonth;
					var currentDate;

					$scope.days = [];
					$scope.month = 'ssTest.calendar.months.' + (month + 1);
					$scope.calendar.year = year;
					$scope.checkingYear = prevMonth.getFullYear();

					for (var k = 1; k <= daysInWeek; k++) {
						$scope.days.push('ssTest.calendar.days.' + k);
					}

					weeks[week] = [];

					// fill first row from monday to the first day of current month
					countPrevDays = getDay(d);
					d.setDate(1 - countPrevDays);

					for (var i = 0; i < countPrevDays; i++) {
						weeks[week].push({date: d.getDate(), anotherMonth: true, month: month});
						d.setDate(d.getDate() + 1);
					}

					// calendar cells with dates
					while (d.getMonth() === month) {
						if (+d  === +today) {
							weeks[week].push({date: d.getDate(), today: true, month: month + 1});
						} else {
							weeks[week].push({date: d.getDate(), month: month + 1});
						}

						if (eventDates) {
							for (var l = 0; l < eventDates.length; l++) {
								eventDate = +eventDates[l]['event_date'].split('-')[2];
								eventMonth = +eventDates[l]['event_date'].split('-')[1];

								if ((d.getDate() === eventDate) &&
									((d.getMonth() + 1) === eventMonth)) {
									currentDate = weeks[week][weeks[week].length - 1];

									if (!currentDate.eventIndexes) {
										currentDate.event = true;
										currentDate.eventIndexes = [l];
									} else {
										currentDate.eventIndexes.push(l);
									}
								}
							}
						}

						if (getDay(d) % daysInWeek === (daysInWeek - 1)) { // sunday is the last day
						// of week => new row
							week++;
							weeks[week] = [];
						}

						d.setDate(d.getDate() + 1);
					}

					// add empty cells if need
					if (getDay(d) !== 0) {
						for (var j = getDay(d); j < daysInWeek; j++) {
							weeks[week].push({date: d.getDate(), anotherMonth: true,
								month: month + 2});
							d.setDate(d.getDate() + 1);
						}
					}

					$scope.weeks = weeks;
					$scope.calendar.showAnimation = false;
				}

				if ((today.getFullYear() === year && today.getMonth() === month) ||
					(nextMonth.getFullYear() === year && nextMonth.getMonth() === month)) {

					$scope.calendar.showSpinner = true;

					AdminServices.getTimeTablesFromNowInMonth().then(function(data) {
						isArray = Array.isArray(data[0]);

						if (!isArray) {
							$scope.calendar.eventDates = eventDates = data;
						}

						$scope.calendar.showSpinner = false;

						buildDates();
					});
				} else {
					$scope.calendar.showSpinner = false;

					buildDates();
				}
			}

			$scope.calendar.createDates = createDates;

			defineLocale($translate.use());
			createDates($scope.calendar.date.getFullYear(), $scope.calendar.date.getMonth());

			$scope.$on('localeChange', function(event, lang) {
				defineLocale(lang);
				createDates($scope.calendar.date.getFullYear(), $scope.calendar.date.getMonth());
			});
		}
	]);
angular
	.module('calendar', [])
	.directive('customCalendar', [function() {
		return {
			restrict: 'EA',
			templateUrl: 'directives/calendar/calendar.html',
			scope: {
				setOnly: '=',
				setDate: '='
			},
			controller: function($scope) {
				$scope.dates = true;
				$scope.months = false;
				$scope.years = false;
				$scope.inform = false;
				$scope.calendar = {
					date: new Date(),
					minYear: 1899,
					methods: {
						today: today,
						moveMonth: moveMonth,
						moveYear: moveYear,
						moveYears: moveYears,
						toMonths: toMonths,
						toYears: toYears,
						getMonth: getMonth,
						getYear: getYear
					},
					change: change,
					showAnimation: null,
					informAnimation: null,
					showSpinner: false,
					showInformSpinner: false
				};

				function today() {
					$scope.calendar.date = new Date();

					if (!$scope.dates) {
						$scope.months = $scope.years = false;
						$scope.dates = true;
					} else {
						$scope.calendar.createDates($scope.calendar.date.getFullYear(),
													$scope.calendar.date.getMonth());
					}
				}

				function moveMonth(n) {
					$scope.calendar.date.setFullYear($scope.calendar.year,
													 $scope.calendar.date.getMonth() + n);
					$scope.calendar.createDates($scope.calendar.date.getFullYear(),
												$scope.calendar.date.getMonth());
				}

				function moveYear(n) {
					$scope.calendar.year += n;
					$scope.calendar.showAnimation = false;
				}

				function moveYears(years) {
					$scope.calendar.year = years;
					$scope.calendar.createYears();
				}

				function toMonths() {
					$scope.dates = false;
					$scope.months = true;
				}

				function toYears() {
					$scope.months = false;
					$scope.years = true;
				}

				function getMonth(month) {
					$scope.calendar.date.setFullYear($scope.calendar.year, month);
					$scope.months = false;
					$scope.dates = true;
				}

				function getYear(year) {
					$scope.calendar.year = year;
					$scope.years = false;
					$scope.months = true;
					$scope.calendar.showAnimation = false;
				}

				function change(mouseEvent, funcName, param) {
					$scope.calendar.data = {
						mouseEvent: mouseEvent,
						funcName: funcName,
						param: param
					};
					$scope.calendar.showAnimation = true;
				}

				$scope.chooseDate = function(data, year) {
					var date = data.date;
					var month = data.month;

					if (date < 10) {
						date = '0' + date;
					}

					if (month === 0) {
						month = 12;
						year--;
					} else if (month < 10) {
						month = '0' + month;
					}

					$scope.setDate = '' + date + month + year;
				};
			},
			link: function($scope) {
				$scope.getEventInfo = function(event, data) {
					var left = event.clientX;
					var top = event.clientY;
					var style = document.createElement('style');

					$scope.calendar.showSpinner = true;

					style.innerHTML = '.inform.ng-hide-add.ng-hide-add-active,' +
						'.inform.ng-hide-remove{left:' + left + 'px;top:' + top + 'px;}';
					document.body.appendChild(style);
					$scope.countEvents = data;
					$scope.calendar.getTestEvent(data[0]);
				};
			}
		};
	}])

	.directive('calendarAnimation', ['$animate', function($animate) {
		return {
			restrict: 'A',
			scope: {
				show: '=',
				methods: '=',
				data: '='
			},
			link: function($scope, $element) {
				$scope.$watch('show', function(show, oldShow) {
					if (show === oldShow) return;

					if (show) {
						var mouseEvent = $scope.data.mouseEvent;
						var funcName = $scope.data.funcName;
						var param = $scope.data.param;
						var coords = $element.parent()[0].getBoundingClientRect();
						var left = mouseEvent.clientX - coords.left + 'px';
						var top = mouseEvent.clientY - coords.top + 'px';

						$element.css({left: left, top: top});
						$animate.removeClass($element, 'ng-hide').then(function() {
							$scope.$apply(function() {
								$scope.methods[funcName](param);
							});
						});
					} else {
						$animate.addClass($element, 'ng-hide');
					}
				});
			}
		};
	}]);
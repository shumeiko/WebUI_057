angular.module('ssTest')
	.directive('entityTable', ['ModalService', 'AdminServices', '$state',
		function(ModalService, AdminServices, $state) {
		return {
			restrict: "EA",
			templateUrl: "directives/table/table.html",
			scope: {
				tableHead: '=',
				tableBody: '=',
				tableOptions: '=',
				tableFilter: '='
			},
			controller: function($scope) {
				$scope.sortField = false;
				$scope.reverse = false;
				$scope.sort = function(fieldName) {
					if ($scope.sortField === fieldName) {
						$scope.reverse = !$scope.reverse;
					} else {
						$scope.sortField = fieldName;
						$scope.reverse = false;
					}
				};
				$scope.isSortUp = function(fieldName) {
					return $scope.sortField === fieldName && !$scope.reverse;
				};
				$scope.isSortDown = function(fieldName) {
					return $scope.sortField === fieldName && $scope.reverse;
				};

				$scope.delete = function(id) {
					ModalService.open({
						data: {
							title: "ssTest.ATTENTION",
							log: {
								logMsg: 'ssTest.table.logMsgDelete'
							},
							action: {
								actionTxt: "ssTest.controls.yes",
								actionFn: function(){
									var i;
									var delService = $scope.tableOptions.delNameService;
									var idNameProp = $scope.tableOptions.entityId;

									AdminServices[delService](id).then(function(data){
										var msq;
										if (data.response === 'ok') {
											for (i = 0; i < $scope.tableBody.length; i++) {
												if ($scope.tableBody[i][idNameProp] == id) {
													$scope.tableBody.splice(i, 1);
													break;
												}
											}
										} else {
											msq = data.response === 'error 23000' ? 'ssTest.responseErrors.ed23000' : 'ssTest.responseErrors.deleteOneself';
											ModalService.open({
												data: {
													title: "ssTest.ERROR",
													log: {
														logMsg: msq
													}
												}
											});
										}
									});
								}
							}
						}
					});
				};
				$scope.enterToEntity = function(entityId) {
					if($scope.tableOptions.state) {
						$state.go($scope.tableOptions.state ,{'id': entityId});
					}
				};
				$scope.edit = function(entityId) {
					$state.go($scope.tableOptions.editState ,{'id': entityId});
				}
			}
		}
	}]);
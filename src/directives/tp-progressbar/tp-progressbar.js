angular
	.module('ssTest')
	.directive('tpProgressbar', tpProgressbar);

tpProgressbar.$inject = [];

function tpProgressbar() {
	return {
		restrict: 'E',
		templateUrl: 'directives/tp-progressbar/tp-progressbar.html',
		link: link
	};

	function link(scope, element, attrs) {
		attrs.$observe('value', render);
		progressBox = element.children()[0].querySelectorAll('.progress')[0];

		element.on('$destroy', function() {
			progressBox.setAttribute("style", "width: 0%")
		})

		function render() {
			if (attrs.value <= 100) {
				progressBox.setAttribute("style", "width: " + attrs.value + "%")
			}
		}
	}
}
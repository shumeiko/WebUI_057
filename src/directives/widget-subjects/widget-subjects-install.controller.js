angular.module('gridster')
	.controller('widgetSubjectsInstallCtrl', ['$scope', 'AdminServices', 'data', '$modalInstance',
		function($scope, AdminServices, data, $modalInstance){
			$scope.formState = {};

			$scope.widget = data.widgetData;$scope.update = data.update;
			$scope.widget.data = false;

			$scope.isValid = function(a) {
				if (a.data) return a.data.subjectId[0] ? true : false;
				return false;
			};

			$scope.subjects = [];
			AdminServices.getSubjects().then(function(subjects) {
				$scope.subjects = subjects;
			});

			$scope.send = function(){
				$scope.formState.formSubmited = true;
			};
			$scope.cancel = function(){
				$modalInstance.close();
			};

			$scope.save = function(){
				data.onDone($scope.widget);
				$modalInstance.close();
			};
		}]);
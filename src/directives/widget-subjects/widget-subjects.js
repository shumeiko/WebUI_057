angular.module('gridster')
	.directive('widgetSubjects', ['ModalService', 'AdminServices', function(ModalService, AdminServices){

		return {
			restrict: 'AE',
			scope: {
				widgetData: "="
			},
			templateUrl: "directives/widget-subjects/widget-subjects.html",
			link: function (scope, element, attrs) {

			},
			controller: function($scope){
				var subjectsIds = $scope.widgetData.subjectId;

				AdminServices.getEntityValues('Subject', subjectsIds).then(function(subjects) {
					$scope.subjects = subjects;
					$scope.loaded = true;
				});
			}

		};
	}]);

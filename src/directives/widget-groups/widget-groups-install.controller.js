angular.module('gridster')
	.controller('widgetGroupsInstallCtrl', ['$scope', 'AdminServices', 'data', '$modalInstance',
		function($scope, AdminServices, data, $modalInstance){
			$scope.formState = {};

			$scope.widget = data.widgetData;$scope.update = data.update;
			$scope.widget.data = false;

			$scope.isValid = function(a) {
				if (a.data) return a.data.groupId[0] ? true : false;
				return false;
			};

			$scope.groups = [];
			AdminServices.getGroups().then(function(groups) {
				$scope.groups = groups;
			});

			$scope.send = function(){
				$scope.formState.formSubmited = true;
			};
			$scope.cancel = function(){
				$modalInstance.close();
			};

			$scope.save = function(){
				data.onDone($scope.widget);
				$modalInstance.close();
			};
		}]);

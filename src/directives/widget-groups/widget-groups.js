angular.module('gridster')
	.directive('widgetGroups', ['ModalService', 'AdminServices', function(ModalService, AdminServices){

		return {
			restrict: 'AE',
			scope: {
				widgetData: "="
			},
			templateUrl: "directives/widget-groups/widget-groups.html",
			link: function (scope, element, attrs) {

			},
			controller: function($scope){
				var groupsIds = $scope.widgetData.groupId;
				AdminServices.getEntityValues('Group', groupsIds).then(function(groups) {
					angular.forEach(groups, function(group) {
						AdminServices.getStudentsByGroup(group.group_id).then(function(students) {
							if (Array.isArray(students[0])) {
								group.countStudents = 0;
							} else {
								group.countStudents = students.length;
							}
						});
					});
					$scope.groups = groups;
					$scope.loaded = true;
				});
			}

		};
	}]);

angular.module('gridster')
	.directive('widgetStudents', ['$timeout', 'ModalService', 'AdminServices', '$sessionStorage',
		function($timeout, ModalService, AdminServices, $sessionStorage){

		return {
			restrict: 'AE',
			scope: {
				widgetData: "="
			},
			templateUrl: "directives/widget-students/widget-students.html",
			link: function (scope, element, attrs) {

			},
			controller: function($scope){
				var groupId = $scope.widgetData.groupId;

				AdminServices.getStudentsByGroup(groupId).then(function(students) {
					if (Array.isArray(students[0])) {
						$scope.students = false;
					} else {
						$scope.students = students;
					}
					$scope.loaded = true;
				});
				AdminServices.getGroups(groupId).then(function(group){
					$scope.groupName = group[0].group_name;

					$sessionStorage.breadcrumbs.group = {
						title: $scope.groupName,
						id: groupId
					};
				});
			}
		};
	}]);
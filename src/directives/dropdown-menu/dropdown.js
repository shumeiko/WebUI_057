angular
	.module('ssTest')
	.directive('dropdownMenu', ['$translate', function($translate) {
		return {
			restrict: 'EA',
			templateUrl: 'directives/dropdown-menu/dropdown-menu.html',
			scope: {},
			controller: function($scope, $log, ModalService, logService, $state) {
				document.getElementById("profile-name").innerHTML = localStorage["nameForUser"];
				$scope.status = {
					isopen: false
				};

				$scope.toggleDropdown = function($event) {
					$event.preventDefault();
					$event.stopPropagation();
					$scope.status.isopen = !$scope.status.isopen;
				};
				$scope.$state = $state;
				$scope.exit = function() {
					ModalService.open({
						data: {
							title: "ssTest.ATTENTION",
							log: {
								logMsg: 'ssTest.menu.modalWindows.exit'
							},
							action: {
								actionTxt: "ssTest.controls.yes",
								actionFn: function(){
									logService.logOut();
								}
							}
						},
						options: {
							size: 'sm'
						}
					});
				};
			}
		};

	}]);
angular.module("gridster").directive(
	"widgetsBox", ['ModalService', '$timeout', 'CaseConverter', '$compile', 'WidgetsList', 'AdminServices', '$q',
		function(ModalService, $timeout, CaseConverter, $compile, WidgetsList, AdminServices, $q){
		return {
			restrict: 'A',
			scope: {
				options: "=gridster",
				widgetItems: "=items"
			},
			require: 'gridster',
			templateUrl: "directives/widgets-box/widgets-box.html",
			link: function (scope, element, attrs, Gridster) {
				scope.Gridster = Gridster;

				scope.insertWidget = function(widget, id) {
					var directiveScope = scope.$new();
					directiveScope.widgetData = widget.data;
					var el = $compile(
						widget.template ? widget.template : "<"+ widget.type +" widget-data='widgetData'></"+ widget.type +">"
					)(directiveScope);
					$timeout(function() {
						document.getElementById(id).appendChild(el[0]);
					}, 0);
				};

			},
			controller: function($scope){

				$scope.widgetsList = WidgetsList;

				$scope.CaseConverter = CaseConverter;

				var onError = function(data) {
					ModalService.logError(data.response);
				};

				$scope.controls = {
					openInstall: function(widget, update) {
						var widgetData = update ? angular.copy(widget) : {
							type: widget.type
						};
						ModalService.open({
							data: {
								onDone: update ? $scope.controls.editWidget : $scope.controls.addWidget,
								widgetData: widgetData,
								update: update
							},
							options: {
								templateUrl: "directives/"+widget.type+"/"+widget.type+"-install.html",
								controller: CaseConverter.toCamel(widget.type) + "InstallCtrl"
							}
						});
					},

					removeWidget: function(widget) {
						var widgetCopy;

						$scope.edit = true;

						if ($scope.widgetsCopy) {
							widgetCopy = $scope.controls._findEqual(widget, $scope.widgetsCopy);
							widgetCopy.loading = true;
						}

						widget.loading = true;

						AdminServices.deleteWidget(widget.id).then(
							function(data) {
								if (data.response === 'ok') {
									//on success
									$scope.widgetItems.splice($scope.widgetItems.indexOf(widget), 1);
									//remove from copy array if exist
									if ($scope.widgetsCopy) {
										$scope.widgetsCopy.splice($scope.widgetsCopy.indexOf(widgetCopy), 1);
									}
								} else {
									onError(data);
									widget.loading = false;
									widgetCopy.loading = false;
								}
								$scope.edit = false;
							},
							function(data){
								onError(data);
								widget.loading = false;
								widgetCopy.loading = false;
								$scope.edit = false;
							}
						);
					},

					addWidget: function(widget){
						$scope.edit = true;
						widget.loading = true;
						widget.sizeX = 1;
						widget.sizeY = 1;
						$scope.widgetItems.push(widget);
						if ($scope.widgetsCopy) {
							$scope.widgetsCopy.push(widget);
						}
						$timeout(function() {
							AdminServices.addWidget({
								sizeX: widget.sizeX,
								sizeY: widget.sizeY,
								cols: widget.col,
								rows: widget.row,
								type: widget.type,
								widget_data: JSON.stringify(widget.data) || ""
							}).then(
								function(data) {
									if (data.response === 'ok') {
										widget.id = data.id;
										delete widget.loading;
									} else {
										onError(data);
										$scope.widgetItems.splice($scope.widgetItems.indexOf(widget), 1);
									}
									$scope.edit = false;
								},
								function(data) {
									onError(data);
									$scope.widgetItems.splice($scope.widgetItems.indexOf(widget), 1);
									$scope.edit = false;
								}
							);
						}, 0);

					},

					editWidget: function(widget){

						$scope.edit = true;

						var widgetInGridster = $scope.controls._findEqual(widget, $scope.widgetItems);
						if ($scope.widgetsCopy) {
							var widgetInCopy = $scope.controls._findEqual(widget, $scope.widgetsCopy);
						}

						widgetInGridster.loading = true;
						if ($scope.widgetsCopy) {
							widgetInCopy.loading = true;
						}
						
						var base  = widgetInCopy ? widgetInCopy : widgetInGridster;

						AdminServices.editWidget({
							sizeX: base.sizeX,
							sizeY: base.sizeY,
							cols: base.col,
							rows: base.row,
							type: base.type,
							widget_data: JSON.stringify(widget.data)
						}, widget.id).then(
							function(data) {
								if (data.response === 'ok') {
									//on success
									delete widgetInGridster.loading;
									angular.copy(widget.data, widgetInGridster.data);
									if ($scope.widgetsCopy) {
										delete widgetInCopy.loading;
										angular.copy(widget.data, widgetInCopy.data);
									}
								} else {
									onError(data);
								}
								$scope.edit = false;
							},
							function(data) {
								onError(data);
								$scope.edit = false;
							}
						);
					},

					editWidgetsStart: function() {
						$scope.controls._turnOnEdit();
					},

					editWidgetsSave: function() {
						var requests = [];
						angular.forEach($scope.widgetItems, function(widget) {
							var isEqual = angular.equals(widget, $scope.controls._findEqual(widget, $scope.widgetsCopy));
							if (!isEqual) {
								var request = AdminServices.editWidget({
									sizeX: widget.sizeX,
									sizeY: widget.sizeY,
									cols: widget.col,
									rows: widget.row,
									type: widget.type,
									widget_data: JSON.stringify(widget.data)
								}, widget.id);
								requests.push(request);
							}
						});

						if (requests.length) {
							$scope.loading = true;
							$q.all(requests).then(
								function(dataArr) {
									for (var i = dataArr.length - 1; i >= 0; i--) {
										var data = dataArr[i];
										if (data.response !== 'ok') {
											onError(data);
											return $scope.controls.editWidgetsCancel();
										}
									}
									$scope.controls._turnOffEdit();
									$scope.loading = false;
								},
								function(data) {
									onError(data);
									$scope.controls.editWidgetsCancel();
									$scope.loading = false;
								}
							);
						} else {
							$scope.controls._turnOffEdit();
						}
					},

					editWidgetsCancel: function() {
						angular.extend($scope.widgetItems, $scope.widgetsCopy);
						$scope.controls._turnOffEdit();
					},

					_findEqual: function(widget, widgetsList){
						for (var i = widgetsList.length - 1; i >= 0; i--) {
							var searchedWidget = widgetsList[i];
							if (searchedWidget.id === widget.id) {
								return searchedWidget;
							}
						}
					},

					_turnOnEdit: function() {
						$scope.widgetsCopy = angular.copy($scope.widgetItems, []);
						$scope.widgetBoxEditable = true;
						$scope.options.draggable.enabled = true;
						$scope.options.resizable.enabled = true;
					},

					_turnOffEdit: function() {
						$scope.widgetsCopy = null;
						$scope.widgetBoxEditable = false;
						$scope.options.draggable.enabled = false;
						$scope.options.resizable.enabled = false;
					}
				};

			}
		};
	}]
);
angular
	.module('ssTest')
	.directive('customPagination', function() {
		return {
			restrict: 'EA',
			templateUrl: 'directives/pagination/pagination.html',
			scope: {
				totalItems: '=',
				numPerPage: '=',
				maxSize: '=',
				getRange: '&',
				getAll: "&"
			},
			controller: function($scope) {
				$scope.getRange()($scope.numPerPage, 0);

				var countButtons;
				var beginBtn;
				var endBtn;

				$scope.currentPage = 1;

				$scope.$watch('totalItems', function() {
					if ($scope.totalItems) {
						countButtons = Math.ceil($scope.totalItems / 10) * 10 / $scope.numPerPage;

						if (countButtons <= 1) {
							$scope.buttons = null;
							return;
						}

						$scope.buttons = [];

						for (var i = 0; i < countButtons; i++) {
							$scope.buttons.push({
								displacement: i * $scope.numPerPage,
								index: i + 1
							});
						}

						beginBtn = $scope.buttons[0].index;
						endBtn = $scope.buttons[$scope.buttons.length - 1].index;

						$scope.shownButtons = $scope.buttons.slice(0, $scope.maxSize);
					}
				});

				function showButtons() {
					var firstBtn = $scope.shownButtons[0].index;
					var lastBtn = $scope.shownButtons[$scope.shownButtons.length - 1].index;

					if (lastBtn < $scope.currentPage) {
						if ($scope.currentPage === endBtn) {
							var startPos = Math.ceil($scope.currentPage / $scope.maxSize);
							$scope.shownButtons = $scope.buttons.slice((startPos - 1) *
								$scope.maxSize, startPos * $scope.maxSize);
							return;
						}

						$scope.shownButtons = $scope.buttons.slice(lastBtn, lastBtn +
							$scope.maxSize);
					} else if (firstBtn > $scope.currentPage) {
						if ($scope.currentPage === beginBtn) {
							$scope.shownButtons = $scope.buttons.slice(0, $scope.maxSize);
							return;
						}

						$scope.shownButtons = $scope.buttons.slice($scope.currentPage -
							$scope.maxSize, $scope.currentPage);
					}
				}

				function generateTable(displacement) {
					$scope.getRange()($scope.numPerPage, displacement);
				}

				$scope.setPage = function(index, displacement) {
					if (index !== $scope.currentPage) {
						$scope.currentPage = index;
						showButtons();
						generateTable(displacement);
					}
				};

				$scope.previous = function() {
					if ($scope.currentPage > 1) {
						$scope.currentPage--;
						showButtons();
						generateTable($scope.buttons[$scope.currentPage - 1].displacement);
					}
				};

				$scope.next = function() {
					if ($scope.currentPage < $scope.buttons.length) {
						$scope.currentPage++;
						showButtons();
						generateTable($scope.buttons[$scope.currentPage - 1].displacement);
					}
				};

				$scope.prevRange = function() {
					var firstIndex = $scope.shownButtons[0].index;
					var prevBtn = $scope.buttons[firstIndex - 2];
					$scope.setPage(prevBtn.index, prevBtn.displacement);
				};

				$scope.nextRange = function() {
					var lastIndex = $scope.shownButtons[$scope.shownButtons.length - 1].index;
					var nextBtn = $scope.buttons[lastIndex];
					$scope.setPage(nextBtn.index, nextBtn.displacement);
				};
			}
		}
	});
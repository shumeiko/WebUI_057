angular
	.module('addForm')
	.directive('addForm', ["$state", "ModalService", function($state, ModalService) {
		return {
			restrict: 'EA',
			templateUrl: 'directives/add-form/add-form.html',
			scope: {
				resetOnSucces: '=',
				formData: '=',
				dependencies: '=',
				additions: '=',
				onSubmit: '&'
			},
			controller: function($scope) {

				$scope.formState = {
					formSubmited: false,
					formSubmitSend: false
				};

				var onSuccess = function(data) {
					ModalService.open({
						data: {
							title: "ssTest.SUCCESS",
							log: {
								logMsg: $scope.formData.txt + '.addedMsg',
								logValues: {name: $scope.formData.name}
							},
							action: {
								actionTxt: "ssTest.controls.continue",
								actionFn: function(){
									$state.go($scope.formData.state, {id: data.id});
								}
							}
						}
					});
					$scope.formState.formSubmitSend = false;
					if ($scope.resetOnSucces){
						resetForm();
					}
				};

				var onError = function(data) {
					ModalService.logError(data.response);
					$scope.formState.formSubmitSend = false;
				};

				var resetForm = function() {
					$scope.formState.formSubmitSend = false;
					if ($scope.dependencies) {
						for (var i = 0; i < $scope.dependencies.length; i++) {
							$scope.dependencies[i].id = '';
						}
					}
					if ($scope.additions) {
						for (var j = 0; j < $scope.additions.length; j++) {
							$scope.additions[j].id = '';
						}
					}
					$scope.formData.name = '';
					$scope.formState = {};
				};

				$scope.submit = function() {
					var entityData = {};
					entityData[$scope.formData.key] = $scope.formData.name;
					if ($scope.dependencies) {
						for (var i = 0; i < $scope.dependencies.length; i++) {
							entityData[$scope.dependencies[i].key] = $scope.dependencies[i].id;
						}
					}
					if ($scope.additions) {
						for (var j = 0; j < $scope.additions.length; j++) {
							entityData[$scope.additions[j].key] = $scope.additions[j].id;
						}
					}
					$scope.formState.formSubmitSend = true;
					$scope.onSubmit({entityData:entityData}).then(
						function(data) {
							if (data.response === "ok") {
								onSuccess(data);
							} else {
								onError(data);
							}
						},
						function(data) {
							onError(data);
						}
					);
				};

			}
		};
	}]);
angular.module('gridster')
	.controller('widgetTestInstallCtrl', ['$scope', 'AdminServices', 'data', '$modalInstance', function($scope, AdminServices, data, $modalInstance){
		$scope.formState = {};

		$scope.widget = data.widgetData;$scope.update = data.update;

		AdminServices.getGroups().then(function(groups) {
			$scope.groups = groups;
		});
		AdminServices.getFaculties().then(function(faculties) {
			$scope.faculties = faculties;
		});

		$scope.send = function(){
			$scope.formState.formSubmited = true;
		};
		$scope.cancel = function(){
			$modalInstance.close();
		};
		$scope.save = function(){
			data.onDone($scope.widget);
			$modalInstance.close();
		};
	}]);
angular.module('gridster')
	.directive('widgetTest', ['ModalService', function(ModalService){

		return {
			restrict: 'AE',
			scope: {
				widgetData: "="
			},
			templateUrl: "directives/widget-test/widget-test.html",
			link: function (scope, element, attrs) {

			},
			controller: function($scope){

			}

		};
	}]);
(function() {
	var baseURL = 'http://dtapi.local/';
	angular.module('ssTest')
		.constant('appConstants', {
			logInURL: baseURL + 'login/index',
			logOutURL: baseURL + 'login/logout',
			IsLoggedURL: baseURL + 'login/isLogged',

			getWidgetsURL: baseURL + 'widget/getRecords/',
			addWidgetURL: baseURL + 'widget/insertData/',
			editWidgetURL: baseURL + 'widget/update/',
			delWidgetURL: baseURL + 'widget/del/',

			getAdminsURL: baseURL + 'AdminUser/getRecords',
			addAdminURL: baseURL + 'AdminUser/insertData',
			editAdminURL: baseURL + 'AdminUser/update/',
			delAdminURL: baseURL + 'AdminUser/del/',

			getGroupsURL : baseURL + 'group/getRecords',
			getRangeOfGroupsURL: baseURL + 'group/getRecordsRange/',
			countGroupsURL: baseURL + 'group/countRecords',
			addGroupURL: baseURL + 'group/insertData',
			editGroupURL: baseURL + 'group/update/',
			delGroupURL: baseURL + 'group/del/',

			getSubjects: baseURL + 'subject/getRecords',
			getRangeOfSubjectsURL: baseURL + 'subject/getRecordsRange/',
			countSubjects: baseURL + 'subject/countRecords',
			addSubject: baseURL + 'subject/insertData',
			editSubject: baseURL + 'subject/update/',
			delSubject: baseURL + 'subject/del/',

			getSpecialities : baseURL + 'speciality/getRecords',
			getRangeOfSpecialitiesURL: baseURL + 'speciality/getRecordsRange/',
			countSpecialities: baseURL + 'speciality/countRecords',
			addSpeciality: baseURL + 'speciality/insertData',
			editSpeciality: baseURL + 'speciality/update/',
			delSpeciality: baseURL + 'speciality/del/',

			getFaculties: baseURL + 'faculty/getRecords',
			getRangeOfFacultiesURL: baseURL + 'faculty/getRecordsRange/',
			countFaculties: baseURL + 'faculty/countRecords',
			addFaculty: baseURL + 'faculty/insertData',
			editFaculty: baseURL + 'faculty/update/',
			delFaculty: baseURL + 'faculty/del/',

			getAnswersURL: baseURL + 'answer/getRecords',
			countAnswersURL: baseURL + 'answer/countRecords',
			addAnswerURL: baseURL + 'answer/insertData',
			getAnswersByQuestionURL: baseURL + 'answer/getAnswersByQuestion',
			editAnswerURL: baseURL + 'answer/update/',
			delAnswer: baseURL + 'answer/del/',

			getAnswersByQuestUserURL: baseURL + 'SAnswer/getAnswersByQuestion',
			checkAnswersURL: baseURL + 'SAnswer/checkAnswers',

			getQuestionsURL: baseURL + 'question/getRecords',
			getQuestionsByLevelRandURL : baseURL + 'question/getQuestionsByLevelRand',
			getRangeOfQuestionsByTestURL: baseURL + 'question/getRecordsRangeByTest/',
			countQuestionsByTestURL: baseURL + 'question/countRecordsByTest/',
			addQuestionURL: baseURL + 'question/insertData',
			editQuestionURL: baseURL + 'question/update/',
			countQuestions: baseURL + 'question/countRecords',
			delQuestion: baseURL + 'question/del/',

			getTestsURL: baseURL + 'test/getRecords',
			getRangeOfTestsURL: baseURL + 'test/getRecordsRange/',
			getTestsBySubjectURL: baseURL + 'test/getTestsBySubject',
			countTestsURL: baseURL + 'test/countRecords',
			addTestURL: baseURL + 'test/insertData',
			editTestURL: baseURL + 'test/update/',
			delTest: baseURL + 'test/del/',

			getTestDetailsURL: baseURL + 'testDetail/getRecords',
			getTestDetailsByTestURL: baseURL + 'TestDetail/getTestDetailsByTest',
			addTestDetailURL: baseURL + 'testDetail/insertData',
			editTestDetailURL: baseURL + 'testDetail/update/',
			delTestDetail: baseURL + 'testDetail/del/',

			addResultsURL: baseURL + 'result/insertData',
			getResultsURL: baseURL + 'result/getRecords',

			getStudentsURL: baseURL + 'student/getRecords',
			getRangeOfStudentsURL: baseURL + 'student/getRecordsRange/',
			countStudentsURL: baseURL + 'student/countRecords',
			delStudentsURL: baseURL + 'student/del/',
			addUserURL: baseURL + 'student/insertData',
			getStudentsByGroupURL: baseURL + 'student/getStudentsByGroup',

			getStudentResultUrl: baseURL + 'result/getRecords',
			delStudentResultURL: baseURL + 'result/del/',
			getResultsByStudentURL: baseURL + 'result/getRecordsByStudent',

			getSchedule: baseURL + 'timeTable/getRecords',
			addSchedule: baseURL + 'timeTable/insertData',
			getScheduleForSubject: baseURL + 'timeTable/getTimeTablesForSubject',
			delSchedule: baseURL + 'timeTable/del/',
			editSchedule: baseURL + 'timeTable/update/',
			getTimeTablesForGroupURL: baseURL + 'timeTable/getTimeTablesForGroup',
			getTimeTablesFromNowInMonthURL: baseURL + 'timeTable/getTimeTablesFromNowInMonth',

			getEntityValuesURL: baseURL + 'EntityManager/getEntityValues',

			getTimeStampURL: baseURL + 'testPlayer/getTimeStamp',
			startTestURL: baseURL + 'log/startTest',
			getLogsByUserURL: baseURL + 'log/getLogsByUser',
			getLogsURL: baseURL + 'log/getRecords'
		})
		.constant('questionsTypes', {
			simple: '1',
			multiple: '2'
		})
		.constant('keyCodes', {
				ENTER: 13,
				ESCAPE: 27,
				SPACE: 32,
				PAGE_UP: 33,
				PAGE_DOWN: 34,
				END: 35,
				HOME: 36,
				LEFT_ARROW : 37,
				UP_ARROW : 38,
				RIGHT_ARROW : 39,
				DOWN_ARROW : 40,
				TAB : 9,
				BACKSPACE: 8,
				DELETE: 46
		})
		.constant('entityNames', {
			question: "Question",
			answer: "Answer"
		})

})();
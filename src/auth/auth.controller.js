angular
	.module("authorization")
	.controller("AuthorizationCtrl", ["$scope", "$state", "logService",
		function($scope, $state, logService) {
			$scope.dataLoading = false;
			$scope.invalidData = false;

			$scope.getError = function(error) {
				if (angular.isDefined(error)) {
					if (error.required) {
						return "ssTest.auth.setData";
					} else if (error.minlength) {
						return "ssTest.auth.minLength";
					}
				}
			};

			$scope.login = function() {
				var authData = {
					username: $scope.userName,
					password: $scope.password
				};

				$scope.dataLoading = true;

				logService.logIn(authData).then(function(data) {
					$scope.checkResponse(data)
				});
			};

			$scope.checkResponse = function(data) {
				if (data.response === "ok" && data.roles[1] === 'admin' ) {
					$scope.invalidData = false;
					$state.go('admin.home');
				}
				else if (data.response === "ok" && data.roles[1] === 'student' ){
					localStorage.nameForUser = data.username;
					localStorage.idForUser = data.id;
					$scope.invalidData = false;
					$state.go('user.home');
				}
				else {
					$scope.dataLoading = false;
					$scope.invalidData = true;
				}
			};
	}]);
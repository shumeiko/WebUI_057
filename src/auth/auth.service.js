angular
	.module('authorization')
	.factory('AuthService', ['$http', 'appConstants', function($http, appConstants) {
		return {
			logIn: function(personalData) {
				return $http.post(appConstants.logInURL, personalData)
					.then(
					function(response) {
						return response.data;
					},
					function(error) {
						return error.data;
					}
				);
			},
			logOut: function(){
				return $http.get(appConstants.logOutURL)
					.then(
					function(response) {
						return response.data;
					},
					function(error) {
						return error.data;
					}
				);
			},
			isLoggedIn: function() {
				return $http.get(appConstants.IsLoggedURL)
					.then(
					function(response) {
						return response.data;
					},
					function(error) {
						return error.data;
					}
				);
			}
		};
	}]);

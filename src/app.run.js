angular.module('ssTest')
	.run(['$rootScope', 'logService', function ($rootScope, logService ) {
		$rootScope.$on('$stateChangeStart', function (event, toState, toParams) {
			if (toState.name === 'auth') return;
			return logService.checkLogged({
				event: event,
				toState: toState,
				toParams: toParams
			});
		});
	}]);
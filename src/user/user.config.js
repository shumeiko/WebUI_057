angular
    .module('user')
    .config(['$urlRouterProvider', '$stateProvider', function($urlRouterProvider, $stateProvider) {
    $urlRouterProvider.otherwise('/');
    $stateProvider
        .state('user.home', {
            url: '/',
            templateUrl: "components/home-user/home-user.html"
        })
        .state('user.user-test', {
            url: '/tests',
            templateUrl: "components/user-test/user-test.html"
        })
		.state('user.start-test', {
			url: '/start-test/:id',
			templateUrl: "components/test-player/start-test.html"
		})
		.state('user.finish-test', {
			url: '/finish-test/:id',
			templateUrl: "components/test-player/finish-test.html"
		})
		.state('user.help', {
			url: '/help',
			templateUrl: "components/help/help.html"
		})
		.state('user.profile', {
			url: '/profile/:id',
			templateUrl: "components/profile/profile.html"
		})
		.state('user.test', {
			url: '/test/:id',
			templateUrl: "components/test-player/test-player.html"
		})
		.state('user.results', {
			url: '/results',
			templateUrl: "components/student-result/student-result.html"
		})
		.state('user.resultDetail', {
			url: '/resultDetail/:id',
			templateUrl: "components/result-detail/result-detail.html"
		})
		.state('user.info', {
			url: '/info',
			templateUrl: "components/info/info.html"
		})
}]);

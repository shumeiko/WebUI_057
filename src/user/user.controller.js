angular
	.module('user')
	.controller("UserNavCtrl", ["$scope", "$state", function($scope, $state) {
		$scope.links = [
			{name: 'test', state: 'user.user-test'},
			{name: 'result', state: 'user.results'}
		];

		$scope.$state = $state;
	}]);
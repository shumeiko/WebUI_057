angular
	.module('user')
	.factory('UserService', ["$http", "appConstants", "HttpHelper", function($http, appConstants, HttpHelper) {
		return {

			getSubjects: function(id) {
				id = id ? '/' + id : '';
				return HttpHelper($http.get(appConstants.getSubjects + id))
			},
			getTestsBySubject: function(id) {
				return HttpHelper($http.get(appConstants.getTestsBySubjectURL + '/' + id))
			},
			getStudents: function(id) {
				id = id ? '/' + id : '';
				return HttpHelper($http.get(appConstants.getStudentsURL + id));
			},
			getStudentResults: function(id) {
				id = id ? '/' + id : '';
				return HttpHelper($http.get(appConstants.getStudentResultUrl + id));
			},
			getResultsByStudent: function(id) {
				return HttpHelper($http.get(appConstants.getResultsByStudentURL + '/' + id));
			},
			getTests: function(id) {
				id = id ? '/' + id : '';
				return HttpHelper($http.get(appConstants.getTestsURL + id));
			},
			getTimeTablesForGroup: function(id) {
				return HttpHelper($http.get(appConstants.getTimeTablesForGroupURL + '/' + id));
			},
			getGroups: function(id) {
				id = id ? '/' + id : '';
				return HttpHelper($http.get(appConstants.getGroupsURL + id));
			},
			getEntityValues: function(entity, idArr) {
				var input = {
					entity: entity,
					ids: idArr
				};
				return HttpHelper($http.post(appConstants.getEntityValuesURL, input));
			}

		}

	}]);
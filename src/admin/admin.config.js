angular
	.module('admin')
	.config(['$urlRouterProvider', '$stateProvider', function($urlRouterProvider, $stateProvider) {
		$urlRouterProvider.otherwise('/');
		$stateProvider
			.state('admin.home', {
				url: '/',
				templateUrl: "components/home-admin/home-admin.html",
				breadcrumb: {
					label: 'ssTest.menu.main'
				}
			})
			.state('admin.register', {
				url: '/register',
				templateUrl: "components/register-admin/register-admin.html",
				breadcrumb: {
					label: 'ssTest.controls.registration',
					parent: 'admin.admins'
				}
			})
			.state('admin.admins', {
				url: '/admins',
				templateUrl: "components/admins/admins.html",
				breadcrumb: {
					label: 'ssTest.components.homeAdmin.sideBar.admins',
					parent: 'admin.home'
				}
			})
			.state('admin.editAdmin', {
				url: '/editAdmin/:id',
				templateUrl: "components/register-admin/register-admin.html",
				breadcrumb: {
					label: 'ssTest.controls.edit',
					parent: 'admin.admins'
				}
			})
			.state('admin.groups', {
				url: '/groups',
				templateUrl: "components/groups/groups.html",
				breadcrumb: {
					label: 'ssTest.menu.groups'
				}
			})
			.state('admin.subjects', {
				url: '/subjects',
				templateUrl: "components/subjects/subjects.html",
				breadcrumb: {
					label: 'ssTest.menu.subjects'
				}
			})
			.state('admin.specialities', {
				url: '/specialities',
				templateUrl: "components/specialities/specialities.html",
				breadcrumb: {
					label: 'ssTest.menu.specialities'
				}
			})
			.state('admin.faculties', {
				url: '/faculties',
				templateUrl: "components/faculties/faculties.html",
				breadcrumb: {
					label: 'ssTest.menu.faculties'
				}
			})
			.state('admin.addGroup', {
				url: '/addGroup',
				templateUrl: "components/add-group/add-group.html",
				breadcrumb: {
					label: 'ssTest.controls.add',
					parent: 'admin.groups'
				}
			})
			.state('admin.addSubject', {
				url: '/addSubject',
				templateUrl: "components/add-subject/add-subject.html",
				breadcrumb: {
					label: 'ssTest.controls.add',
					parent: 'admin.subjects'
				}
			})
			.state('admin.addSpeciality', {
				url: '/addSpeciality',
				templateUrl: "components/add-speciality/add-speciality.html",
				breadcrumb: {
					label: 'ssTest.controls.add',
					parent: 'admin.specialities'
				}
			})
			.state('admin.addFaculty', {
				url: '/addFaculty',
				templateUrl: "components/add-faculty/add-faculty.html",
				breadcrumb: {
					label: 'ssTest.controls.add',
					parent: 'admin.faculties'
				}
			})
			.state('admin.editGroup', {
				url: '/editGroup/:id',
				templateUrl: "components/edit-group/edit-group.html",
				breadcrumb: {
					label: 'ssTest.controls.edit',
					parent: 'admin.groups'
				}
			})
			.state('admin.editSubject', {
				url: '/editSubject/:id',
				templateUrl: "components/edit-subject/edit-subject.html",
				breadcrumb: {
					label: 'ssTest.controls.edit',
					parent: 'admin.subjects'
				}
			})
			.state('admin.editSpeciality', {
				url: '/editSpeciality/:id',
				templateUrl: "components/edit-speciality/edit-speciality.html",
				breadcrumb: {
					label: 'ssTest.controls.edit',
					parent: 'admin.specialities'
				}
			})
			.state('admin.editFaculty', {
				url: '/editFaculty/:id',
				templateUrl: "components/edit-faculty/edit-faculty.html",
				breadcrumb: {
					label: 'ssTest.controls.edit',
					parent: 'admin.faculties'
				}
			})
			.state('admin.tests', {
				url: '/tests/:id',
				templateUrl: "components/tests/tests.html",
				breadcrumb: {
					label: '{{subject.title}}',
					parent: 'admin.subjects',
					id: '{{subject.id}}'
				}
			})
			.state('admin.testsList', {
				url: '/tests',
				templateUrl: "components/tests/tests.html",
				breadcrumb: {
					label: 'ssTest.components.tests.label',
					parent: 'admin.home'
				}
			})
			.state('admin.addTest', {
				url: '/addTests/:id',
				templateUrl: "components/add-test/add-test.html",
				breadcrumb: {
					label: 'ssTest.controls.add',
					parent: 'admin.tests'
				}
			})
			.state('admin.questions', {
				url: '/questions/:id',
				templateUrl: "components/questions/questions.html",
				breadcrumb: {
					label: '{{test.title}}',
					parent: 'admin.tests',
					id: '{{test.id}}'
				}
			})
			.state('admin.addQuestion', {
				url: '/addQuestion/:id',
				templateUrl: "components/add-question/add-question.html",
				breadcrumb: {
					label: 'ssTest.controls.add',
					parent: 'admin.questions'
				}
			})
			.state('admin.addAnswer', {
				url: '/addAnswer/:id',
				templateUrl: "components/add-answer/add-answer.html",
				breadcrumb: {
					label: 'ssTest.controls.add',
					parent: 'admin.answers'
				}
			})
			.state('admin.detailOfTest', {
				url: '/detailOfTest/:id',
				templateUrl: "components/test-detail/test-detail.html",
				breadcrumb: {
					label: 'ssTest.controls.settings',
					parent: 'admin.questions',
					id: '{{testDetailId}}'
				}
			})
			.state('admin.editDetailOfTest', {
				url: '/editDetailOfTest/:id',
				templateUrl: "components/edit-test-detail/edit-test-detail.html",
				breadcrumb: {
					label: 'ssTest.controls.edit',
					parent: 'admin.detailOfTest'
				}
			})
			.state('admin.addTestDetail', {
				url: '/addTestDetail/:id',
				templateUrl: "components/add-test-detail/add-test-detail.html",
				breadcrumb: {
					label: 'ssTest.controls.add',
					parent: 'admin.detailOfTest'
				}
			})
			.state('admin.charts', {
				url: '/charts',
				templateUrl: "components/charts/charts.html"
			})
			.state('admin.studentResult', {
				url: '/studentResult/:id',
				templateUrl: "components/student-result/student-result.html",
				breadcrumb: {
					label: '{{student.title}}',
					parent: 'admin.students',
					id: '{{student.id}}'
				}
			})
			.state('admin.resultDetail', {
				url: '/resultDetail/:id',
				templateUrl: "components/result-detail/result-detail.html",
				breadcrumb: {
					label: '{{session}}',
					parent: 'admin.studentResult'
				}
			})
			.state('admin.students', {
				url: '/students/:id',
				templateUrl: "components/students/students.html",
				breadcrumb: {
					label: '{{group.title}}',
					parent: 'admin.groups',
					id: '{{group.id}}'
				}
			})
			.state('admin.studentsList', {
				url: '/students',
				templateUrl: "components/students/students.html",
				breadcrumb: {
					label: 'ssTest.components.students.label',
					parent: 'admin.home'
				}
			})
			.state('admin.answers', {
				url: '/answers/:id',
				templateUrl: "components/answers/answers.html",
				breadcrumb: {
					label: '{{question.title}}',
					parent: 'admin.questions',
					id: '{{question.id}}'
				}
			})
			.state('admin.schedule', {
				url: '/schedule/:id',
				templateUrl: "components/schedule/schedule.html",
				breadcrumb: {
					label: 'Розклад',
					parent: 'admin.tests',
					id: '{{scheduleId}}'
				}
			})
			.state('admin.addSchedule', {
				url: '/addSchedule/:id',
				templateUrl: "components/add-edit-schedule/add-edit-schedule.html",
				breadcrumb: {
					label: 'ssTest.controls.add',
					parent: 'admin.schedule'
				}
			})
			.state('admin.registerUser', {
				url: '/registerUser/:id',
				templateUrl: "components/register-user/register-user.html",
				breadcrumb: {
					label: 'ssTest.controls.registration',
					parent: 'admin.students'
				}
			})
			.state('admin.editTest', {
				url: '/editTest/:id',
				templateUrl: "components/edit-test/edit-test.html",
				breadcrumb: {
					label: 'ssTest.controls.edit',
					parent: 'admin.tests'
				}
			})
			.state('admin.editAnswer', {
				url: '/editAnswer/:id',
				templateUrl: "components/edit-answer/edit-answer.html",
				breadcrumb: {
					label: 'ssTest.controls.edit',
					parent: 'admin.answers'
				}
			})
			.state('admin.editQuestion', {
				url: '/editQuestion/:id',
				templateUrl: "components/edit-question/edit-question.html",
				breadcrumb: {
					label: 'ssTest.controls.edit',
					parent: 'admin.questions'
				}
			})
			.state('admin.editSchedule', {
				url: '/editSchedule/:id',
				templateUrl: "components/add-edit-schedule/add-edit-schedule.html",
				breadcrumb: {
					label: 'ssTest.controls.edit',
					parent: 'admin.schedule'
				}
			})
	}]);
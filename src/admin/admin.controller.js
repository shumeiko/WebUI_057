angular.module('admin')
	.controller('AdminCtrl', ['$scope', 'logService', '$state', 'ModalService',
		function($scope, logService, $state, ModalService) {
			$scope.navCollapsed = true;

			$scope.links = [
				{name: 'ssTest.menu.groups', state: '.groups', checkArr: [
					'groups', 'addGroup', 'editGroup', 'students', 'registerUser'
				]},
				{name: 'ssTest.menu.subjects', state: '.subjects', checkArr: [
					'subjects', 'addSubject', 'editSubject',
					'tests', 'addTest', 'editTest', 'schedule',
					'questions', 'addQuestion', 'editQuestion',
					'detailOfTest', 'addTestDetail', 'editDetailOfTest',
					'answers', 'addAnswer', 'editAnswer'
				]},
				{name: 'ssTest.menu.specialities', state: '.specialities', checkArr: [
					'specialities', 'addSpeciality', 'editSpeciality'
				]},
				{name: 'ssTest.menu.faculties', state: '.faculties', checkArr: [
					'faculties', 'addFaculty', 'editFaculty'
				]}
			];

			$scope.isActive = function(arr) {
				for (var i = 0; i < arr.length; i++) {
					var result = $state.includes('admin.' + arr[i]);

					if (result) {
						return result;
					}
				}
			};

			$scope.exit = function() {
				ModalService.open({
					data: {
						title: "ssTest.ATTENTION",
						log: {
							logMsg: 'ssTest.menu.modalWindows.exit'
						},
						action: {
							actionTxt: "ssTest.controls.yes",
							actionFn: function(){
								logService.logOut();
							}
						}
					},
					options: {
						size: 'sm'
					}
				});
			};
		}
	]);
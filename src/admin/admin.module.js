angular
	.module('admin', [
		'ui.router',
		'addForm',
		'subject',
		'faculty',
		'speciality',
		'group',
		'calendar',
		'studentResult',
		'resultDetail',
		'tests',
		'addTest',
		'addQuestion',
		'answers',
		'questions',
		'testParam',
		'student',
		'gridster',
		'schedule',
		'addAnswer',
		'addTestDetail',
		'ngImgCrop'
	]);


angular
	.module('admin')
	.factory('AdminServices', ['$http', 'appConstants', 'HttpHelper', '$q', function($http, appConstants, HttpHelper, $q) {
		var fakeReq = function(data) {
			var deferred = $q.defer();
			setTimeout(function() {
				deferred.resolve(data);
			}, 2000);
			return deferred.promise;
		};
		return {
			getAdmins: function(id) {
				id = id ? '/' + id : '';
				return HttpHelper($http.get(appConstants.getAdminsURL + id));
			},
			addAdmin: function(data) {
				return HttpHelper($http.post(appConstants.addAdminURL, data));
			},
			editAdmin: function(data, id) {
				return HttpHelper($http.post(appConstants.editAdminURL + id, data));
			},
			delAdmin: function(id) {
				return HttpHelper($http.get(appConstants.delAdminURL + id));
			},

			getWidgets: function(id) {
				return HttpHelper($http.get(appConstants.getWidgetsURL + (id ? id : '')));
			},
			addWidget: function(data) {
				return HttpHelper($http.post(appConstants.addWidgetURL, data));
			},
			editWidget: function(data, id) {
				return HttpHelper($http.post(appConstants.editWidgetURL + id, data));
			},
			deleteWidget: function(id) {
				return HttpHelper($http.get(appConstants.delWidgetURL + id));
			},

			getGroups: function(id) {
				id = id ? '/' + id : '';
				return HttpHelper($http.get(appConstants.getGroupsURL + id));
			},
			getRangeOfGroups: function(quant, displ) {
				return HttpHelper($http.get(appConstants.getRangeOfGroupsURL + quant + '/' + displ));
			},
			countGroups: function() {
				return HttpHelper($http.get(appConstants.countGroupsURL));
			},
			addGroup: function(data) {
				return HttpHelper($http.post(appConstants.addGroupURL, data));
			},
			editGroup: function(data, id) {
				return HttpHelper($http.post(appConstants.editGroupURL + id, data));
			},
			delGroup: function(id) {
				return HttpHelper($http.get(appConstants.delGroupURL + id));
			},

			getSubjects: function(id) {
				id = id ? '/' + id : '';
				return HttpHelper($http.get(appConstants.getSubjects + id));
			},
			getRangeOfSubjects: function(quant, displ) {
				return HttpHelper($http.get(appConstants.getRangeOfSubjectsURL + quant + '/' + displ));
			},
			countSubjects: function() {
				return HttpHelper($http.get(appConstants.countSubjects));
			},
			addSubject: function(data) {
				return HttpHelper($http.post(appConstants.addSubject, data));
			},
			editSubject: function(data, id) {
				return HttpHelper($http.post(appConstants.editSubject + id, data));
			},
			delSubject: function(id) {
				return HttpHelper($http.get(appConstants.delSubject + id));
			},

			getSpecialities: function(id) {
				id = id ? '/' + id : '';
				return HttpHelper($http.get(appConstants.getSpecialities + id));
			},
			getRangeOfSpecialities: function(quant, displ) {
				return HttpHelper($http.get(appConstants.getRangeOfSpecialitiesURL + quant + '/' + displ));
			},
			countSpecialities: function() {
				return HttpHelper($http.get(appConstants.countSpecialities));
			},
			addSpeciality: function(data) {
				return HttpHelper($http.post(appConstants.addSpeciality, data));
			},
			editSpeciality: function(data, id) {
				return HttpHelper($http.post(appConstants.editSpeciality + id, data));
			},
			delSpeciality: function(id) {
				return HttpHelper($http.get(appConstants.delSpeciality + id));
			},

			getFaculties: function(id) {
				id = id ? '/' + id : '';
				return HttpHelper($http.get(appConstants.getFaculties + id));
			},
			getRangeOfFaculties: function(quant, displ) {
				return HttpHelper($http.get(appConstants.getRangeOfFacultiesURL + quant + '/' + displ));
			},
			countFaculties: function() {
				return HttpHelper($http.get(appConstants.countFaculties));
			},
			addFaculty: function(data) {
				return HttpHelper($http.post(appConstants.addFaculty, data));
			},
			editFaculty: function(data, id) {
				return HttpHelper($http.post(appConstants.editFaculty + id, data));
			},
			delFaculty: function(id) {
				return HttpHelper($http.get(appConstants.delFaculty + id));
			},

			getTests: function(id) {
				id = id ? '/' + id : '';
				return HttpHelper($http.get(appConstants.getTestsURL + id));
			},
			getRangeOfTests: function(quant, displ) {
				return HttpHelper($http.get(appConstants.getRangeOfTestsURL + quant + '/' + displ));
			},
			getTestsBySubject: function(id) {
				return HttpHelper($http.get(appConstants.getTestsBySubjectURL + '/' + id));
			},
			countTests: function() {
				return HttpHelper($http.get(appConstants.countTestsURL));
			},
			addTest: function(data) {
				return HttpHelper($http.post(appConstants.addTestURL, data));
			},
			editTest: function(data, id) {
				return HttpHelper($http.post(appConstants.editTestURL + id, data));
			},
			delTest: function(id) {
				return HttpHelper($http.get(appConstants.delTest + id));
			},

			getTestDetails: function(id) {
				id = id ? '/' + id : '';
				return HttpHelper($http.get(appConstants.getTestDetailsURL + id));
			},
			addTestDetail: function(data) {
				return HttpHelper($http.post(appConstants.addTestDetailURL, data));
			},
			editTestDetail: function(data, id) {
				return HttpHelper($http.post(appConstants.editTestDetailURL + id, data));
			},
			delTestDetail: function(id) {
				return HttpHelper($http.get(appConstants.delTestDetail + id));
			},

			getQuestions: function(id) {
				id = id ? '/' + id : '';
				return HttpHelper($http.get(appConstants.getQuestionsURL + id));
			},
			getQuestionsByTest: function(id) {
				return HttpHelper($http.get(appConstants.countQuestionsByTestURL + id).then(
					function(response) {
						return $http.get(appConstants.getRangeOfQuestionsByTestURL + id + '/' + response.data.numberOfRecords + '/' + 0)
					}));
			},
			getRangeOfQuestionsByTest: function(id, quant, displ) {
				return HttpHelper($http.get(appConstants.getRangeOfQuestionsByTestURL + id + '/' + quant + '/' + displ));
			},
			countQuestions: function() {
				return HttpHelper($http.get(appConstants.countQuestions));
			},
			countQuestionsByTest: function(id) {
				return HttpHelper($http.get(appConstants.countQuestionsByTestURL + id));
			},
			addQuestion: function(data) {
				return HttpHelper($http.post(appConstants.addQuestionURL, data));
			},
			editQuestion: function(data, id) {
				return HttpHelper($http.post(appConstants.editQuestionURL + id, data));
			},
			delQuestion: function(id) {
				return HttpHelper($http.get(appConstants.delQuestion + id));
			},

			getAnswers: function(id) {
				id = id ? '/' + id : '';
				return HttpHelper($http.get(appConstants.getAnswersURL + id));
			},
			getAnswersByQuestion: function(id) {
				return HttpHelper($http.get(appConstants.getAnswersByQuestionURL + '/' + id));
			},
			countAnswers: function() {
				return HttpHelper($http.get(appConstants.countAnswersURL));
			},
			addAnswer: function(data) {
				return HttpHelper($http.post(appConstants.addAnswerURL, data));
			},
			editAnswer: function(data, id) {
				return HttpHelper($http.post(appConstants.editAnswerURL + id, data));
			},
			delAnswer: function(id) {
				return HttpHelper($http.get(appConstants.delAnswer + id));
			},

			getStudents: function(id) {
				id = id ? '/' + id : '';
				return HttpHelper($http.get(appConstants.getStudentsURL + id));
			},
			getRangeOfStudents: function(quant, displ) {
				return HttpHelper($http.get(appConstants.getRangeOfStudentsURL + quant + '/' + displ));
			},
			getStudentsByGroup: function(id) {
				return HttpHelper($http.get(appConstants.getStudentsByGroupURL + '/' + id));
			},
			countStudents: function() {
				return HttpHelper($http.get(appConstants.countStudentsURL));
			},
			delStudent: function(id) {
				return HttpHelper($http.get(appConstants.delStudentsURL + id));
			},
			registerUser: function(data) {
				return HttpHelper($http.post(appConstants.addUserURL, data));
			},
			getStudentResults: function(id) {
				id = id ? '/' + id : '';
				return HttpHelper($http.get(appConstants.getStudentResultUrl + id));
			},
			delStudentResult: function(id) {
				return HttpHelper($http.get(appConstants.delStudentResultURL + id));
			},
			getTestDetailsByTest: function(id) {
				return HttpHelper($http.get(appConstants.getTestDetailsByTestURL + '/' + id));
			},
			getResultsByStudent: function(id) {
				return HttpHelper($http.get(appConstants.getResultsByStudentURL + '/' + id));
			},
			getSchedule: function(id) {
				id = id ? '/' + id : '';
				return HttpHelper($http.get(appConstants.getSchedule + id));
			},
			addSchedule: function(data) {
				return HttpHelper($http.post(appConstants.addSchedule, data));
			},
			getScheduleForSubject: function(id) {
				id = id ? '/' + id : '';
				return HttpHelper($http.get(appConstants.getScheduleForSubject + id));
			},
			delSchedule: function(id) {
				return HttpHelper($http.get(appConstants.delSchedule + id));
			},
			getTimeTablesFromNowInMonth: function() {
				return HttpHelper($http.get(appConstants.getTimeTablesFromNowInMonthURL));
			},
			editSchedule: function(data, id) {
				return HttpHelper($http.post(appConstants.editSchedule + id, data));
			},
			getEntityValues: function(entity, idArr) {
				var input = {
					entity: entity,
					ids: idArr
				};
				return HttpHelper($http.post(appConstants.getEntityValuesURL, input));
			},
			getUniqueIdsList: function(entityData, propName) {
				var i;
				var l;
				var id;
				var idList = [];

				for (i = 0, l = entityData.length; i < l; i++) {
					id = entityData[i][propName];
					if (idList.indexOf(id) === -1) {
						idList.push(id);
					}
				}
				return idList;
			}
		};
	}])
	.factory('WidgetsList', [function() {
		return [
			{
				type: 'widget-students-rating'
			},
			{
				type: 'widget-students'
			},
			{
				type: 'widget-tests'
			},
			{
				type: 'widget-subjects'
			},
			{
				type: 'widget-groups'
			},
			{
				type: 'widget-calculator'
			}
		];
	}])
	.factory('StudentProvider', ['AdminServices', function(AdminServices) {
		return {
			getResultsDataByStudent: function(studentId) {
				return AdminServices.getResultsByStudent(studentId).then(function(testsResults) {

					if (Array.isArray(testsResults[0])) return false;

					var testsIdList = AdminServices.getUniqueIdsList(testsResults, 'test_id');
					return AdminServices.getEntityValues('Test', testsIdList).then(function(tests) {
						var i;
						var j;
						var subject;
						var testId;
						var score;
						var testName;
						var testsDataForTestsResults = {};
						var scoreListsBySubjects = {};

						for (i = 0; i < tests.length; i++) {
							testsDataForTestsResults[tests[i].test_id] = {
								subjectId: tests[i].subject_id,
								testName: tests[i].test_name
							};
						}

						for (j = 0; j < testsResults.length; j++) {
							testId = testsResults[j].test_id;
							score = testsResults[j].result;
							subject = testsDataForTestsResults[testId].subjectId;
							testName = testsResults[j].testName = testsDataForTestsResults[testId].testName;
							testsResults[j].subjectId = subject;

							if (scoreListsBySubjects[subject]) {
								scoreListsBySubjects[subject][testName] = score;
							} else {
								scoreListsBySubjects[subject] = {};
								scoreListsBySubjects[subject][testName] = score;
							}
						}

						return {
							testsResults: testsResults,
							scoreListsBySubjects: scoreListsBySubjects
						};
					});
				})
			},
			getRating: function(ratingListBySubjects) {
				var ratingBySubject;
				var sumRatings = 0;
				var count = 0;

				for (ratingBySubject in ratingListBySubjects) {
					sumRatings += +ratingListBySubjects[ratingBySubject];
					count++;
				}
				return sumRatings ? (sumRatings / count).toFixed() : 0;
			},
			getChartMarks: function(marks, legend, obj3) {
				var chartMarks = [];
				var chartMark;
				var item;

				if (!marks) return false;

				for (item in marks) {
					chartMark = {
						name: legend ? legend[item] : item,
						mark: marks[item],
						childData: obj3 ? obj3[item] : false
					};
					chartMarks.push(chartMark);
				}
				return chartMarks;
			}
		}
	}]);

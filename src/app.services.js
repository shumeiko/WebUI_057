angular
	.module("ssTest")
	.factory("ModalService", ["$rootScope", "$modal", "LogError", "$sce", "$compile",
		function($rootScope, $modal, LogError, $sce, $compile) {

			var defaults = {
				animation: true,
				templateUrl: "components/modal/modal.html",
				controller: "ModalCtrl"
			};

			var open = function(modalData) {
				var modal;
				var options;
				var modalOptions;

				if (!modalData) {
					return;
				} else if (angular.isString(modalData)) {
					modalData = {
						data: {
							log: {
								logMsg: modalData
							}
						}
					}
				} else if (angular.isArray(modalData)) {
					modalData = {
						data: {
							title: modalData[0],
							log: {
								logMsg: modalData[1]
							}
						}
					}
				} else if (angular.isObject(modalData)) {

					var hasHtml;
					var el;
					modalOptions = modalData.options;

					try {
						hasHtml = !!modalData.data.log.logHtml;
					} catch (e) {
						hasHtml = false;
					}

					if (hasHtml) {
						//prepare placeholder
						var placeholder = $sce.trustAsHtml("<div id=" + placeholderId + "></div>");
						//compile html str send to modal
						var compiled = $compile(modalData.data.log.logHtml);
						//apply scope
						el = compiled(modalData.data.log.scope || $rootScope);
						//append element to placeholder
						modalData.data.log.logHtml = placeholder;
					}

				}

				options = angular.extend({}, defaults, modalOptions);

				options.resolve = {
					data: function() {
						return modalData.data;
					}
				};

				modal = $modal.open(options);

				if (hasHtml) {
					modal.rendered.then(
						function() {
							angular.element(document.getElementById(placeholderId)).append(el);
						}
					);
				}

				return modal;
			};

			return {
				open: open,
				logError: function(error, modalOpts) {
					return open({
						data: {
							title: "ssTest.ERROR",
							log: LogError(error)
						}
					}, modalOpts);
				},
				close: function(result) {
					return $modal.close(result);
				},
				dismiss: function(reason) {
					return $modal.close(reason);
				}
			};
		}
	])
	.factory("LogError", ["$translate", function($translate) {
		var errReg = /^error\s[\w]{1,5}$/;
		var permissionsReg = /^You don't have permissions/;
		var errorsTxt = "ssTest.responseErrors.e";
		var errorUnknown = errorsTxt + "Unknown";

		return function(str) {
			if (errReg.test(str)) {
				var errorNum = str.replace(/.*\s/, "");
				var errorCode = errorsTxt + errorNum;
				if ($translate.instant(errorCode) !== errorCode) {
					return {
						logMsg: errorCode
					};
				} else {
					return {
						logMsg: errorUnknown,
						logValues: {error: str}
					};
				}
			} else if (permissionsReg.test(str)) {
				return {
					logMsg: errorsTxt + "Access"
				};
			} else {
				return {
					logMsg: errorUnknown,
					logValues: {error: str}
				};
			}
		};
	}])
	.factory('logService', ['AuthService', '$state', function(AuthService, $state) {
		var logged = false;
		var toAuth = function() {
			logged = false;
			$state.go('auth');
		};
		return {
			checkLogged: function(stateParams) {
				var paramsObject = angular.isObject(stateParams);
				if (stateParams === true || !logged) {
					if (paramsObject) {
						stateParams.event.preventDefault();
					}
					AuthService.isLoggedIn().then(
						function(data) {
							if ((data.response === 'logged' && data.roles[1] === 'admin'
								&& stateParams.toState.name.indexOf('admin') !== -1) ||
								(data.response === 'logged' && data.roles[1] === 'student'
								&& stateParams.toState.name.indexOf('user') !== -1)) {
								if (paramsObject) {
									logged = true;
									$state.go(stateParams.toState, stateParams.toParams);
								}
							} else {
								toAuth();
							}
						},
						function() {
							toAuth();
						}
					);
				}
			},
			logOut: function(){
				AuthService.logOut().then(
					function() {
						logged = false;
						toAuth();
					},
					function() {
						logged = false;
						toAuth();
					}
				);
			},
			logIn: function(personalData){
				return AuthService.logIn(personalData).then(
					function(data){
						logged = true;
						return data;
					},
					function(data){
						return data;
					}
				);
			}
		};
	}])
	.factory('HttpHelper', ['logService', '$q', function(logService, $q) {
		return function(request){
			return request.then(
				function(response) {
					return response.data;
				},
				function(error) {
					if (error.status === 403) {
						logService.checkLogged(true);
						return $q.reject(error.data);
					}
					return error.data;
				}
			);
		};
	}])
	.factory('CaseConverter', [function(){
		var snake = 'snake';
		var spinal = 'spinal';
		var camel = 'camel';

		var getConverter = function (str, toType) {
			var replaceFn;
			var reg;
			var fromType;
			var searchStr;

			if (/_/g.test(str)) {
				fromType = snake;
				searchStr = "_\\w";
			} else  if (/-/g.test(str)) {
				fromType = spinal;
				searchStr = "-\\w";
			} else {
				fromType = camel;
				searchStr = "[A-Z]";
			}

			if (toType === fromType) {
				return function (str) {
					return str;
				};
			}

			reg = new RegExp(searchStr, 'g');

			if (fromType === snake) {
				if (toType === spinal) {
					replaceFn = function (match) {
						return "-" + match[1];
					}
				} else if (toType === camel) {
					replaceFn = function (match) {
						return match[1].toUpperCase();
					}
				}
			} else if (fromType === spinal) {
				if (toType === snake) {
					replaceFn = function (match) {
						return "_" + match[1];
					}
				} else if (toType === camel) {
					replaceFn = function (match) {
						return match[1].toUpperCase();
					}
				}
			} else {
				if (toType === spinal) {
					replaceFn = function (match) {
						return "-" + match.toLowerCase();
					}
				} else if (toType === snake) {
					replaceFn = function (match) {
						return "_" + match.toLowerCase();
					}
				}
			}

			//return str.replace(reg, replaceFn);
			return function (str) {
				return str.replace(reg, replaceFn);
			};
		};

		var convert = function (input, toType, skipString) {
			var convObj;

			if (!skipString) {
				var converter;
			}

			if (input == null) {
				return input;
			}

			if (Array.isArray(input)){
				convObj = [];
				for (var i = 0, len = input.length; i < len; i++) {
					var val = input[i];
					convObj[i] = convert(val, toType, true);
				}
			} else if (typeof input === 'object') {
				(function() {
					convObj = {};
					for (var key in input) {
						//noinspection JSUnfilteredForInLoop
						if (!input.hasOwnProperty(key)) {
							continue;
						}
						var val = input[key];
						if (!converter) {
							converter = getConverter(key, toType);
						}
						convObj[converter(key)] = convert(val, toType, true);
					}
				})();
			} else if (!skipString && typeof input === 'string') {
				if (!converter) {
					converter = getConverter(input, toType);
				}
				convObj = converter(input)
			} else {
				convObj = input
			}
			return convObj;
		};

		return {
			toSnake: function (input) {
				return convert(input, 'snake');
			},
			toSpinal: function (input) {
				return convert(input, 'spinal');
			},
			toCamel: function (input) {
				return convert(input, 'camel');
			}
		}
	}]);
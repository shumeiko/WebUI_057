angular
	.module('ssTest')
	.config(['$urlRouterProvider', '$stateProvider', function($urlRouterProvider, $stateProvider) {
		$urlRouterProvider.otherwise('/');
		$stateProvider
			.state('auth', {
				url: '/',
				templateUrl: 'auth/view.html'
			})
			.state('admin', {
				url: '/admin',
				templateUrl: 'admin/view.html'
			})

			.state('user', {
				url: '/user',
				templateUrl: 'user/view.html'
			})
	}])
	.config(['$translateProvider', function ($translateProvider) {
		$translateProvider
			.useSanitizeValueStrategy('escaped')
			.useStaticFilesLoader({
				prefix: 'langs/lang-',
				suffix: '.json'
			})
			.preferredLanguage('uk')
			.useCookieStorage();
	}])
	.config(['$animateProvider', function ($animateProvider) {
		$animateProvider.classNameFilter(/^((?!(btn)).)*$/);
	}]);
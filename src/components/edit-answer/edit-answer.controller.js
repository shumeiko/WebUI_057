angular
	.module('admin')
	.controller('EditAnswerCtrl', ['$scope', '$filter', 'ModalService', '$stateParams', 'AdminServices','fileReader',
		function($scope, $filter, ModalService, $stateParams, AdminServices, fileReader) {;
			AdminServices.getAnswers($stateParams.id).then(function(data) {
				$scope.answer = data[0];
				$scope.imageSrc = $scope.answer.attachment;
				$scope.questionId = $scope.answer.question_id;
				$scope.answer.answer_text = $filter('decode')($scope.answer.answer_text);
			});
			$scope.formSend = function(){
				$scope.formSubmited = true
			};
			var input = "";
			var reader = new FileReader();
			$scope.readFile = function() {
				var file = document.querySelector('input[type=file]').files[0];
				var preview = document.querySelector('img');
				var b64file = {};
				reader.onload = function(event) {
					b64file = event.target.result;
					input = b64file;
				};
				if (file) {
					reader.readAsDataURL(file);
				}
			};
			$scope.previewFile = function(){
				fileReader.readAsDataUrl($scope.file, $scope)
					.then(function(result) {
						$scope.imageSrc = result;
					});
			};

			var onSuccess = function(data) {
				ModalService.open({
					data: {
						title: "ssTest.SUCCESS",
						log: {
							logMsg: "ssTest.components.editAnswer.editedMsg",
							logValues: {name: $scope.answer.answer_text}
						}
					}
				});
				$scope.formSubmited = false;
				$scope.formSubmitSend = false;
			};

			var onError = function(data) {
				ModalService.logError(data.response);
				$scope.formSubmited = false;
				$scope.formSubmitSend = false;
			};

			$scope.editAnswer = function() {
				$scope.answer.attachment = input;
				$scope.formSubmitSend = true;

				AdminServices.editAnswer($scope.answer, $stateParams.id).then(
					function(data){
						if (data.response === "ok") {
							onSuccess(data);
						} else {
							onError(data);
						}
					},
					function(data) {
						onError(data);
					}
				);

			};

	}]);
angular.module('group', [])
	.controller('GroupsListCtrl', ['$scope', '$q', '$timeout', '$cacheFactory', 'AdminServices',
		function($scope, $q, $timeout, $cacheFactory, AdminServices) {
			$scope.groupHead = [
				{
					title: 'ssTest.components.groups.group',
					propertyName: 'group_name',
					sort: true
				},
				{
					title: 'ssTest.components.groups.speciality',
					propertyName: 'specialityName',
					sort: true
				},
				{
					title: 'ssTest.components.groups.faculty',
					propertyName: 'facultyName',
					sort: true
				}
			];

			$scope.groupOptions = {
				entityId: "group_id",
				state: 'admin.students',
				editState: 'admin.editGroup',
				delNameService: 'delGroup',
				pagination: true,
				itemsPerPage: 10,
				getRange: getRange,
				getAll: getAll
			};

			function getRange(quant, displ) {
				AdminServices.getRangeOfGroups(quant, displ).then(function(groups) {
					if (Array.isArray(groups[0])) $scope.empty = true;
					$scope.groupsList = bindAllIdWithName(groups);
					$scope.groupOptions.numItem = displ;
					$scope.loaded = true;
				});
			}

			function getAll() {
				$scope.groupOptions.pagination = false;
				AdminServices.getGroups().then(function(groups) {
					$scope.groupsList = bindAllIdWithName(groups);
				});
			}

			function bindAllIdWithName(groups) {
				var idLists = {
					faculty: AdminServices.getUniqueIdsList(groups, 'faculty_id'),
					speciality: AdminServices.getUniqueIdsList(groups, 'speciality_id')
				};

				$q.all([
					AdminServices.getEntityValues('Faculty', idLists.faculty),
					AdminServices.getEntityValues('Speciality', idLists.speciality)
				]).then(function(data) {
					var i;
					var j;
					var group;
					var faculties = data[0];
					var specialities = data[1];
					var cache = {
						facName: {},
						specName: {}
					};

					for (i = 0; i < faculties.length; i++) {
						cache.facName[faculties[i].faculty_id] = faculties[i].faculty_name;
					}
					for (i = 0; i < specialities.length; i++) {
						cache.specName[specialities[i].speciality_id] = specialities[i].speciality_name;
					}

					for (j = 0; j < groups.length; j++) {
						group = groups[j];
						group.facultyName = cache.facName[group.faculty_id];
						group.specialityName = cache.specName[group.speciality_id];
					}
					$scope.filterLists = cache;
				});
				return groups;
			}

			AdminServices.countGroups().then(function(data) {
				$scope.groupOptions.quantity = data.numberOfRecords;
			});
		}]);

(function() {
	"use strict";
	angular
		.module('admin')
		.controller('addSpecialityCtrl', ["$scope", "AdminServices", function($scope, AdminServices) {

			$scope.specialityData = {
				title: 'Спеціальність',
				key: 'speciality_name',
				state: 'admin.editSpeciality',
				txt: 'ssTest.components.addSpeciality'
			};
			$scope.addSpeciality = function(entityData){
				return AdminServices.addSpeciality(entityData);
			};
			$scope.specialityAdditions = [
				{
					additionTitle: 'Код',
					key: 'speciality_code',
					txt: 'ssTest.components.addSpeciality.additions.code'
				}
			];

		}]);
}());
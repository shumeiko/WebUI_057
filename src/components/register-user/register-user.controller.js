angular
	.module('admin')
	.controller('AddUserCtrl', ['$scope', '$state', '$stateParams', 'AdminServices','ModalService','fileReader',
		function($scope, $state, $stateParams, AdminServices, ModalService, fileReader ) {
			var regDataUser;
			var strongRegex = new RegExp("^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[!@#\$%\^&\*])(?=.{8,})");
			var mediumRegex = new RegExp("^(((?=.*[a-z])(?=.*[A-Z]))|((?=.*[a-z])(?=.*[0-9]))|((?=.*[A-Z])(?=.*[0-9])))(?=.{6,})");
			$scope.formSubmit = false;
			function checkPswd(pswd, show, strength, strengthClass, wrong) {
				if (angular.isDefined(pswd)) {
					if ($scope[wrong]) {
						$scope[wrong] = false;
					}
					$scope[show] = true;
					if (strongRegex.test(pswd)) {
						$scope[strength] = 'ssTest.components.addAdmin.strength.strong';
						$scope[strengthClass] = 'strong';
					} else if (mediumRegex.test(pswd)) {
						$scope[strength] = 'ssTest.components.addAdmin.strength.medium';
						$scope[strengthClass] = 'medium';
					} else {
						$scope[strength] = 'ssTest.components.addAdmin.strength.weak';
						$scope[strengthClass] = 'weak';
					}
				}
			}
			$scope.$watch('userPswd', function() {
				checkPswd($scope.userPswd, 'showStrength', 'strength', 'strengthClass');
			});

			$scope.$watch('userPswdConfirm', function() {
				checkPswd($scope.userPswdConfirm, 'showStrengthConfirm', 'strengthConfirm', 'strengthConfirmClass', 'wrong');
			});

			$scope.reset = function() {
				$scope.formSubmit = false;
				$scope.userName = '';
				$scope.userEmail = '';
				$scope.userPswd = '';
				$scope.userPswdConfirm = '';
				$scope.userGradebook = '';
				$scope.userSurname = '';
				$scope.userFirstname = '';
				$scope.userMiddlename = '';
			};

			var onSuccess = function() {
				ModalService.open({
					data: {
						title: "ssTest.SUCCESS",
						log: {
							logMsg: 'ssTest.components.addUser.addedMsg',
							logValues: {name: $scope.userName}
						},
						action: {
							actionTxt: "ssTest.controls.yes",
							actionFn: function(){
								$state.go('admin.home');
							}
						}
					}
				});
				$scope.reset();
			};

			var onError = function(data) {
				ModalService.logError(data.response);
			};
			var input = "";
			$scope.myImage="";
			$scope.imgData = {
				myCroppedImage: ""
			};
			var handleFileSelect = function(evt) {
				var file = evt.currentTarget.files[0];
				var reader = new FileReader();
				reader.onload = function (evt) {
					$scope.$apply(function($scope){
						$scope.myImage = evt.target.result;
					});
				};
				reader.readAsDataURL(file);
			};
			angular.element(document.querySelector('#fileInput')).on('change',handleFileSelect);

			$scope.regUser = function(formValid) {
				$scope.formSubmit = true;
				if ($scope.userPswd !== $scope.userPswdConfirm) {
					$scope.wrong = true;
					return;
				}
				if (formValid) {

					regDataUser = {
						username: $scope.userName,
						password: $scope.userPswd,
						password_confirm: $scope.userPswdConfirm,
						email: $scope.userEmail,
						gradebook_id: $scope.userGradebook,
						student_surname: $scope.userSurname,
						student_name: $scope.userFirstname,
						student_fname: $scope.userMiddlename,
						group_id: $stateParams.id,
						plain_password: $scope.userPswd,
						photo: $scope.imgData.myCroppedImage
					};
					AdminServices.registerUser(regDataUser).then(function(data) {
						if (data.response === 'ok') {
							onSuccess(data)
						} else {
							onError(data)
						}
					});
				}
			};
			AdminServices.getGroups($stateParams.id).then(function(data) {
				$scope.additionTitle = data[0].group_name;
			});
		}
	]);
angular.module('faculty', [])
	.controller('FacultiesListCtrl', ['$scope', 'AdminServices', function($scope, AdminServices) {
		$scope.facultyHead = [
			{
				title: 'ssTest.components.faculties.faculty',
				propertyName: 'faculty_name',
				sort: true
			},
			{
				title: 'ssTest.components.faculties.description',
				propertyName: 'faculty_description',
				sort: false
			}
		];

		function getRange(quant, displ) {
			AdminServices.getRangeOfFaculties(quant, displ).then(function(data) {
				if (Array.isArray(data[0])) $scope.empty = true;
				$scope.faculties = data;
				$scope.facultyOptions.numItem = displ;
				$scope.loaded = true;
			});
		}

		function getAll() {
			$scope.facultyOptions.pagination = false;

			AdminServices.getFaculties().then(function(data) {
				$scope.facultyOptions.numItem = 0;
				$scope.faculties = data;
			});
		}

		$scope.facultyOptions = {
			entityId: "faculty_id",
			state: false,
			editState: 'admin.editFaculty',
			delNameService: 'delFaculty',
			pagination: true,
			itemsPerPage: 10,
			getRange: getRange,
			getAll: getAll
		};

		AdminServices.countFaculties().then(function(data) {
			$scope.facultyOptions.quantity = data.numberOfRecords;
		});
	}]);

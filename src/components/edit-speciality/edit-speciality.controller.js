angular
	.module('admin')
	.controller('EditSpecialityCtrl', ['$scope', '$stateParams', 'AdminServices', function($scope, $stateParams, AdminServices) {
		$scope.specialityData = {
			title: 'Спеціальність',
			key: 'speciality_name',
			state: 'admin.specialities',
			txt: 'ssTest.components.editSpeciality'
		};

		AdminServices.getSpecialities($stateParams.id).then(
			function(data) {
				$scope.specialityData.name = data[0].speciality_name;
				$scope.specialityAdditions[0].id = data[0].speciality_code;
			}
		);

		$scope.editSpeciality = function(entityData){
			var obj = {
				speciality_code: entityData['speciality_code'],
				speciality_name: entityData['speciality_name']
			};
			return AdminServices.editSpeciality(obj, $stateParams.id);
		};
		$scope.specialityAdditions = [
			{
				additionTitle: 'Код',
				key: 'speciality_code',
				txt: 'ssTest.components.editSpeciality.additions.code'
			}
		];
	}]);
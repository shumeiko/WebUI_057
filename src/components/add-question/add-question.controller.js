angular
	.module('addQuestion')
	.controller('addQuestCtrl', ['$scope', '$state', 'ModalService', '$translate', '$stateParams', 'AdminServices', 'fileReader',
		function($scope, $state, ModalService, $translate, $stateParams, AdminServices, fileReader) {
			$scope.formSubmitSend = false;
			$scope.sendForm = function() {
				$scope.formSubmited = true;
			};

			$scope.resetForm = function() {
				$scope.addQuestForm.$setPristine();
				$scope.quest = {};
				$scope.imageSrc = "";
				$scope.formSubmited = false;
			};
			var onSuccess = function(data) {
				ModalService.open({
					data: {
						title: "ssTest.SUCCESS",
						log: {
							logMsg: "ssTest.components.addQuestion.addedMsg",
							logValues: {name: $scope.quest.question_text}
						},
						action: {
							actionTxt: "ssTest.controls.continue",
							actionFn: function() {
								$state.go('admin.questions', {id: $stateParams.id});
							}
						}
					}
				});
				$scope.formSubmitSend = false;
				$scope.resetForm();
			};

			var onError = function(data) {
				ModalService.logError(data.response);
				$scope.formSubmitSend = false;
			};

			AdminServices.getTests($stateParams.id).then(function(data) {
				$scope.additionTitle = data[0].test_name;
			});
			var input = '';
			var reader = new FileReader();
			$scope.readFile = function() {
				var file = document.querySelector('input[type=file]').files[0];
				var preview = document.querySelector('img');
				var b64file = {};
				reader.onload = function(event) {
					b64file = event.target.result;
					input = b64file;
					};
				 if (file) {
					reader.readAsDataURL(file);
					 }
				};
			$scope.previewFile = function(){
				fileReader.readAsDataUrl($scope.file, $scope)
					.then(function(result) {
					$scope.imageSrc = result;
				});
			};
			$scope.quest = {
				test_id: $stateParams.id
			};

			$scope.newQuest = function() {
				$scope.formSubmitSend = true;
				$scope.quest.attachment = input;

				AdminServices.addQuestion($scope.quest).then(
					function(data) {
						if (data.response === "ok") {
							onSuccess(data);
						} else {
							onError(data);
						}
					},
					function(data) {
						onError(data);
					}
				);
			};

		}]);
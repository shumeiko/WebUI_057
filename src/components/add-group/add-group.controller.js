(function() {
	"use strict";
	angular
		.module('admin')
		.controller('addGroupCtrl', ["$scope", "AdminServices", "$q", function($scope, AdminServices, $q) {
			$scope.groupData = false;

			var groupData = {
				title: 'Група',
				key: 'group_name',
				state: 'admin.students',
				txt: 'ssTest.components.addGroup'
			};

			$scope.addGroup = function(entityData){
				return AdminServices.addGroup(entityData);
			};

			var pushDepList = function(depList, name) {
				var arr = [];
				for (var i = 0, l = depList.length; i < l; i++) {
					arr.push({
						id: depList[i][name + "_id"],
						name: depList[i][name + "_name"]
					});
				}
				return arr;
			};

			var getDepItem = function(depList, names){
				return {
					dependencyEntities: pushDepList(depList, names.name),
					key: names.name + "_id",
					txt: 'ssTest.components.addGroup.dependencies.' + names.name
				};
			};

			var formatDependency = function(arr) {
				var names = [
					{
						name: 'speciality'
					},
					{
						name: 'faculty'
					}
				];
				var depArr = [];

				for (var i = 0, l = arr.length; i < l; i++) {
					depArr.push(getDepItem(arr[i], names[i]));
				}

				return depArr;
			};

			$q.all([AdminServices.getSpecialities(), AdminServices.getFaculties()]).then(function(data) {
				$scope.groupDependencies = formatDependency(data);
				$scope.groupData = groupData;
			});

		}]);
}());
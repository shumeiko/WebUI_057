angular
	.module('questions')
	.controller('questionsCtrl', ['$scope', '$rootScope', '$filter', '$stateParams', '$sessionStorage', 'AdminServices',
		function($scope, $rootScope, $filter, $stateParams, $sessionStorage, AdminServices) {
			$scope.testId = $stateParams.id;
			$scope.questionHead = [
				{
					title: "ssTest.components.questions.label",
					propertyName: 'question_text',
					sort: false,
					attachment: 'attachment'
				},
				{
					title: "ssTest.components.questions.level",
					propertyName: 'level',
					sort: true
				},
				{
					title: "ssTest.components.questions.type.label",
					propertyName: 'type',
					sort: false
				}
			];

			function getRange(quant, displ) {
				AdminServices.getRangeOfQuestionsByTest($stateParams.id, quant, displ).then(function(questions) {
					if (Array.isArray(questions[0])) $scope.empty = true;
					$scope.questionOptions.numItem = displ;
					changeTypeValue(questions);
					$scope.questions = questions;
					$scope.loaded = true;
				})
			}

			function getAll() {
				$scope.questionOptions.pagination = false;

				AdminServices.getQuestionsByTest($stateParams.id).then(function(questions) {
					if (Array.isArray(questions[0])) $scope.empty = true;
					$scope.questionOptions.numItem = 0;
					changeTypeValue(questions);
					$scope.questions = questions;
				});
			}

			function changeTypeValue(data) {
				var i;
				for (i = 0; i < data.length; i++) {
					if(data[i].type === "1") {
						data[i].type =  $filter('translate')('ssTest.components.questions.type.simple');
					}
					else {
						data[i].type = $filter('translate')('ssTest.components.questions.type.multi');
					}
				}
			}
			$scope.questionOptions = {
				entityId: "question_id",
				state: 'admin.answers',
				delNameService: 'delQuestion',
				editState: 'admin.editQuestion',
				pagination: true,
				itemsPerPage: 10,
				getRange: getRange,
				getAll: getAll
			};

			AdminServices.countQuestionsByTest($stateParams.id).then(function(data) {
				$scope.questionOptions.quantity = data.numberOfRecords;
			});

			AdminServices.getTests($stateParams.id).then(function(data) {
				$scope.additionTitle = data[0].test_name;
				$sessionStorage.breadcrumbs.test = {
					title: $scope.additionTitle,
					id: $stateParams.id
				};
				return AdminServices.getSubjects(data[0].subject_id);

			}).then(function(subject) {
				$sessionStorage.breadcrumbs.subject = {
					title: subject[0].subject_name,
					id: subject[0].subject_id
				};
				$rootScope.$emit('loadedEntity');
			});

			$scope.questionTitlesPdf = $scope.questionHead.slice(0, 1);
			$scope.questionAdditionsPdf = $scope.questionHead.slice(-2);
		}]);
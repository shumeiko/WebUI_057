angular.module('addTest')
	.controller('AddTestCtrl', ['$scope', '$state', 'ModalService', '$translate', '$stateParams', 'AdminServices',
		function($scope, $state, ModalService, $translate, $stateParams, AdminServices) {
			$scope.test = {
				subject_id: $stateParams.id

			};
			$scope.formSubmitSend = false;
			$scope.sendForm = function(){
				$scope.formSubmited = true;
			};

			$scope.resetForm = function() {
				$scope.addTestForm.$setPristine();
				$scope.test = {};
				$scope.formSubmited = false;
			};

			var onSuccess = function(data) {
				ModalService.open({
					data: {
						title: "ssTest.SUCCESS",
						log: {
							logMsg: "ssTest.components.addTest.addedMsg",
							logValues: {name: $scope.test.test_name}
						},
						action: {
							actionTxt: "ssTest.controls.continue",
							actionFn: function() {
								$state.go('admin.tests', {id: $stateParams.id});
							}
						}
					}
				});
				$scope.formSubmitSend = false;
				$scope.resetForm();
			};

			var onError = function(data) {
				ModalService.logError(data.response);
				$scope.formSubmitSend = false;
			};

			AdminServices.getSubjects($stateParams.id).then(function(data) {
				$scope.additionTitle = data[0].subject_name;
			});

			$scope.newTest = function() {
				$scope.formSubmitSend = true;

				AdminServices.addTest($scope.test).then(
					function(data) {
						if (data.response === "ok") {
							onSuccess(data);
						} else {
							onError(data);
						}
					},
					function(data) {
						onError(data);
					}
				);

			};
		}]);



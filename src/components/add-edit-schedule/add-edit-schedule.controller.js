angular
	.module('schedule')
	.controller('AddEditScheduleCtrl', ['$scope', '$state', 'ModalService', '$stateParams', 'AdminServices',
		function($scope, $state, ModalService, $stateParams, AdminServices) {
			$scope.setOnly = false;
			$scope.pattern = { 'd': /[0-3]/, 'm': /[01]/, 'y': /2/};
			$scope.timetable = {};

			AdminServices.getSubjects($scope.subjectId).then(function(data) {
				$scope.additionTitle = data[0].subject_name;
			});

			AdminServices.getGroups().then(function(data) {
				$scope.groups = data;
				$scope.groupName = data[0].group_name;
			});

			$scope.$watch('timetable.event_date', function() {
				if ($scope.timetable.event_date) {
					var date = $scope.timetable.event_date.slice(0, 2);
					var month = $scope.timetable.event_date.slice(2, 4);
					var year = $scope.timetable.event_date.slice(4);
					var d;
					var maxDate;

					if (month > 12) month = 12;

					d = new Date(year, month);
					maxDate = d.getDate(d.setDate(0));

					if (date > maxDate) date = maxDate;

					$scope.timetable.event_date = '' + date + month + year;
				}
			});

			var onError = function(data) {
				ModalService.logError(data.response);
				$scope.formSubmitSend = false;
			};

			function addSchedule() {
				$scope.formSubmitSend = false;
				$scope.timetable.subject_id = $stateParams.id;

				$scope.sendForm = function(){
					$scope.formSubmited = true;

					if ($scope.ScheduleForm.$valid) {
						$scope.formSubmited = false;
						newSchedule();
					}
				};

				$scope.resetForm = function() {
					$scope.ScheduleForm.$setPristine();
					$scope.timetable = {};
					$scope.formSubmited = false;
				};

				var onSuccess = function() {
					ModalService.open({
						data: {
							title: "ssTest.SUCCESS",
							log: {
								logMsg: "ssTest.components.addTestSchedule.addedMsg",
								logValues: {name: $scope.groupName}
							},
							action: {
								actionTxt: "ssTest.controls.continue",
								actionFn: function() {
									$state.go('admin.schedule', {id: $stateParams.id});
								}
							}
						}
					});
					$scope.formSubmitSend = false;
					$scope.resetForm();
				};

				var newSchedule = function() {
					var date = $scope.timetable.event_date.slice(0, 2);
					var month = $scope.timetable.event_date.slice(2, 4);
					var year = $scope.timetable.event_date.slice(4);

					$scope.timetable.event_date = [year, month, date].join('-');

					AdminServices.addSchedule($scope.timetable).then(
						function(data){
							if (data.response === "ok") {
								onSuccess(data);
							} else {
								onError(data);
							}
						},
						function(data) {
							onError(data);
						}
					);
				};
			}

			function editSchedule() {
				$scope.formSubmitSend = false;

				AdminServices.getSchedule($stateParams.id).then(function(data) {
					var eventDate = data[0].event_date.split('-').reverse();
					data[0].event_date = eventDate.join('');
					$scope.timetable = data[0];
				});

				$scope.sendForm = function(){
					$scope.formSubmited = true;

					if ($scope.ScheduleForm.$valid) {
						$scope.formSubmited = false;
						newSchedule();
					}
				};

				var onSuccess = function() {
					ModalService.open({
						data: {
							title: "ssTest.SUCCESS",
							log: {
								logMsg: "ssTest.components.editTestSchedule.addedMsg",
								logValues: {name: $scope.groupName}
							}
						}
					});
					$scope.formSubmitSend = false;
				};

				var newSchedule = function() {
					var date = $scope.timetable.event_date.slice(0, 2);
					var month = $scope.timetable.event_date.slice(2, 4);
					var year = $scope.timetable.event_date.slice(4);

					$scope.timetable.event_date = [year, month, date].join('-');

					AdminServices.editSchedule($scope.timetable, $stateParams.id).then(
						function(data){
							if (data.response === "ok") {
								onSuccess(data);
							} else {
								onError(data);
							}
						},
						function(data) {
							onError(data);
						}
					);
				};
			}

			switch ($state.current.name) {
				case 'admin.addSchedule':
					$scope.edit = false;
					addSchedule();
					break;
				case 'admin.editSchedule':
					$scope.edit = true;
					editSchedule();
			}
		}
	]);
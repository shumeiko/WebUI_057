angular
	.module('user')
	.controller('ProfileCtrl', ['$stateParams', '$scope', '$q', 'UserService', function($stateParams, $scope, $q, UserService) {
		$q.all([
			UserService.getStudents(localStorage["idForUser"]),
			UserService.getGroups()
		]).then(function(data) {
			$scope.students = data[0];
			$scope.photo = data[0][0].photo;
			$scope.id = data[0][0].group_id;
			if ($scope.photo == "") {
				$scope.photo = "images/ups.jpg";
			}
			var i;
			var group = data[1];
			for (i = 0; i < group.length; i++) {
				if (group[i].group_id == $scope.id) {
					$scope.group_name = group[i].group_name;
				}
			}

		});
	}]);
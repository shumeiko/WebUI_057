angular
	.module('admin')
	.controller('RegisterAdminCtrl', ['$scope', '$state', '$stateParams', '$sce', 'ModalService', 'AdminServices',
		function($scope, $state, $stateParams, $sce, ModalService, AdminServices) {
			var regData;
			var strongRegex = /^(?=.*[a-zа-яґёєії])(?=.*[A-ZА-ЯҐЁЄІЇ])(?=.*\d)(?=.*[!@#\$%\^&\*№?])(?=.{10,})/;
			var mediumRegex = /^(?=.*[a-zа-яґёєії])(?=.*[A-ZА-ЯҐЁЄІЇ])(?=.*\d)(?=.{6,})/;
			var addedMsg = 'ssTest.components.addAdmin.addedMsg';
			var editedMsg = 'ssTest.components.addAdmin.editedMsg';
			var reqString = "ssTest.components.addAdmin.pswdSecurity.requirements.";
			var pswdMedReq = ["notLessSymbMed", "digit", "lowerCase", "upperCase"];
			var pswdStrReq = ["notLessSymbStr", "digit", "lowerCase", "upperCase", "specSymb"];
			$scope.formSubmit = false;

			function bindStr(arr, str) {
				for (var i = 0; i < arr.length; i++) {
					arr[i] = str + arr[i];
				}
			}

			bindStr(pswdMedReq, reqString);
			bindStr(pswdStrReq, reqString);

			$scope.pswdStrReq = pswdStrReq;
			$scope.pswdMedReq = pswdMedReq;

			function checkPswd(pswd, show, strength, strengthClass, wrong) {
				if (angular.isDefined(pswd)) {
					if ($scope[wrong]) {
						$scope[wrong] = false;
					}
					$scope[show] = true;
					if (strongRegex.test(pswd)) {
						$scope[strength] = 'ssTest.components.addAdmin.strength.strong';
						$scope[strengthClass] = 'strong';
					} else if (mediumRegex.test(pswd)) {
						$scope[strength] = 'ssTest.components.addAdmin.strength.medium';
						$scope[strengthClass] = 'medium';
					} else {
						$scope[strength] = 'ssTest.components.addAdmin.strength.weak';
						$scope[strengthClass] = 'weak';
					}
				}
			}

			$scope.$watch('adminPswd', function() {
				checkPswd($scope.adminPswd, 'showStrength', 'strength', 'strengthClass');
			});

			$scope.$watch('adminPswdConfirm', function() {
				checkPswd($scope.adminPswdConfirm, 'showStrengthConfirm', 'strengthConfirm', 'strengthConfirmClass', 'wrong');
			});

			$scope.reset = function() {
				$scope.formSubmit = false;
				$scope.adminName = '';
				$scope.adminEmail = '';
				$scope.adminPswd = '';
				$scope.adminPswdConfirm = '';
			};

			var onSuccess = function(msg) {
				ModalService.open({
					data: {
						title: "ssTest.SUCCESS",
						log: {
							logMsg: msg,
							logValues: {name: $scope.adminName}
						},
						action: {
							actionTxt: "ssTest.controls.yes",
							actionFn: function(){
								$state.go('admin.home');
							}
						}
					}
				});
				$scope.reset();
			};

			var onError = function(data) {
				ModalService.logError(data.response);
			};

			$scope.register = function(formValid) {
				$scope.formSubmit = true;

				if ($scope.adminPswd !== $scope.adminPswdConfirm) {
					$scope.wrong = true;
					return;
				}

				if (formValid) {
					regData = {
						username: $scope.adminName,
						password: $scope.adminPswd,
						password_confirm: $scope.adminPswdConfirm,
						email: $scope.adminEmail
					};

					if (!$scope.editing) {
						AdminServices.addAdmin(regData).then(function(data) {
							if (data.response === 'ok') {
								onSuccess(addedMsg);
							} else {
								onError(data);
							}
						});
					} else {
						AdminServices.editAdmin(regData, $stateParams.id).then(function(data) {
							if (data.response === 'ok') {
								onSuccess(editedMsg);
							} else {
								onError(data);
							}
						})
					}
				}
			};

			if ($state.current.name === 'admin.editAdmin') {
				$scope.editing = true;

				AdminServices.getAdmins($stateParams.id).then(function(data) {
					$scope.adminName = data[0].username;
					$scope.adminEmail = data[0].email;
				});
			}
		}
	]);
angular.module('subject', [])
	.controller('SubjectsListCtrl', ['$scope', 'AdminServices', function($scope, AdminServices) {
		$scope.subjectHead = [
			{
				title: 'ssTest.components.subjects.subject',
				propertyName: 'subject_name',
				sort: true
			},
			{
				title: 'ssTest.components.subjects.description',
				propertyName: 'subject_description',
				sort: false
			}
		];

		function getRange(quant, displ) {
			AdminServices.getRangeOfSubjects(quant, displ).then(function(data) {
				if (Array.isArray(data[0])) $scope.empty = true;
				$scope.subjects = data;
				$scope.subjectOptions.numItem = displ;
				$scope.loaded = true;
			});
		}

		function getAll() {
			$scope.subjectOptions.pagination = false;

			AdminServices.getSubjects().then(function(data) {
				$scope.subjectOptions.numItem = 0;
				$scope.subjects = data;
			});
		}

		$scope.subjectOptions = {
			entityId: "subject_id",
			state: 'admin.tests',
			editState: 'admin.editSubject',
			delNameService: 'delSubject',
			pagination: true,
			itemsPerPage: 10,
			getRange: getRange,
			getAll: getAll
		};

		AdminServices.countSubjects().then(function(data) {
			$scope.subjectOptions.quantity = data.numberOfRecords;
		});
	}]);


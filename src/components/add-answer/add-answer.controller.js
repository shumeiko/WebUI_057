angular
	.module('addAnswer')
	.controller('AddAnswerCtrl', ['$scope', '$state', 'ModalService', '$translate', '$stateParams', 'AdminServices', 'fileReader',
		function($scope, $state, ModalService, $translate, $stateParams, AdminServices, fileReader) {
			$scope.answer = {
				question_id: $stateParams.id

			};
			$scope.formSubmitSend = false;
			$scope.sendForm = function(){
				$scope.formSubmited = true;
			};

			$scope.resetForm = function() {
				$scope.addAnswerForm.$setPristine();
				$scope.answer = {};
				$scope.imageSrc = "";
				$scope.formSubmited = false;
			};

			var onSuccess = function(data) {
				ModalService.open({
					data: {
						title: "ssTest.SUCCESS",
						log: {
							logMsg: "ssTest.components.addAnswer.addedMsg",
							logValues: {name: $scope.answer.answer_text}
						},
						action: {
							actionTxt: "ssTest.controls.continue",
							actionFn: function() {
								$state.go('admin.answers', {id: $stateParams.id});
							}
						}
					}
				});
				$scope.formSubmitSend = false;
				$scope.resetForm();
			};

			var onError = function(data) {
				ModalService.logError(data.response);
				$scope.formSubmitSend = false;
			};

			var input = '';
			var reader = new FileReader();
			$scope.readFile = function() {
				var file = document.querySelector('input[type=file]').files[0];
				var preview = document.querySelector('img');
				var b64file = {};
				reader.onload = function(event) {
					b64file = event.target.result;
					input = b64file;
				};
				if (file) {
					reader.readAsDataURL(file);
				}
			};
			$scope.previewFile = function(){
				fileReader.readAsDataUrl($scope.file, $scope)
					.then(function(result) {
						$scope.imageSrc = result;
					});
			};
			$scope.newAnswer = function() {
				$scope.formSubmitSend = true;
				$scope.answer.attachment = input;

				AdminServices.addAnswer($scope.answer).then(
					function(data) {
						if (data.response === "ok") {
							onSuccess(data);
						} else {
							onError(data);
						}
					},
					function(data) {
						onError(data);
					}
				);

			};

		}]);
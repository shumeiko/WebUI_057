angular
	.module("schedule")
	.controller("ScheduleCtrl", ["$scope","$stateParams", "$sessionStorage", "AdminServices",
		function($scope, $stateParams, $sessionStorage, AdminServices) {
		$scope.subjectId = $stateParams.id;

		AdminServices.getSubjects($scope.subjectId).then(function(data) {
			$scope.additionTitle = data[0].subject_name;
		});

		$scope.timetableHead = [
			{
				title: "ssTest.components.groups.group",
				propertyName: 'group_id',
				sort: true
			},
			{
				title: "ssTest.components.studentResults.date",
				propertyName: 'event_date',
				sort: true
			}
		];

		$scope.timetableOptions = {
			entityId: "timetable_id",
			state: false,
			delNameService: 'delSchedule',
			editState: 'admin.editSchedule'
		};

		AdminServices.getScheduleForSubject($scope.subjectId).then(function(timetables) {
			var i;
			var groupsId = [];
			if (Array.isArray(timetables[0])) $scope.empty = true;

			for (i = 0; i < timetables.length; i++) {
				groupsId.push(timetables[i].group_id);
			}

			$scope.timetables = timetables;

			AdminServices.getEntityValues('Group', groupsId).then(function(groups) {
				var i;
				var j;
				for (i = 0; i < timetables.length; i++) {
					for (j = 0; j < groups.length; j++) {
						if (timetables[i].group_id === groups[j].group_id) {
							$scope.timetables[i].group_id = groups[j].group_name;
						}
					}

				}
				$scope.loaded = true;
			});
		});

		$sessionStorage.breadcrumbs.scheduleId = $stateParams.id;
	}]
);
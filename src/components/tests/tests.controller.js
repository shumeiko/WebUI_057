angular
	.module('tests')
	.controller('testsCtrl', ['$scope', '$rootScope', '$filter', '$q', '$state', '$stateParams', '$sessionStorage', 'AdminServices',
		function ($scope, $rootScope, $filter, $q, $state, $stateParams, $sessionStorage, AdminServices) {
			$scope.subjectId = $stateParams.id;

			$scope.allTests = false;

			$scope.testHead = [
				{
					title: "ssTest.components.addTest.label",
					propertyName: 'test_name',
					sort: true
				},
				{
					title: "ssTest.components.addTest.additions.tasks",
					propertyName: 'tasks',
					sort: false
				},
				{
					title: "ssTest.components.addTest.additions.duration",
					propertyName: 'time_for_test',
					sort: false
				},
				{
					title: "ssTest.components.addTest.additions.status",
					propertyName: 'enabled',
					sort: false
				},
				{
					title: "ssTest.components.addTest.additions.attempts",
					propertyName: 'attempts',
					sort: false
				}
			];

			$scope.testOptions = {
				entityId: "test_id",
				state: 'admin.questions',
				delNameService: 'delTest',
				editState: 'admin.editTest'
			};

			function showTests() {
				$scope.allTests = true;

				$scope.testHead.splice(1, 0, {
					title: "ssTest.components.tests.subject",
					propertyName: 'subject_id',
					sort: true
				});

				$scope.testOptions.pagination = true;
				$scope.testOptions.itemsPerPage = 10;

				function getIdArray(tests) {
					var idArray = [];

					for (var i = 0; i < tests.length; i++) {
						if (idArray.indexOf(tests[i].subject_id) === -1) {
							idArray.push(tests[i].subject_id);
						}
					}

					return idArray;
				}

				function bindIdsWithNames(data) {
					var tests = data;
					var idArray = getIdArray(data);

					AdminServices.getEntityValues('Subject', idArray).then(function(data) {
						for (var i = 0; i < data.length; i++) {
							for (var j = 0; j < tests.length; j++) {
								if (data[i].subject_id === tests[j].subject_id) {
									tests[j].subject_id = data[i].subject_name;
								}
							}
						}
						changeStatusValue(tests);
						$scope.tests = tests;
					});
				}

				function getRange(quant, displ) {
					AdminServices.getRangeOfTests(quant, displ).then(function(data) {
						if (Array.isArray(data[0])) $scope.empty = true;
						bindIdsWithNames(data);
						$scope.testOptions.numItem = displ;
						$scope.loaded = true;
					});
				}

				function getAll() {
					$scope.testOptions.pagination = false;

					AdminServices.getTests().then(function(data) {
						bindIdsWithNames(data);

						$scope.testOptions.numItem = 0;
					});
				}

				AdminServices.countTests().then(function(data) {
					$scope.testOptions.quantity = data.numberOfRecords;
				});

				$scope.testOptions.getRange = getRange;
				$scope.testOptions.getAll = getAll;
			}

			function showTestsBySubject() {

				AdminServices.getTestsBySubject($stateParams.id).then(function(tests) {
					if (Array.isArray(tests[0])) $scope.empty = true;
					changeStatusValue(tests);
					$scope.loaded = true;
					$scope.tests = tests;
				});

				AdminServices.getSubjects($stateParams.id).then(function(data) {
					$scope.additionTitle = data[0].subject_name;
					$sessionStorage.breadcrumbs.subject = {
							title: $scope.additionTitle,
							id: $stateParams.id
					};

					$rootScope.$emit('loadedEntity');
				});
			}

			function changeStatusValue (data) {
				var i;
				for (i = 0; i < data.length; i++) {
					if(data[i].enabled === "1") {
						data[i].enabled =  $filter('translate')('ssTest.components.tests.status.enabled');
					}
					else {
						data[i].enabled = $filter('translate')('ssTest.components.tests.status.disabled');
					}
				}
			}

			switch ($state.current.name) {
				case 'admin.tests':
					showTestsBySubject();
					break;
				case 'admin.testsList':
					showTests();
			}

			$scope.testTitlesPdf = $scope.testHead.slice(0, -4);
			$scope.testAdditionsPdf = $scope.testHead.slice(-4);
		}
	]);
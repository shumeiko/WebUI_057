angular
	.module('admin')
	.controller('EditSubjectCtrl', ['$scope', '$stateParams', 'AdminServices', function($scope, $stateParams, AdminServices) {
		$scope.subjectData = {
			title: 'Предмет',
			key: 'subject_name',
			state: 'admin.tests',
			txt: 'ssTest.components.editSubject'
		};

		AdminServices.getSubjects($stateParams.id).then(
			function(data) {
				$scope.subjectData.name = data[0].subject_name;
				$scope.subjectAdditions[0].id = data[0].subject_description;
			}
		);

		$scope.editSubject = function(entityData){
			return AdminServices.editSubject(entityData, $stateParams.id);
		};
		$scope.subjectAdditions = [
			{
				additionTitle: 'Опис',
				additionArea: true,
				key: 'subject_description',
				txt: 'ssTest.components.editSubject.additions.desc'
			}
		];
	}]);
(function() {
	"use strict";

	angular.module('gridster')
		.controller('installWidgetCtrl', ["$scope", "$modalInstance", 'data',
			function($scope, $modalInstance, data) {

				$scope.widgetData = angular.copy(data);

				$scope.formState = {};

				$scope.save = function() {
					alert('save');
					console.log($scope.widgetData)
					$modalInstance.close();
					$scope.formState = {};
				};

				$scope.cancel = function() {
					$modalInstance.close();
				};
			}
		]);

}());
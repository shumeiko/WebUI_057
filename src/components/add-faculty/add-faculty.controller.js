(function() {
	"use strict";
	angular
		.module('admin')
		.controller('addFacultyCtrl', ["$scope", "AdminServices", function($scope, AdminServices) {

			$scope.facultyData = {
				title: 'Факультет',
				key: 'faculty_name',
				state: 'admin.editFaculty',
				txt: 'ssTest.components.addFaculty'
			};
			$scope.addFaculty = function(entityData){
				return AdminServices.addFaculty(entityData);
			};
			$scope.facultyAdditions = [
				{
					additionTitle: 'Опис',
					additionArea: true,
					key: 'faculty_description',
					txt: 'ssTest.components.addFaculty.additions.desc'
				}
			];

		}]);
}());
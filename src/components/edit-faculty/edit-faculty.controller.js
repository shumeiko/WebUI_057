angular
	.module('admin')
	.controller('EditFacultyCtrl', ['$scope', '$stateParams', 'AdminServices', function($scope, $stateParams, AdminServices) {
		$scope.facultyData = {
			title: 'Факультет',
			key: 'faculty_name',
			state: 'admin.faculties',
			txt: 'ssTest.components.editFaculty'
		};

		AdminServices.getFaculties($stateParams.id).then(
			function(data) {
				$scope.facultyData.name = data[0].faculty_name;
				$scope.facultyAdditions[0].id = data[0].faculty_description;
			}
		);

		$scope.editFaculty = function(entityData){
			return AdminServices.editFaculty(entityData, $stateParams.id);
		};

		$scope.facultyAdditions = [
			{
				additionTitle: 'Опис',
				additionArea: true,
				key: 'faculty_description',
				txt: 'ssTest.components.editFaculty.additions.desc'
			}
		];

	}]);
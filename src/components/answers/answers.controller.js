angular
	.module('answers')
	.controller('answersCtrl', ['$scope', '$rootScope', '$filter', '$stateParams', '$sessionStorage', 'AdminServices',
		function($scope, $rootScope, $filter, $stateParams, $sessionStorage, AdminServices) {
			$scope.questionId = $stateParams.id;

			$scope.answerHead = [
				{
					title: "ssTest.components.answers.label",
					propertyName: 'answer_text',
					sort: false,
					attachment: 'attachment'
				},
				{
					title: "ssTest.components.addAnswer.additions.trueAnswer",
					propertyName: 'true_answer',
					sort: false
				}
			];

			$scope.answerOptions = {
				entityId: "answer_id",
				state: false,
				delNameService: 'delAnswer',
				editState: 'admin.editAnswer'
			};

			AdminServices.getAnswersByQuestion($stateParams.id).then(function(answers) {
				if (Array.isArray(answers[0])) $scope.empty = true;
				changeAnswerValue(answers);
				$scope.answers = answers;
				$scope.loaded = true;
			});

			AdminServices.getQuestions($stateParams.id).then(function(data) {
				$scope.additionTitle = data[0].question_text;
				$scope.attachment = data[0].attachment;
				$sessionStorage.breadcrumbs.question = {
					title: $scope.additionTitle,
					id: $stateParams.id
				};

				$rootScope.$emit('loadedEntity');
			});

			function changeAnswerValue(data) {
				var i;
				for (i = 0; i < data.length; i++) {
					if(data[i].true_answer === "1") {
						data[i].true_answer =  $filter('translate')('ssTest.controls.yes');
					}
					else {
						data[i].true_answer = $filter('translate')('ssTest.controls.no');
					}
				}
			}

		}]);

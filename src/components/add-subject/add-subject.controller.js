(function() {
	"use strict";
	angular
		.module('admin')
		.controller('addSubjectCtrl', ["$scope", "AdminServices", function($scope, AdminServices) {

			$scope.subjectData = {
				title: 'Предмет',
				key: 'subject_name',
				state: 'admin.tests',
				txt: 'ssTest.components.addSubject'
			};
			$scope.addSubject = function(entityData){
				return AdminServices.addSubject(entityData);
			};
			$scope.subjectAdditions = [
				{
					additionTitle: 'Опис',
					additionArea: true,
					key: 'subject_description',
					txt: 'ssTest.components.addSubject.additions.desc'
				}
			];

		}]);
}());
angular.module('resultDetail', [])
	.controller('ResultDetailListCtrl',
	['$scope', '$rootScope', '$sessionStorage', '$state', '$stateParams', '$q', 'AdminServices',
		function($scope, $rootScope, $sessionStorage, $state, $stateParams, $q, AdminServices) {
			$scope.resultDetailHead = [
				{
					title: 'ssTest.components.resultDetail.question',
					propertyName: 'question',
					sort: true,
					attachment: 'attachmentQuestion'
				},
				{
					title: 'ssTest.components.resultDetail.answer',
					propertyName: 'answer',
					sort: false,
					attachment: 'attachmentAnswer'
				},
				{
					title: 'ssTest.components.resultDetail.level',
					propertyName: 'level',
					sort: true
				}
			];

			$scope.resultDetailOptions = {
				mark: true,
				entityId: false,
				state: false,
				delNameService: false,
				hideTableActions: {
					edit: true,
					delete: true
				}
			};

			AdminServices.getStudentResults($stateParams.id).then(function(data) {
				var studentResult = data[0];

				$scope.idLists = {
					questions: studentResult.questions.split('/'),
					trueAnswers: studentResult.true_answers.split('/'),
					answersByType: parseAnswersString(studentResult.answers)
				};

				$scope.idLists.allAnsvers = getAllAnswers($scope.idLists.answersByType);

				$scope.testId = studentResult.test_id;
				AdminServices.getTests($scope.testId).then(function(test) {
					$scope.additionTitle = test[0].test_name;
					if ($state.current.name === 'admin.resultDetail') {
						$sessionStorage.breadcrumbs.session = $scope.additionTitle;
						$rootScope.$emit('loadedEntity');
					}
				});

				return $q.all([
					AdminServices.getEntityValues('Question', $scope.idLists.questions),
					AdminServices.getEntityValues('Answer', $scope.idLists.allAnsvers)
				]);

			}).then(function(entities) {
				var i;
				var j;
				var cacheAnswersNames = {};
				var resultDetail;
				var resultDetails = [];
				var question = entities[0];
				var answers = entities[1];

				for (j = 0; j < answers.length; j++) {
					cacheAnswersNames[answers[j].answer_id] = answers[j];
				}

				for (i = 0; i < question.length; i++) {
					var answer = getAnswerById($scope.idLists.answersByType[i], cacheAnswersNames);
					resultDetail = {
						level: question[i].level,
						question: question[i].question_text,
						attachmentQuestion: question[i].attachment,
						answer: answer.answer_text || 'Немає відповіді',
						attachmentAnswer: answer.attachment
					};
					resultDetail.falseAnswer = $scope.idLists.trueAnswers[i] !== '1';
					resultDetails.push(resultDetail);
				}

				$scope.resultDetails = resultDetails;
				return AdminServices.getTestDetailsByTest($scope.testId);

			}).then(function(testDetails) {
				var i;
				var j;
				var level;
				var mark;
				var rate;
				var chartData = {};
				var rateByLevelList = {};
				var rightPoints = 0;
				var allPoints = 0;
				var result = $scope.resultDetails;
				$scope.chartData = [];

				for (i = 0; i < testDetails.length; i++) {
					rateByLevelList[testDetails[i].level] = testDetails[i].rate;
				}

				for (j = 0; j < result.length; j++) {
					level = result[j].level;
					if (chartData[level]) {
						chartData[level].qQnty++;
						if (!result[j].falseAnswer) {
							chartData[level].qRight ? chartData[level].qRight++ : chartData[level].qRight = 1;
						}
					} else {
						chartData[level] = {};
						chartData[level].level = result[j].level;
						chartData[level].qQnty = 1;
						chartData[level].qRight = 0;
						if (!result[j].falseAnswer) chartData[level].qRight = 1;
					}
				}

				for (var item in chartData) {
					rate = rateByLevelList[item];
					mark = chartData[item];
					mark.qCost = rate;
					rightPoints += mark.qRight * rate;
					allPoints += mark.qQnty * rate;
					$scope.chartData.push(mark);
				}

				$scope.rightPoints = rightPoints;
				$scope.allPoints = allPoints;
				$scope.rate = (rightPoints / allPoints * 100).toFixed(0);
				$scope.loaded = true;
			});

			function parseAnswersString(str) {
				var i;
				var parsedAnswers = [];
				var multiChoiceAnswers;
				var dimensionalArr = str.split('/');

				for (i = 0; i < dimensionalArr.length; i++) {
					if (dimensionalArr[i][0] == '[') {
						multiChoiceAnswers = dimensionalArr[i].slice(1, -1).split(':');
						parsedAnswers.push(multiChoiceAnswers);
					} else {
						parsedAnswers.push(dimensionalArr[i]);
					}
				}
				return parsedAnswers
			}

			function getAllAnswers(answers) {
				var uniqueAnswers = [];
				var i;
				var j;

				for (i = 0; i < answers.length; i++) {
					if (!Array.isArray(answers[i])) {
						if (answers[i] !== 0) uniqueAnswers.push(answers[i]);
					} else {
						for (j = 0; j < answers[i].length; j++) {
							if (answers[i][j] !== 0) uniqueAnswers.push(answers[i][j]);
						}
					}
				}
				return uniqueAnswers;
			}

			function getAnswerById(item, cache) {
				var j;
				var str = '';

				if (!Array.isArray(item)) {
					if (item !== 0) return cache[item];
					return false;
				} else {
					for (j = 0; j < item.length; j++) {
						if (item[j] === 0) return false;
						str += cache[item[j]] + ', ';
					}
					return str.slice(0, -2);
				}
			}

		}]);
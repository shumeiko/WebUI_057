angular
	.module('admin')
	.controller('HomeAdminCtrl', ['$scope', '$q', 'AdminServices', 'ModalService',
		function($scope, $q, AdminServices, ModalService) {
			$scope.sideBar = [
				{
					title: 'ssTest.components.homeAdmin.sideBar.admins',
					state: 'admin.admins',
					glyph: 'king'
				},
				{
					title: 'ssTest.components.homeAdmin.sideBar.tests',
					state: 'admin.testsList',
					glyph: 'list'
				},
				{
					title: 'ssTest.components.homeAdmin.sideBar.students',
					state: 'admin.studentsList',
					glyph: 'education'
				}
			];

			$scope.statistics = [
				{title: 'ssTest.components.homeAdmin.statistics.regGroups'},
				{title: 'ssTest.components.homeAdmin.statistics.regSubjects'},
				{title: 'ssTest.components.homeAdmin.statistics.regSpecialities'},
				{title: 'ssTest.components.homeAdmin.statistics.regFaculties'},
				{title: 'ssTest.components.homeAdmin.statistics.regTests'},
				{title: 'ssTest.components.homeAdmin.statistics.regUsers'}
			];

			$q.all([
				AdminServices.countGroups(),
				AdminServices.countSubjects(),
				AdminServices.countSpecialities(),
				AdminServices.countFaculties(),
				AdminServices.countTests(),
				AdminServices.countStudents()
			]).then(function(data) {
				for (var i = 0; i < data.length; i++) {
					$scope.statistics[i].quantity = data[i].numberOfRecords;
				}
			});

			AdminServices.getWidgets().then(
				function(data) {
					if (data[0][1] !== 'null') {
						var widgetsArr = [];
						for (var i = 0; i < data.length; i++) {
							var widget = data[i];
							widgetsArr.push({
								id: Number(widget.widget_id),
								sizeX: Number(widget.sizeX),
								sizeY: Number(widget.sizeY),
								col: Number(widget.cols),
								row: Number(widget.rows),
								type: widget.type,
								data: widget.widget_data ? JSON.parse(widget.widget_data.replace(/&quot;/g, '"')) : ""
							});
						}
						$scope.standardItems = widgetsArr;
					} else {
						$scope.standardItems = [];
					}
				}, 
				function(error) {
					ModalService.logError(error);
				}
			);

			$scope.gridsterOpts = {
				columns: 3,
				pushing: true,
				floating: true,
				swapping: true,
				width: 'auto',
				colWidth: 'auto',
				rowHeight: 'match',
				margins: [30, 30],
				outerMargin: false,
				isMobile: false,
				mobileBreakPoint: 768,
				mobileModeEnabled: true,
				minColumns: 1,
				minRows: 1,
				defaultSizeX: 1,
				defaultSizeY: 1,
				minSizeX: 1,
				minSizeY: 1,
				resizable: {
					enabled: false,
					handles: ['e', 's', 'w', 'se', 'sw']
				},
				draggable: {
					enabled: false,
					handle: '.drag-zone'
				}
			};
		}
	]);
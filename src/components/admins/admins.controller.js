angular
	.module('admin')
	.controller('AdminsCtrl', ['$scope', 'AdminServices', function($scope, AdminServices) {
		$scope.adminsHead = [
			{
				title: 'ssTest.components.admins.login',
				propertyName: 'username',
				sort: true
			},
			{
				title: 'ssTest.components.admins.email',
				propertyName: 'email',
				sort: false
			}
		];

		AdminServices.getAdmins().then(function(data) {
			$scope.admins = data;
		});

		$scope.adminsOptions = {
			entityId: "id",
			state: false,
			editState: 'admin.editAdmin',
			delNameService: 'delAdmin'
		};
	}]);
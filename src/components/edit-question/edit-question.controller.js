angular
	.module('admin')
	.controller('editQuestCtrl',['$scope', 'ModalService', '$filter', '$stateParams', 'AdminServices',
		function($scope, ModalService, $filter, $stateParams, AdminServices) {

			AdminServices.getQuestions($stateParams.id).then(function(data) {
				var questions = data[0];
				questions.level = parseInt(questions.level);
				$scope.quest = questions;
				$scope.imageSrc = $scope.quest.attachment;
				$scope.questionId = $scope.quest.question_id;
				$scope.quest.question_text = $filter('decode')($scope.quest.question_text);

				return AdminServices.getTests($scope.quest.test_id);
			}).then(function(data) {
				$scope.additionTitle = data[0].test_name;
			});

			var input = "";
			var reader = new FileReader();
			$scope.readFile = function() {
				var file = document.querySelector('input[type=file]').files[0];
				var preview = document.querySelector('img');
				var b64file = {};
				reader.onload = function(event) {
					b64file = event.target.result;
					input = b64file;
				};
				if (file) {
					reader.readAsDataURL(file);
				}
			};
			$scope.previewFile = function(){
				fileReader.readAsDataUrl($scope.file, $scope)
					.then(function(result) {
						$scope.imageSrc = result;
					});
			};

			var onSuccess = function(data) {
				ModalService.open({
					data: {
						title: "ssTest.SUCCESS",
						log: {
							logMsg: "ssTest.components.editQuestion.editedMsg",
							logValues: {name: $scope.quest.question_text}
						}
					}
				});
				$scope.formSubmited = false;
				$scope.formSubmitSend = false;
			};

			var onError = function(data) {
				ModalService.logError(data.response);
				$scope.formSubmited = false;
				$scope.formSubmitSend = false;
			};

			$scope.sendForm = function(){
				$scope.formSubmited = true
			};

			$scope.editQuest = function() {
				$scope.formSubmitSend = true;
				$scope.quest.attachment = input;

				AdminServices.editQuestion($scope.quest, $stateParams.id).then(
					function(data){
						if (data.response === "ok") {
							onSuccess(data);
						} else {
							onError(data);
						}
					},
					function(data) {
						onError(data);
					}

				);

			};
		}
	]);
angular.module('student', [])
	.controller('StudentsListCtrl', ['$scope', '$state', '$rootScope', '$q', '$stateParams', '$sessionStorage', 'StudentProvider', 'AdminServices',
		function($scope, $state, $rootScope, $q, $stateParams, $sessionStorage, StudentProvider, AdminServices) {
			$scope.allStudents = false;

			$scope.studentHead = [
				{
					title: 'Фото',
					propertyName: 'photo',
					sort: true,
					photo: true
				},
				{
					title: 'ssTest.components.students.surname',
					propertyName: 'student_surname',
					sort: true
				},
				{
					title: "ssTest.components.students.name",
					propertyName: 'student_name',
					sort: true
				},
				{
					title: "ssTest.components.addUser.placeholders.middlename",
					propertyName: 'student_fname',
					sort: true
				},
				{
					title: "ssTest.components.students.gradeBook",
					propertyName: 'gradebook_id',
					sort: false
				},
				{
					title: "ssTest.components.students.password",
					propertyName: 'plain_password',
					sort: false
				}
			];

			$scope.studentOptions = {
				entityId: "user_id",
				state: 'admin.studentResult',
				delNameService: 'delStudent',
				hideTableActions: {
					edit: true
				}
			};

			function showStudentsByGroup() {
				$scope.groupId = $stateParams.id;
				$scope.studentHead.push(
					{
						title: "ssTest.components.students.rating",
						propertyName: 'averageResult',
						sort: true,
						percentage: true
					}
				);

				AdminServices.getStudentsByGroup($scope.groupId).then(function(students) {
					if (Array.isArray(students[0])) $scope.empty = true;
					var resultsPromises = [];
					$scope.students = students;
					angular.forEach(students, function(student) {
						resultsPromises.push(StudentProvider.getResultsDataByStudent(student.user_id));
					});
					return $q.all(resultsPromises);

				}).then(function(result) {
					var i;
					var idSubject;
					var subjects;

					for (i = 0; i < $scope.students.length; i++) {
						subjects = result[i].scoreListsBySubjects;
						for (idSubject in subjects) {
							subjects[idSubject] = StudentProvider.getRating(subjects[idSubject]);
						}
						$scope.students[i].averageResult = +StudentProvider.getRating(subjects);
					}
					$scope.loaded = true;
				});

				AdminServices.getGroups($scope.groupId).then(function(data) {
					$scope.additionTitle = data[0].group_name;
					$sessionStorage.breadcrumbs.group = {
						title: $scope.additionTitle,
						id: $stateParams.id
					};

					$rootScope.$emit('loadedEntity');
				});
			}

			function showStudents() {
				$scope.allStudents = true;

				$scope.studentHead.push({
					title: "ssTest.components.students.group",
					propertyName: 'group_id',
					sort: true
				});

				$scope.studentOptions.pagination = true;
				$scope.studentOptions.itemsPerPage = 10;

				function getIdArray(students) {
					var idArray = [];

					for (var i = 0; i < students.length; i++) {
						if (idArray.indexOf(students[i].group_id) === -1) {
							idArray.push(students[i].group_id);
						}
					}

					return idArray;
				}

				function bindIdsWithNames(data) {
					var students = data;
					var idArray = getIdArray(data);

					AdminServices.getEntityValues('Group', idArray).then(function(data) {
						for (var i = 0; i < data.length; i++) {
							for (var j = 0; j < students.length; j++) {
								if (data[i].group_id === students[j].group_id) {
									students[j].group_id = data[i].group_name;
								}
							}
						}
						$scope.students = students;
					});
				}

				function getRange(quant, displ) {
					AdminServices.getRangeOfStudents(quant, displ).then(function(data) {
						if (Array.isArray(data[0])) $scope.empty = true;
						bindIdsWithNames(data);
						$scope.studentOptions.numItem = displ;
						$scope.loaded = true;
					});
				}

				function getAll() {
					$scope.studentOptions.pagination = false;

					AdminServices.getStudents().then(function(data) {
						bindIdsWithNames(data);

						$scope.studentOptions.numItem = 0;
					});
				}

				AdminServices.countStudents().then(function(data) {
					$scope.studentOptions.quantity = data.numberOfRecords;
				});

				$scope.studentOptions.getRange = getRange;
				$scope.studentOptions.getAll = getAll;
			}

			switch ($state.current.name) {
				case 'admin.students':
					showStudentsByGroup();
					break;
				case 'admin.studentsList':
					showStudents();
			}
		}]);
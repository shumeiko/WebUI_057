angular.module('speciality', [])
	.controller('SpecialitiesListCtrl', ['$scope', 'AdminServices', function($scope, AdminServices) {
		$scope.specialityHead = [
			{
				title: 'ssTest.components.specialities.speciality',
				propertyName: 'speciality_name',
				sort: true
			},
			{
				title: 'ssTest.components.specialities.code',
				propertyName: 'speciality_code',
				sort: false
			}
		];

		function getRange(quant, displ) {
			AdminServices.getRangeOfSpecialities(quant, displ).then(function(data) {
				if (Array.isArray(data[0])) $scope.empty = true;
				$scope.specialities = data;
				$scope.specialityOptions.numItem = displ;
				$scope.loaded = true;
			});
		}

		function getAll() {
			$scope.specialityOptions.pagination = false;

			AdminServices.getSpecialities().then(function(data) {
				$scope.specialityOptions.numItem = 0;
				$scope.specialities = data;
			});
		}

		$scope.specialityOptions = {
			entityId: "speciality_id",
			state: false,
			editState: 'admin.editSpeciality',
			delNameService: 'delSpeciality',
			pagination: true,
			itemsPerPage: 10,
			getRange: getRange,
			getAll: getAll
		};

		AdminServices.countSpecialities().then(function(data) {
			$scope.specialityOptions.quantity = data.numberOfRecords;
		});
	}]);
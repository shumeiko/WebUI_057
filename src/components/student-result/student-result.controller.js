angular.module('studentResult', [])
	.controller('StudentResultsListCtrl', ['$scope', '$rootScope', '$stateParams', '$sessionStorage', '$q', '$state', 'StudentProvider', 'AdminServices',
		function($scope, $rootScope, $stateParams, $sessionStorage, $q, $state, StudentProvider, AdminServices) {
			var studentId;
			$scope.studentResultHead = [
				{
					title: 'ssTest.components.studentResults.test',
					propertyName: 'testName',
					sort: true
				},
				{
					title: 'ssTest.components.studentResults.subject',
					propertyName: 'subjectName',
					sort: true
				},
				{
					title: 'ssTest.components.studentResults.date',
					propertyName: 'session_date',
					sort: true
				},
				/*{
				 title: 'ssTest.components.studentResults.start',
				 propertyName: 'start_time',
				 sort: false
				 },
				 {
				 title: 'ssTest.components.studentResults.end',
				 propertyName: 'end_time',
				 sort: false
				 },*/
				{
					title: 'ssTest.components.studentResults.rating',
					propertyName: 'result',
					sort: false,
					percentage: true
				}
			];

			$scope.studentResultOptions = {
				entityId: 'session_id',
				hideTableActions: {
					edit: true
				}
			};

			if ($state.current.name === 'user.results') {

				studentId = localStorage["idForUser"];
				$scope.studentResultOptions.state = 'user.resultDetail';
				$scope.studentResultOptions.hideTableActions.delete = true;

			} else if ($state.current.name === 'admin.studentResult') {

				studentId = $stateParams.id;
				$scope.studentResultOptions.state = 'admin.resultDetail';
				$scope.studentResultOptions.delNameService = 'delStudentResult';
			}

			AdminServices.getStudents(studentId).then(function(data) {
				$scope.additionTitle = data[0].student_surname + ' ' + data[0].student_name;

				if ($state.current.name === 'admin.studentResult') {

					$sessionStorage.breadcrumbs.student = {
						title: $scope.additionTitle,
						id: $stateParams.id
					};
					return AdminServices.getGroups(data[0].group_id).then(function(group) {
						$sessionStorage.breadcrumbs.group = {
							title: group[0].group_name,
							id: group[0].group_id
						};
						$rootScope.$emit('loadedEntity');
					});
				}
			});

			StudentProvider.getResultsDataByStudent(studentId).then(function(data) {
				if (!data) return $scope.empty = true;
				var idSubject;
				var result = data.testsResults;
				var subjects = data.scoreListsBySubjects;
				var subjectsIdList = AdminServices.getUniqueIdsList(result, 'subjectId');

				for (idSubject in subjects) {
					subjects[idSubject] = StudentProvider.getRating(subjects[idSubject]);
				}
				$scope.averageScore = StudentProvider.getRating(subjects);

				AdminServices.getEntityValues('Subject', subjectsIdList).then(function(subjects) {
					var i;
					var j;
					var subjectsNames = {};
					$scope.subjects = [];

					for (i = 0; i < subjects.length; i++) {
						subjectsNames[subjects[i].subject_id] = subjects[i].subject_name;
						$scope.subjects.push(subjects[i].subject_name);
					}

					for (j = 0; j < result.length; j++) {
						result[j].subjectName = subjectsNames[result[j].subjectId];
					}

					$scope.studentResults = result;
					$scope.chartData = {
						options: {
							title: false
						},
						chartMarks: StudentProvider.getChartMarks(data.scoreListsBySubjects, subjectsNames)
					};
				});
			});
		}]);
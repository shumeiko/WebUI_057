(function() {
	"use strict";


	angular.module('ssTest')
		.controller('ModalCtrl', ["$scope", "$modalInstance", 'data', function($scope, $modalInstance, data) {

		$scope.modalData = data;

		$scope.action = function() {
			if (data && data.action && data.action.actionFn){
				data.action.actionFn();
			}
			$modalInstance.close();
		};

		$scope.cancel = function() {
			$modalInstance.close();
		};
	}]);

}());
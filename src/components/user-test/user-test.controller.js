angular
	.module('user-test', [])
	.controller('UserTestCtrl', ['$scope', '$q', 'UserService', function($scope, $q, UserService) {
		$scope.testHead = [
			{
				title: 'ssTest.components.userTests.testName',
				propertyName: 'test_name',
				sort: true
			},
			{
				title: 'ssTest.components.userTests.tasks',
				propertyName: 'tasks',
				sort: false
			},
			{
				title: 'ssTest.components.userTests.timeForTest',
				propertyName: 'time_for_test',
				sort: false
			},
			{
				title: 'ssTest.components.userTests.attempts',
				propertyName: 'attempts',
				sort: false
			}
		];

		$scope.testOptions = {
			entityId: "test_id",
			state: 'user.test',
			editState: false,
			hideTableActions: {
				edit: true,
				delete: true
			}
		};

		$scope.getTests = function(id) {
			UserService.getTestsBySubject(id).then(function(data) {
				$scope.tests = data;
			});
		};

		UserService.getStudents(localStorage["idForUser"]).then(function (student) {
			return UserService.getTimeTablesForGroup(student[0].group_id)
		}).then(function (timeTable) {
			var i;
			var date;
			var promises = [];
			angular.forEach(timeTable, function(item) {
				date = item.event_date;
				promises.push(UserService.getSubjects(item.subject_id))
			});
			$q.all(promises).then(function(subjects) {
				for (i = 0; i < subjects.length; i++) {
					subjects[i][0].eventDate = date;
				}
				$scope.subjects = subjects;
				console.log(subjects);
			})
		});
	}]);


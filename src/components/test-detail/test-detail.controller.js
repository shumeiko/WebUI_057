angular
	.module('testParam')
	.controller('testParamCtrl', ['$scope', '$stateParams', '$sessionStorage', 'AdminServices',
		function($scope, $stateParams, $sessionStorage, AdminServices) {
			$scope.testId = $stateParams.id;


			$scope.testDetailsHead = [
				{
					title: "ssTest.components.addTestDetail.additions.level",
					propertyName: 'level',
					sort: true
				},
				{
					title: "ssTest.components.addTestDetail.additions.tasks",
					propertyName: 'tasks',
					sort: true
				},
				{
					title: "ssTest.components.addTestDetail.additions.rate",
					propertyName: 'rate',
					sort: false
				}
			];

			$scope.testDetailsOptions = {
				entityId: "id",
				state: false,
				delNameService: 'delTestDetail',
				editState: 'admin.editDetailOfTest'
			};

			AdminServices.getTestDetailsByTest($stateParams.id).then(function(data) {
				var testDetails = data;
				var i, countTasks, countRates, level;
				level = [];
				countTasks = 0;
				countRates = 0;
				function getTasks() {
					for(i=0; i<testDetails.length; i++){
					countTasks += parseInt(testDetails[i].tasks);
					}
					return countTasks;
				}
				function getRates() {
					for(i=0; i<testDetails.length; i++){
						countRates += parseInt(testDetails[i].rate);
					}
					return countRates;
				}
				function getLevel() {
					for(i=0; i<testDetails.length; i++){
						level.push(testDetails[i].level);
					}
					return level.length;
				}
				$scope.countTasks = getTasks();
				$scope.countRates = getRates();
				$scope.countLevels = getLevel();
				if (Array.isArray(testDetails[0])) $scope.empty = true;
				$scope.testDetails = testDetails;
				$scope.loaded = true;
			});
			AdminServices.getTests($scope.testId).then(function(data) {
				$scope.additionTitle = data[0].test_name;
			});
			$sessionStorage.breadcrumbs.testDetailId = $stateParams.id;
	}]);

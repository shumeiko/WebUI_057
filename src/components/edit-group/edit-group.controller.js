angular
	.module('admin')
	.controller('EditGroupCtrl', ["$scope", "$stateParams", "AdminServices", "$q", function($scope, $stateParams, AdminServices, $q) {
		$scope.groupData = false;

		var groupData = {
			title: 'Група',
			key: 'group_name',
			state: 'admin.students',
			txt: 'ssTest.components.editGroup'
		};

		$scope.editGroup = function(entityData){
			return AdminServices.editGroup(entityData, $stateParams.id);
		};

		var pushDepList = function(depList, name) {
			var arr = [];
			for (var i = 0, l = depList.length; i < l; i++) {
				arr.push({
					id: depList[i][name + "_id"],
					name: depList[i][name + "_name"]
				})
			}
			return arr;
		};

		var getDepItem = function(depList, names){
			return {
				dependencyEntities: pushDepList(depList, names.name),
				key: names.name + "_id",
				txt: 'ssTest.components.editGroup.dependencies.' + names.name
			};
		};

		var formatDependency = function(arr) {
			var names = [
				{
					name: 'speciality'
				},
				{
					name: 'faculty'
				}
			];
			var depArr = [];

			for (var i = 0, l = arr.length; i < l; i++) {
				depArr.push(getDepItem(arr[i], names[i]));
			}

			return depArr;
		};

		$q.all([AdminServices.getSpecialities(), AdminServices.getFaculties(), AdminServices.getGroups($stateParams.id)]).then(function(data) {
			$scope.groupDependencies = formatDependency(data.slice(0, data.length-1));
			$scope.groupData = groupData;

			$scope.groupData.name = data[2][0].group_name;
			$scope.groupDependencies[0].id = data[2][0].speciality_id;
			$scope.groupDependencies[1].id = data[2][0].faculty_id;
		});

	}]);
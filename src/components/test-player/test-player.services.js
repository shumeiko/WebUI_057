angular
	.module('testPlayer')
	.factory('tpHttpServices', tpHttpServices)
	.factory('tpServices', tpServices);

tpHttpServices.$inject = ['appConstants', '$http', 'HttpHelper'];
function tpHttpServices(appConstants, $http, HttpHelper) {
	'use strict';
	return {
		getEntityValues: getEntityValues,
		getQuestions: getQuestions,
		getQuestionsByLevelRand: getQuestionsByLevelRand,

		getAnswersByQuestion: getAnswersByQuestion,
		checkAnswers: checkAnswers,

		getTestsInfo: getTestsInfo,
		getTestDetailsByTest: getTestDetailsByTest,

		addResult: addResult,

		startTest: startTest,
		getTimeStamp: getTimeStamp,

		getLogsByUser: getLogsByUser,
		getLog: getLog,
		getLogs: getLogs
	};

	function getQuestions(id) {
		return HttpHelper($http.get(appConstants.getQuestionsURL + "/" + id, {cache: true}));
	}

	function getQuestionsByLevelRand(idTest, levelOfQuestion, countOfQuestions) {
		return HttpHelper($http
			.get(appConstants.getQuestionsByLevelRandURL + '/' + idTest +
			'/' + levelOfQuestion + '/' + countOfQuestions));
	}

	function getAnswersByQuestion(id) {
		return HttpHelper($http
			.get(appConstants.getAnswersByQuestUserURL + "/" + id, {cache: true}));
	}

	function getTestsInfo(id) {
		return HttpHelper($http
			.get(appConstants.getTestsURL + "/" + id));
	}

	function getTestDetailsByTest(id) {
		return HttpHelper($http
			.get(appConstants.getTestDetailsByTestURL + '/' + id, {cache: true}));
	}

	function checkAnswers(data) {
		return HttpHelper($http.post(appConstants.checkAnswersURL, data));
	}

	function addResult(data) {
		return HttpHelper($http.post(appConstants.addResultsURL, data));
	}

	function getEntityValues(entity, idArr) {
		var input = {
			entity: entity,
			ids: idArr
		};
		return HttpHelper($http.post(appConstants.getEntityValuesURL, input));
	}


	function startTest(userId, testId) {
		return HttpHelper($http.get(appConstants.startTestURL + '/' + userId + '/' + testId));
	}

	function getTimeStamp() {
		return HttpHelper($http.get(appConstants.getTimeStampURL));
	}

	function getLogsByUser(userId) {
		return HttpHelper($http.get(appConstants.getLogsByUserURL + '/' + userId));
	}

	function getLog(logId) {
		return HttpHelper($http.get(appConstants.getLogsURL + '/' + logId));
	}
	function getLogs() {
		return HttpHelper($http.get(appConstants.getLogsURL));
	}
}

tpServices.$inject = ["UserService", 'AuthService', 'tpHttpServices', '$q',"$sessionStorage"];
function tpServices(UserService, AuthService, tpHttpServises, $q, $sessionStorage) {
	'use strict';
	return {
		timeParser: timeParser,
		createRows: createRows,
		serializeArray: serializeArray,
		getInitialTestData: getInitialTestData,
		checkTest: checkTest,
		addMinutes: addMinutes,
		checkAvailableTest: checkAvailableTest
	};


	function checkAvailableTest(studentId, tInfo) {
		return $q(function(resolve, reject) {
			var attempts= tInfo.attempts;
			var tId = tInfo.test_id;
			UserService
				.getResultsByStudent(studentId)
				.then(function(studentResults) {
					studentResults
						.forEach(function(result) {
							if (result.test_id === tId) {
								--attempts;
							}
						});
					if (attempts > 0) {
						resolve(true);
					} else if (attempts === 0) {
						reject(false);
					}
				});
		});
	}


	function addMinutes(date, minutes) {
		return new Date(date.getTime() + minutes * 60000);
	}

	function createRows(to) {
		var rows = [];
		var row;
		var cols = 4;

		for (var i = 1; i <= to; i += cols) {
			row = [];
			for (var j = i, jj = i + cols; j < jj; j++) {

				row.push(j);
				if (j === to) {
					break;
				}
			}
			rows.push(row);
		}
		return rows;
	}

	function getInitialTestData(tId) {
		return $q(function(resolve) {
			var initTestData = {};
			$q.all([
				tpHttpServises
					.getTestsInfo(tId),
				tpHttpServises
					.getTestDetailsByTest(tId),
				AuthService.isLoggedIn()
			])
				.then(function(data) {
					initTestData.tInfo = data[0][0];
					initTestData.tDetails = data[1];
					initTestData.studentId = data[2].id;
					initTestData.tInfo.tasks = 0;
					initTestData.tDetails.forEach(function(tD) {
						initTestData.tInfo.tasks += parseInt(tD.tasks);
					});

					return getQuestionsByTestDetails(tId, initTestData);
				})
				.then(function(qBank) {
					sortQuestionsByLevel(qBank);
					initTestData.qBank = qBank;
					resolve(initTestData);
				});
		});
	}

	function getQuestionsByTestDetails(tId, initTestData) {
		return $q(function(resolve) {
			var qBank = [];

			initTestData.tDetails.forEach(function(tD, index) {
				tpHttpServises
					.getQuestionsByLevelRand(tId, tD.level, tD.tasks)
					.then(function(data) {
						data.forEach(function(q) {
							qBank.push({
								question_id: q.question_id,
								level: q.level
							});
						});
						return getAnswersToQuestions(qBank);
					})
					.then(function(data) {
						if (qBank.length === initTestData.tInfo.tasks) {
							resolve(qBank);
						}
					})
			});
		});
	}

	function getAnswersToQuestions(partOfQuestions) {
		return $q(function(resolve) {
				partOfQuestions.forEach(function(q, index, array) {
						tpHttpServises
							.getAnswersByQuestion(q.question_id)
							.then(function(data) {
								var arrayOfIds = [];
								data.forEach(function(a) {
									arrayOfIds.push({
										answer_id: a.answer_id
									});
								});
								createRandomArray(arrayOfIds);
								partOfQuestions[index].answers = arrayOfIds;
								partOfQuestions[index].sAnswer = null;
								partOfQuestions[index].mAnswers = [];
							}).then(function() {
								for(var i = 0; i < array.length; i++) {
									if(array[i].answers === undefined) {
										return;
									}
								}
								if (index === array.length - 1) {
									resolve(partOfQuestions);
								}
							});
					});
			});
	}

	function timeParser(inputSeconds) {
		var h = parseInt(inputSeconds / 3600);
		var m = parseInt((inputSeconds - (h * 3600)) / 60);
		var s = inputSeconds - h * 3600 - m * 60;
		if (h > 0) {
			return {
				h: h,
				m: m,
				s: s
			};
		} else {
			return {
				m: m,
				s: s
			};
		}
	}

	function createRandomArray(array) {
		array.sort(function(a, b) {
			return Math.round(Math.random());
		});
	}

	function serializeArray(inputArray) {
		var serializedArray = '';
		inputArray.forEach(function(arrayOfInputArray, indexAnsw, arrayAnsw) {
			if (arrayOfInputArray instanceof Array) {
				// Array Of Array
				serializedArray += '[';
				if (arrayOfInputArray.length < 2) {
					serializedArray += arrayOfInputArray;
				} else {
					arrayOfInputArray.forEach(function(item, index, array) {
						serializedArray += item;
						if (index < array.length - 1) {
							serializedArray += ":";
						}
					});
				}
				serializedArray += ']';
			} else {
				// String or number;
				serializedArray += arrayOfInputArray;
			}
			if (indexAnsw < arrayAnsw.length - 1) {
				serializedArray += '/';
			}
		});
		return serializedArray;
	}

	function sortQuestionsByLevel(qArray) {
		qArray.sort(function(a, b) {
			a = parseInt(a.level);
			b = parseInt(b.level);
			if (a < b) {
				return -1;
			} else if (a > b) {
				return 1;
			} else {
				return 0;
			}
		});
	}
		// finish test handling
	function checkTest() {
		return $q(function(resolve, reject) {
			var result = {};
			var arrForCheck;

			arrForCheck = prepareArrayForCheck($sessionStorage.qBank);

			tpHttpServises
				.checkAnswers(arrForCheck)
				.then(function(checkedArray) {
					createResultByLevel(result, checkedArray);
					createPercentageValue(result);
					getUserAnswersForStoring(result, checkedArray);
					resolve(result);
				}, function(error) {
					reject("Can't check answers." + error);
				});
		});
	}

	function prepareArrayForCheck(qBank) {
		var arrayForCheck = [];
		var answer = [];
		qBank.forEach(function(q) {
			if (q.answered) {
				if (q.sAnswer) {
					answer.push(q.sAnswer);
				} else {
					answer = q.mAnswers;
				}
				arrayForCheck.push({
					"question_id": q.question_id,
					"answer_ids": answer
				});
				answer = [];
			}
		});
		return arrayForCheck;
	}

	function createResultByLevel(result, checkedArray) {
		result.byLevel = [];
		var idsRightQuestions;
		idsRightQuestions = checkedArray.filter(function(item) {
			return item.true === 1;
		}).map(function(item) {
			return item.question_id;
		});
		$sessionStorage.tDetails.forEach(function(tD) {
			result.byLevel.push({
				level: parseInt(tD.level),
				qCost: parseInt(tD.rate),
				qQnty: parseInt(tD.tasks),
				qRight: getRightQuestionsByLevel(tD.level, idsRightQuestions)
			});
		});
	}

	function getRightQuestionsByLevel(level, idsRightQuestions) {
		var qRight = 0;
		$sessionStorage.qBank.forEach(function(q) {
			if (q.level === level && idsRightQuestions.indexOf(q.question_id) !== -1) {
				++qRight;
			}
		});
		return qRight;
	}

	function createPercentageValue(result) {
		var sumScore = 0;
		result.pValue = 0;

		result.byLevel.forEach(function(data) {
			sumScore += data.qCost * data.qQnty;
		});

		result.byLevel.forEach(function(level) {
			result.pValue += Math.round(level.qCost * level.qRight / sumScore * 100);
		});
		return result.pValue;
	}

	function getUserAnswersForStoring(result, checkedArray) {
		var answer;
		var trueAnswer;

		result.questions = [];
		result.answers = [];
		result.true_answers = [];

		$sessionStorage.qBank.forEach(function(q) {
			answer = q.mAnswers.length === 0 ? q.sAnswer : q.mAnswers;
			if (q.answered === true) {
				result.questions.push(q.question_id);
				result.answers.push(answer);

				for (var i = 0, ii = checkedArray.length; i < ii; i++) {
					if (checkedArray[i].question_id === q.question_id) {
						trueAnswer = checkedArray[i].true;
						break;
					}
				}
				result.true_answers.push(trueAnswer);
			} else {
				result.questions.push(q.question_id);
				result.answers.push(0);
				result.true_answers.push(0);
			}
		});

		result.questions = serializeArray(result.questions);
		result.answers = serializeArray(result.answers);
		result.true_answers = serializeArray(result.true_answers);
	}
}
angular
	.module("testPlayer")
	.controller("FinishTestCtrl", FinishTestCtrl);

FinishTestCtrl.$inject = ["$scope", '$q', "$state", "$stateParams", "tpServices",
	'$sessionStorage', 'tpHttpServices','AuthService'];

function FinishTestCtrl($scope, $q, $state, $stateParams, tpServices,
						$sessionStorage, tpHttpServises, AuthService) {

	if ($sessionStorage.qBank === undefined ) {
		$scope.activeResult = false;
	} else {
		$q.all([
			tpServices.checkTest(),
			tpHttpServises.getLog($sessionStorage.logId),
			AuthService.isLoggedIn()
		]).then(function(data) {
			$scope.result = data[0];
			$scope.chartData = data[0].byLevel;
			$scope.result.studentId = data[1].id;
			$scope.activeResult = true;
			return tpHttpServises.addResult({
				student_id: data[2].id,
				test_id: parseInt($stateParams.id),
				session_date: data[1][0].log_date,
				start_time: data[1][0].log_time,
				end_time: $sessionStorage.endTime,
				result: data[0].pValue,
				questions: data[0].questions,
				answers: data[0].answers,
				true_answers: data[0].true_answers
			}).then(function() {
				$sessionStorage.$reset();
			});
		});
	}
}
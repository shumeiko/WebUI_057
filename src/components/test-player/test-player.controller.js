angular
	.module('testPlayer')
	.controller("tpCtrl", tpCtrl);

tpCtrl.$inject = ["$q", '$window', "$scope", "$state", "$stateParams", "questionsTypes",
	"tpServices", "$sessionStorage", "$interval", "ModalService", "$filter", "tpHttpServices"];

function tpCtrl($q, $window, $scope, $state, $stateParams, questionsTypes, tpServices,
				$sessionStorage, $interval, ModalService, $filter, tpHttpServices) {
	'use strict';
	var cost;
	var testId = $stateParams.id;
	var interval;
	var listenerStateChange;
	var canLeaveTest;
	var studentId;
	var logId;
	var tInfo;

	$scope.showBody = false;

	$scope.progressBarValue = 0;
	$scope.getQuestion = getQuestion;
	$scope.answerToQuestion = answerToQuestion;
	$scope.toggleAnswers = toggleAnswers;
	$scope.markQuestion = markQuestion;
	$scope.checked = checked;
	$scope.finishTest = finishTestButton;
	$scope.questionsTypes = questionsTypes;
	$scope.currentQuestion = 0;

	function changeStateListener(event, toState) {
		if (canLeaveTest) {
			return;
		} else {
			event.preventDefault();
			ModalService.open({
				data: {
					title: "ssTest.components.tPlayer.onChangeStateHeader",
					log: {
						logMsg: "ssTest.components.tPlayer.onChangeStateBody"
					},
					action: {
						actionTxt: "ssTest.components.tPlayer.areYouSure",
						actionFn: function() {
							// remove listener
							listenerStateChange();
							$state.go(toState.name);
						}
					}
				}
			});
		}
	}

	tpServices
		.getInitialTestData(testId)
		.then(function(data) {
			tInfo = data.tInfo;
			$sessionStorage.tDetails = data.tDetails;
			studentId = data.studentId;

			tpServices.checkAvailableTest(studentId, tInfo)
				.then(function(available) {
					if (available) {

						listenerStateChange = $scope.$on('$stateChangeStart',
							changeStateListener);

						$window.onbeforeunload = function() {
							return $filter('translate')("ssTest.components.tPlayer.areYouSure");
						};

						$scope.$on('$destroy', function() {
							$window.onbeforeunload = undefined;
							$interval.cancel(interval);
							listenerStateChange();
						});

						$scope.showBody = true;
						if (!$sessionStorage.qBank) {
							$sessionStorage.qBank = data.qBank;
						}
						startTest();
					}
				});
		});

	function startTest() {
		createEntityLog(studentId, testId)
			.then(function(data) {
				getTimeToFinish(testId, data).then(function(time) {
					timer(time);
				});
			});

		canLeaveTest = false;
		cost = Math.round(100 / tInfo.tasks);
		$scope.qBank = $sessionStorage.qBank;
		$scope.rows = tpServices.createRows(tInfo.tasks);
		getQuestion(getNextUnansweredQuestionNumber());
		$sessionStorage.qBank.forEach(function(q) {
			if (q.answered) {
				$scope.progressBarValue += cost;
			}
		});
	}

	function timer(seconds) {
		interval = $interval(function() {
			var currentTime;
			if (seconds > 0) {
				if (seconds < 60 && seconds % 2 === 0) {
					$scope.testReadyToFinish = true;
				}
				seconds -= 1;
				currentTime = tpServices.timeParser(seconds);
				if (currentTime.s.toString().length < 2) {
					currentTime.s = '0' + currentTime.s;
				}
				$scope.sec = currentTime.s;
				$scope.m = currentTime.m;
			} else if (seconds === 0) {
				$interval.cancel(interval);
				canLeaveTest = true;
				finishTest();

			}
		}, 1000);
	}

	function updateProgress() {
		$scope.progressBarValue += cost;
	}

	function markQuestion() {
		if ($sessionStorage.qBank[$scope.currentQuestion].marked) {
			$sessionStorage.qBank[$scope.currentQuestion].marked = null;
		} else {
			$sessionStorage.qBank[$scope.currentQuestion].marked = true;
		}

	}

	function getQuestion(number) {
		var qId;
		var arrAnswIds = [];
		qId = $sessionStorage.qBank[number].question_id;

		//if($sessionStorage.qBank[number].answers === undefined) {
		//
		//} else {
			$sessionStorage.qBank[number].answers.forEach(function(a) {
				arrAnswIds.push(a.answer_id);
			});
		//}


		$q.all([
			tpHttpServices.getQuestions(qId),
			tpHttpServices.getEntityValues('SAnswer', arrAnswIds)
		]).then(function(data) {
			$scope.question = data[0][0];
			$scope.question.answers = data[1];
			$scope.question.mAnswers = $sessionStorage.qBank[$scope.currentQuestion].mAnswers;

		});
		$scope.sAnswer = $sessionStorage.qBank[number].sAnswer;
		$scope.currentQuestion = number;
	}

	function answerToQuestion() {
		if (!$sessionStorage.qBank[$scope.currentQuestion].answered) {
			updateProgress();
		}
		$sessionStorage.qBank[$scope.currentQuestion].answered = true;
		$sessionStorage.qBank[$scope.currentQuestion].sAnswer = $scope.sAnswer;

		if (isAllAnswered()) {
			showModalWindowFinishTest();
			return;
		} else {
			getQuestion(getNextUnansweredQuestionNumber());
			return;

		}
		getQuestion($scope.currentQuestion + 1);
	}

	function showModalWindowFinishTest() {
		ModalService.open({
			data: {
				title: "ssTest.components.tPlayer.answerToAllQuestionsHeader",
				log: {
					logMsg: "ssTest.components.tPlayer.answerToAllQuestions"
				},
				action: {
					actionTxt: "ssTest.components.tPlayer.finishTest",
					actionFn: function() {
						canLeaveTest = true;
						finishTest();
					}
				}
			}
		});
	}

	function toggleAnswers(answerId) {
		var exists = $sessionStorage.qBank[$scope.currentQuestion].mAnswers.indexOf(answerId);
		if (exists > -1) {
			$sessionStorage.qBank[$scope.currentQuestion].mAnswers.splice(exists, 1);
		} else {
			$sessionStorage.qBank[$scope.currentQuestion].mAnswers.push(answerId);
		}
	}

	function checked(answerId) {
		return $sessionStorage.qBank[$scope.currentQuestion].mAnswers.indexOf(answerId) > -1;
	}

	function isAllAnswered() {
		for (var i = 0, ii = $sessionStorage.qBank.length; i < ii; i++) {
			if (!$sessionStorage.qBank[i].answered) {
				return false;
			}
		}
		return true;
	}

	function getNextUnansweredQuestionNumber() {
		var i;
		var ii = $sessionStorage.qBank.length;
		var from = $scope.currentQuestion;
		for (i = from; i < ii; i++) {
			if (!$sessionStorage.qBank[i].answered) {
				return i;
			}

		}
		for (i = 0; i < from; i++) {
			if (!$sessionStorage.qBank[i].answered) {
				return i;
			}

		}
		return 0;
	}

	function finishTestButton() {
		var allAnswered = isAllAnswered();
		if (allAnswered) {
			finishTest();
		} else {
			ModalService.open({
				data: {
					title: "ssTest.components.tPlayer.answerNotToAllQuestionsHeader",
					log: {
						logMsg: "ssTest.components.tPlayer.answerNotToAllQuestions"
					},
					action: {
						actionTxt: "ssTest.components.tPlayer.finishTest",
						actionFn: function() {
							// remove listener
							listenerStateChange();
							finishTest();
						}
					}
				}
			});
		}
	}

	function finishTest() {
		canLeaveTest = true;
		tpHttpServices.getTimeStamp()
			.then(function(data) {
				var currentTime = new Date(data.unix_timestamp * 1000);
				$sessionStorage.endTime = currentTime.getHours()
					+ ':'
					+ currentTime.getMinutes()
					+ ':'
					+ currentTime.getSeconds();
				$state.go('user.finish-test', {id: testId});
			});

	}

	function getTimeToFinish(testId, logId) {
		return $q(function(resolve) {
			var timeForTest;
			var currentTime;
			var finishTime;
			var startTime;
			tpHttpServices
				.getTestsInfo(testId)
				.then(function(data) {
					timeForTest = data[0].time_for_test * 60000;
					return tpHttpServices.getLog(logId);
				})
				.then(function(data) {

					startTime = new Date(data[0].log_date + 'T' + data[0].log_time);

					finishTime = new Date(startTime.getTime() + timeForTest);

					tpHttpServices.getTimeStamp().then(function(data) {
						currentTime = new Date(data.curtime * 1000);
						if (finishTime - currentTime < 0) {
							resolve(0);
							return;
						}
						resolve(((finishTime - currentTime) / 1000));
					});
				});
		});
	}

	function createEntityLog(studentId, testId) {
		return $q(function(resolve) {
			tpHttpServices.startTest(studentId, testId)
				.then(function(data) {
					if (data.response === 'ok') {
						$sessionStorage.logId = data.id;
						logId = data.id;
						resolve(logId);
					} else if (data.response === "Error. User made test" +
						" recently") {
						// get latest record of log
						tpHttpServices.getLogs()
							.then(function(data) {
								for (var i = data.length - 1, ii = 0; i >= ii; i--) {
									if (data[i].user_id === studentId && data[i].test_id === testId) {
										$sessionStorage.logId = data[i].log_id;
										resolve(data[i].log_id);
									}
								}
							});
					}
				});
		});
	}
}
angular.module('addTestDetail')
	.controller('EditTestDetailCtrl', ['$scope', '$state', 'ModalService', '$translate','$stateParams', 'AdminServices',
		function ($scope, $state, ModalService, $translate, $stateParams, AdminServices) {

			AdminServices.getTestDetails($stateParams.id).then(function(data) {
				$scope.testDetail = data[0];
				$scope.testDetail.level = parseInt($scope.testDetail.level);
				$scope.testDetail.tasks = parseInt($scope.testDetail.tasks);
				$scope.testDetail.rate = parseInt($scope.testDetail.rate);
				$scope.testId = $scope.testDetail.test_id;
				return AdminServices.getTests($scope.testDetail.test_id);
			}).then(function(data) {
				$scope.additionTitle = data[0].test_name;
			});

			$scope.formSubmitSend = false;
			$scope.sendForm = function(){
				$scope.formSubmited = true;
			};

			var onSuccess = function(data) {
				ModalService.open({
					data: {
						title: "ssTest.SUCCESS",
						log: {
							logMsg: "ssTest.components.editTestDetail.editedMsg",
							logValues: {name: $scope.additionTitle}
						}
					}
				});
				$scope.formSubmitSend = false;
			};

			var onError = function(data) {
				ModalService.logError(data.response);
				$scope.formSubmitSend = false;
			};

			$scope.editTestDetail = function() {
				$scope.formSubmitSend = true;
				AdminServices.editTestDetail($scope.testDetail, $stateParams.id).then(
					function(data){
						if (data.response === "ok") {
							onSuccess(data);
						} else {
							onError(data);
						}
					},
					function(data) {
						onError(data);
					}
				);
			};
		}
	]);

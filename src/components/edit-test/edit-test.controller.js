angular.module('admin')
	.controller('EditTestCtrl', ['$scope', 'ModalService','$translate','$stateParams', 'AdminServices', function ($scope, ModalService, $translate, $stateParams, AdminServices) {
		AdminServices.getTests($stateParams.id).then(function(data) {
			var testData = data[0];
			if (/:/.test(testData.time_for_test)) {
				var timeArr = testData.time_for_test.split(":");
				testData.time_for_test = parseInt(timeArr[0]) * 60 + parseInt(timeArr[1]);
			} else {
				testData.time_for_test = parseInt(testData.time_for_test);
			}
			testData.attempts = parseInt(testData.attempts);
			testData.tasks = parseInt(testData.tasks);
			testData.time_for_test = parseInt(testData.time_for_test);
			$scope.test = data[0];
			$scope.subjectId = $scope.test.subject_id;
		});

		$scope.sendForm = function(){
			$scope.formSubmited = true
		};

		var onSuccess = function() {
			ModalService.open({
				data: {
					title: "ssTest.SUCCESS",
					log: {
						logMsg: "ssTest.components.editTest.editedMsg",
						logValues: {name: $scope.test.test_name}
					}
				}
			});
			$scope.formSubmited = false;
			$scope.formSubmitSend = false;
		};

		var onError = function(data) {
			ModalService.logError(data.response);
			$scope.formSubmited = false;
			$scope.formSubmitSend = false;
		};

		$scope.editTest = function() {
			$scope.formSubmitSend = true;

			AdminServices.editTest($scope.test, $stateParams.id).then(
				function(data){
					if (data.response === "ok") {
						onSuccess(data);
					} else {
						onError(data);
					}
				},
				function(data) {
					onError(data);
				}

			);

		};
	}]);

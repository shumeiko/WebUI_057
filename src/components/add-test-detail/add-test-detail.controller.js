angular.module('addTestDetail')
	.controller('AddTestDetailCtrl', ['$scope', '$state', 'ModalService', '$translate','$stateParams', 'AdminServices',
		function ($scope, $state, ModalService, $translate, $stateParams, AdminServices) {
			$scope.testDetail = {
				test_id: $stateParams.id
			};
			$scope.formSubmitSend = false;
			$scope.sendForm = function(){
				$scope.formSubmited = true;
			};

			$scope.resetForm = function() {
				$scope.addTestDetailForm.$setPristine();
				$scope.testDetail = {};
				$scope.formSubmited = false;
			};

			AdminServices.getTests($stateParams.id).then(function(data) {
				$scope.additionTitle = data[0].test_name;
			});

			var onSuccess = function(data) {
				ModalService.open({
					data: {
						title: "ssTest.SUCCESS",
						log: {
							logMsg: "ssTest.components.addTestDetail.addedMsg",
							logValues: {name: $scope.additionTitle}
						},
						action: {
							actionTxt: "ssTest.controls.continue",
							actionFn: function() {
								$state.go('admin.detailOfTest', {id: $stateParams.id});
							}
						}
					}
				});
				$scope.formSubmitSend = false;
				$scope.resetForm();
			};

			var onError = function(data) {
				ModalService.logError(data.response);
				$scope.formSubmitSend = false;
			};

			$scope.newTestDetail = function() {
				$scope.formSubmitSend = true;

				AdminServices.addTestDetail($scope.testDetail).then(
					function(data){
						if (data.response === "ok") {
							onSuccess(data);
						} else {
							onError(data);
						}
					},
					function(data) {
						onError(data);
					}

				);

			};

		}
	]);

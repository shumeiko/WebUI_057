'use strict';

angular
    .module('home-user',[])
    .controller('SubjectsUserListCtrl', ['$scope', '$q', 'UserService', function ($scope, $q, UserService) {

		UserService.getStudents(localStorage["idForUser"]).then(function (student) {
			return UserService.getTimeTablesForGroup(student[0].group_id)
		}).then(function (timeTable) {
			var i;
			var promises = [];

			for(i=0; i<timeTable.length; i++) {
				promises.push(UserService.getTestsBySubject(timeTable[i].subject_id));
			}
			$q.all(promises).then( function(testsBySubjects){
				var i, j;
				var tests = [];
				for(i=0; i<testsBySubjects.length; i++) {
					for(j=0; j<testsBySubjects[i].length; j++) {
						tests.push(testsBySubjects[i][j]);
					}
				}
				$scope.tests = tests;
				$scope.testId = tests[0].test_id;
			});

		});

}]);
describe('Controller: YearsCtrl', function() {
	var scope;

	beforeEach(module('calendar'));

	beforeEach(inject(
		function($rootScope, $controller) {
			scope = $rootScope.$new();

			scope.calendar = {year: 2015};
			$controller('YearsCtrl', {$scope: scope});
		}
	));

	it('should create properly set of years', function() {
		var years = scope.years;

		expect(years.length).toEqual(3);
		expect(years[0].length).toEqual(4);
		//check title of first year in set
		expect(years[0][0].title).toEqual(2009);
		expect(years[0][0].anotherYear).toEqual(true);
		//check current year
		expect(years[1][2].title).toEqual(2015);
		expect(years[1][2].anotherYear).not.toBeDefined();
	});
});
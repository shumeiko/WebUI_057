describe('Controller: MonthsCtrl', function() {
	var scope;

	beforeEach(module('calendar'));

	beforeEach(inject(
		function($rootScope, $controller) {
			scope = $rootScope.$new();

			scope.calendar = {};
			$controller('MonthsCtrl', {$scope: scope});
		}
	));

	it('should create properly year', function() {
		var months = scope.months;

		expect(months.length).toEqual(3);
		expect(months[0].length).toEqual(4);
		//check title of first month
		expect(months[0][0].title).toEqual('ssTest.calendar.months.shorthand.1');
		expect(months[0][0].index).toEqual(1);
		//check title of last month
		expect(months[2][3].title).toEqual('ssTest.calendar.months.shorthand.12');
		expect(months[2][3].index).toEqual(12);
	});
});
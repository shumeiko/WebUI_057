describe('Controller: InformCtrl', function() {
	var scope;
	var AdminServices;
	var httpBackend;

	beforeEach(module('ssTest'));

	beforeEach(inject(function($rootScope, $controller, _AdminServices_, $q, $httpBackend) {
		var entityValuesDeferred = $q.defer();
		var testsDeferred = $q.defer();
		var studentsDeferred = $q.defer();

		scope = $rootScope.$new();
		AdminServices = _AdminServices_;
		httpBackend = $httpBackend;

		scope.calendar = {eventDates: [{event_date: "2015-10-16"}]};
		scope.countEvents = [0, 1];

		$controller('InformCtrl', {
			$scope: scope,
			AdminServices: AdminServices,
			$q: $q
		});

		entityValuesDeferred.resolve([{subject_name: 'JS', group_name: 'AKT-1'}]);
		testsDeferred.resolve([
			{enabled: '1', test_name: 'Angular'},
			{enabled: '0', test_name: 'Backbone'}
		]);
		studentsDeferred.resolve({length: 10});
		spyOn(AdminServices, 'getEntityValues').and.returnValue(entityValuesDeferred.promise);
		spyOn(AdminServices, 'getTestsBySubject').and.returnValue(testsDeferred.promise);
		spyOn(AdminServices, 'getStudentsByGroup').and.returnValue(studentsDeferred.promise);
		httpBackend.whenGET('langs/lang-uk.json').respond('');
	}));

	it('should show properly inform dialog window', function() {
		scope.getTestEvent(0);
		scope.$digest();

		expect(scope.event.event_date).toEqual('16/10/2015');
		expect(scope.event.subject_name).toEqual('JS');
		expect(scope.event.group_name).toEqual('AKT-1');
		expect(scope.event.tests).toEqual(['Angular']);
		expect(scope.event.students).toEqual(10);
	});
});
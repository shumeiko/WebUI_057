describe('Controller: DatesCtrl', function() {
	var rootScope;
	var scope;
	var AdminServices;
	var translate;
	var deferred;
	var httpBackend;

	beforeEach(module('ssTest'));

	beforeEach(inject(
		function(_$rootScope_, $controller, _AdminServices_, $translate, $q, $httpBackend) {
			rootScope = _$rootScope_;
			scope = rootScope.$new();
			AdminServices = _AdminServices_;
			translate = $translate;
			deferred = $q.defer();
			httpBackend = $httpBackend;

			scope.calendar = {date: new Date()};
			deferred.resolve([{event_date: "2015-10-17"}, {event_date: "2015-10-17"}]);
			spyOn(AdminServices, 'getTimeTablesFromNowInMonth').and.returnValue(deferred.promise);

			$controller('DatesCtrl', {
				$scope: scope,
				AdminServices: AdminServices,
				$translate: translate
			});

			httpBackend.whenGET('langs/lang-uk.json').respond('');
		}
	));

	it('should create properly month: October 2015', function() {
		var weeks;

		scope.$digest();
		scope.calendar.createDates(2015, 9);
		weeks = scope.weeks;
		//check the year
		expect(scope.calendar.year).toEqual(2015);
		//check the month
		expect(scope.month).toEqual('ssTest.calendar.months.10');
		//check the number of weeks
		expect(weeks.length).toEqual(5);
		//check monday of first week
		expect(weeks[0][0].date).toEqual(28);
		expect(weeks[0][0].anotherMonth).toEqual(true);
		expect(scope.weeks[0][0].month).toEqual(9);
		//check first day of current month
		expect(weeks[0][3].date).toEqual(1);
		expect(weeks[0][3].month).toEqual(10);
		//check event day
		expect(weeks[2][5].date).toEqual(17);
		expect(weeks[2][5].month).toEqual(10);
		expect(weeks[2][5].event).toEqual(true);
		expect(weeks[2][5].eventIndexes).toEqual([0, 1]);
		//check last day of current month
		expect(weeks[4][5].date).toEqual(31);
		expect(weeks[4][5].month).toEqual(10);
		//check sunday of last week
		expect(weeks[4][6].date).toEqual(1);
		expect(weeks[4][6].anotherMonth).toEqual(true);
		expect(weeks[4][6].month).toEqual(11);
	});

	it('should create properly month with \'en\' locale: January 2016', function() {
		var weeks;

		scope.calendar.date.setFullYear(2016, 0);
		rootScope.$broadcast('localeChange', 'en');
		weeks = scope.weeks;
		//check the year
		expect(scope.calendar.year).toEqual(2016);
		//check the month
		expect(scope.month).toEqual('ssTest.calendar.months.1');
		//check the number of weeks
		expect(weeks.length).toEqual(6);
		//check monday of first week
		expect(weeks[0][0].date).toEqual(27);
		expect(weeks[0][0].anotherMonth).toEqual(true);
		expect(scope.weeks[0][0].month).toEqual(0);
		//check first day of current month
		expect(weeks[0][5].date).toEqual(1);
		expect(weeks[0][5].month).toEqual(1);
		//check last day of current month
		expect(weeks[5][0].date).toEqual(31);
		expect(weeks[5][0].month).toEqual(1);
		//check sunday of last week
		expect(weeks[5][6].date).toEqual(6);
		expect(weeks[5][6].anotherMonth).toEqual(true);
		expect(weeks[5][6].month).toEqual(2);
	});
});
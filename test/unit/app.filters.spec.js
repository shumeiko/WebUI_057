describe('Filter: decode', function() {
	var filter;

	beforeEach(module('ssTest'));

	beforeEach(inject(function($filter) {
		filter = $filter;
	}));

	it('should transform special symbols\' code into symbols', function() {
		expect(filter('decode')('&lt;script&gt;')).toEqual('<script>');
	});
});
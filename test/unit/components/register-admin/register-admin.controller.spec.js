describe('Controller: RegisterAdminCtrl', function() {
	var scope;
	var ctrl;
	var state;
	var AdminServices;
	var ModalService;
	var deferred;
	var httpBackend;

	beforeEach(module('ssTest'));

	beforeEach(inject(
		function($rootScope, $controller, $state, _AdminServices_, _ModalService_, $q, $httpBackend) {
			scope = $rootScope.$new();
			ctrl = $controller;
			state = $state;
			AdminServices = _AdminServices_;
			ModalService = _ModalService_;
			deferred = $q.defer();
			httpBackend = $httpBackend;
			$controller('RegisterAdminCtrl', {
				$scope: scope,
				AdminServices: AdminServices,
				ModalService: ModalService
			});

			scope.adminName = 'admin';
			scope.adminEmail = 'email@gmail.com';
			scope.adminPswd = '1234';
			scope.adminPswdConfirm = '1234';

			spyOn(AdminServices, 'addAdmin').and.returnValue(deferred.promise);
			spyOn(AdminServices, 'editAdmin').and.returnValue(deferred.promise);
			spyOn(AdminServices, 'getAdmins').and.returnValue(deferred.promise);
			spyOn(ModalService, 'open').and.callThrough();
			spyOn(ModalService, 'logError').and.callThrough();
			httpBackend.whenGET('langs/lang-uk.json').respond('');
			httpBackend.whenGET('components/modal/modal.html').respond('');
		}
	));

	it('should show an error if password doesn\'t match to confirmPassword', function() {
		scope.adminPswdConfirm = '2345';
		scope.register(true);
		expect(scope.wrong).toEqual(true);
	});

	it('should register an admin if the data is valid, show the success modal window and clean up '
		+ 'input fields', function() {
		deferred.resolve({response: 'ok'});
		scope.register(true);
		scope.$digest();

		expect(AdminServices.addAdmin).toHaveBeenCalled();
		expect(ModalService.open).toHaveBeenCalled();
		expect(ModalService.logError).not.toHaveBeenCalled();

		expect(scope.adminName).toEqual('');
		expect(scope.adminEmail).toEqual('');
		expect(scope.adminPswd).toEqual('');
		expect(scope.adminPswdConfirm).toEqual('');
	});

	it('should edit the admin\'s data if the state is \'edit\', show success modal window and ' +
		'clean up input fields', function() {
		scope.editing = true;
		deferred.resolve({response: 'ok'});
		scope.register(true);
		scope.$digest();

		expect(AdminServices.editAdmin).toHaveBeenCalled();
		expect(ModalService.open).toHaveBeenCalled();
		expect(ModalService.logError).not.toHaveBeenCalled();

		expect(scope.adminName).toEqual('');
		expect(scope.adminEmail).toEqual('');
		expect(scope.adminPswd).toEqual('');
		expect(scope.adminPswdConfirm).toEqual('');
	});

	it('should show an error modal window if server send the error', function() {
		deferred.resolve({});
		scope.register(true);
		scope.$digest();

		expect(AdminServices.addAdmin).toHaveBeenCalled();
		expect(ModalService.open).not.toHaveBeenCalled();
		expect(ModalService.logError).toHaveBeenCalled();

		scope.editing = true;
		scope.register(true);
		scope.$digest();

		expect(AdminServices.editAdmin).toHaveBeenCalled();
		expect(ModalService.open).not.toHaveBeenCalled();
		expect(ModalService.logError).toHaveBeenCalled();
	});

	it('should check password on security and hide \'wrong\' message if confirm password matches ' +
		'to the password', function() {
		scope.$digest();
		expect(scope.strength).toEqual('ssTest.components.addAdmin.strength.weak');

		scope.adminPswd = 'abCD12';
		scope.$digest();
		expect(scope.strength).toEqual('ssTest.components.addAdmin.strength.medium');

		scope.adminPswd = 'abCD12!@34';
		scope.$digest();
		expect(scope.strength).toEqual('ssTest.components.addAdmin.strength.strong');

		scope.wrong = true;
		scope.adminPswdConfirm = 'abCD12!@34';
		scope.$digest();
		expect(scope.wrong).toEqual(false);
	});

	it('should show admin data if we set \'edit\' state', function() {
		deferred.resolve([{username: 'admin2', email: 'email2@gmail.com'}]);
		state.current.name = 'admin.editAdmin';
		ctrl('RegisterAdminCtrl', {
			$scope: scope,
			$state: state,
			AdminServices: AdminServices,
			ModalService: ModalService
		});
		scope.$digest();

		expect(scope.editing).toEqual(true);
		expect(AdminServices.getAdmins).toHaveBeenCalled();
		expect(scope.adminName).toEqual('admin2');
		expect(scope.adminEmail).toEqual('email2@gmail.com');
	});
});
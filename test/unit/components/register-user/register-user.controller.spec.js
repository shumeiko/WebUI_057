describe('Controller: AddUserCtrl', function() {
	var scope;
	var ctrl;
	var state;
	var AdminServices;
	var ModalService;
	var deferred;
	var httpBackend;


	beforeEach(module('ssTest'));

	beforeEach(inject(
		function($rootScope, $controller, $state, _AdminServices_, _ModalService_, $q, $httpBackend) {
			var groupdeferred = $q.defer();
			scope = $rootScope.$new();
			ctrl = $controller;
			state = $state;
			AdminServices = _AdminServices_;
			ModalService = _ModalService_;
			deferred = $q.defer();
			httpBackend = $httpBackend;
			$controller('AddUserCtrl', {
				$scope: scope,
				AdminServices: AdminServices,
				ModalService: ModalService
			});


			scope.userName = 'qwerty';
			scope.userEmail = 'qwerty@gmail.com';
			scope.userPswd = 'qweqweqwe';
			scope.userPswdConfirm = 'qweqweqwe';
			scope.userGradebook = '123123123';
			scope.userSurname = 'Ivan';
			scope.userFirstname = 'Pupkin';
			scope.userMiddlename = 'Pavlo';

			spyOn(AdminServices, 'registerUser').and.returnValue(deferred.promise);
			spyOn(AdminServices, 'getGroups').and.returnValue(deferred.promise);
			spyOn(ModalService, 'open').and.callThrough();
			spyOn(ModalService, 'logError').and.callThrough();
			httpBackend.whenGET('http://dtapi.local/group/getRecords').respond('');
			httpBackend.whenGET('langs/lang-uk.json').respond('');
			httpBackend.whenGET('components/modal/modal.html').respond('');
		}
	));

	it('should show an error if password doesn\'t match to confirmPassword', function() {
		scope.userPswdConfirm = 'qweqwe';
		scope.regUser(true);
		expect(scope.wrong).toEqual(true);
	});

	it('should register user if the data is valid, show the success modal window and clean up '
		+ 'input fields', function() {
		deferred.resolve({response: 'ok'});
		scope.regUser(true);
		scope.$digest();

		expect(AdminServices.registerUser).toHaveBeenCalled();
		expect(ModalService.open).toHaveBeenCalled();
		expect(ModalService.logError).not.toHaveBeenCalled();

		expect(scope.userName).toEqual('');
		expect(scope.userEmail).toEqual('');
		expect(scope.userPswd).toEqual('');
		expect(scope.userPswdConfirm).toEqual('');
		expect(scope.userGradebook).toEqual('');
		expect(scope.userSurname).toEqual('');
		expect(scope.userFirstname).toEqual('');
		expect(scope.userMiddlename).toEqual('');

	});


	it('should show an error modal window if server send the error', function() {
		deferred.resolve({});
		scope.regUser(true);
		scope.$digest();

		expect(AdminServices.registerUser).toHaveBeenCalled();
		expect(ModalService.open).not.toHaveBeenCalled();
		expect(ModalService.logError).toHaveBeenCalled();

	});

	it('should check password on security and hide \'wrong\' message if confirm password matches ' +
		'to the password', function() {
		scope.$digest();
		expect(scope.strength).toEqual('ssTest.components.addAdmin.strength.weak');

		scope.userPswd = 'abCD12';
		scope.$digest();
		expect(scope.strength).toEqual('ssTest.components.addAdmin.strength.medium');

		scope.userPswd = 'abCD12!@34';
		scope.$digest();
		expect(scope.strength).toEqual('ssTest.components.addAdmin.strength.strong');

		scope.wrong = true;
		scope.userPswdConfirm = 'abCD12!@34';
		scope.$digest();
		expect(scope.wrong).toEqual(false);
	});

});
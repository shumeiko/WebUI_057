describe('Controller: AuthorizationCtrl', function() {
	var scope;
	var state;
	var logService;
	var deferred;
	var httpBackend;

	beforeEach(module('ssTest'));

	beforeEach(inject(function($rootScope, $controller, $state, _logService_, $q, $httpBackend) {
		scope = $rootScope.$new();
		state = $state;
		logService = _logService_;
		deferred = $q.defer();
		httpBackend = $httpBackend;
		$controller('AuthorizationCtrl', {
			$scope: scope,
			$state: state,
			logService: logService
		});

		spyOn(logService, 'logIn').and.returnValue(deferred.promise);
		spyOn(state, 'go').and.callThrough();
		httpBackend.expectGET('langs/lang-uk.json').respond('');
		httpBackend.expectGET('http://dtapi.local/login/isLogged').respond('');
	}));

	it('should show error if the data is invalid', function() {
		expect(scope.getError({required: true})).toEqual('ssTest.auth.setData');
		expect(scope.getError({minlength: true})).toEqual('ssTest.auth.minLength');
	});

	it('should check if it\'s an administrator', function() {
		deferred.resolve({response: 'ok', roles: ['role', 'admin']});
		scope.login();
		scope.$digest();

		expect(scope.invalidData).toEqual(false);
		expect(state.go).toHaveBeenCalledWith('admin.home');
	});

	it('should check if it\'s a student', function() {
		deferred.resolve({response: 'ok', roles: ['role', 'student']});
		scope.login();
		scope.$digest();

		expect(scope.invalidData).toEqual(false);
		expect(state.go).toHaveBeenCalledWith('user.home');
	});

	it('should check if it isn\'t a user of our service', function() {
		deferred.resolve({});
		scope.login();
		scope.$digest();

		expect(scope.invalidData).toEqual(true);
		expect(state.go).not.toHaveBeenCalled();
	});
});
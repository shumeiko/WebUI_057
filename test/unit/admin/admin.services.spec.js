describe('Admin service', function() {
	var AdminServices;

	beforeEach(module('ssTest'));

	beforeEach(inject(function(_AdminServices_) {
		AdminServices = _AdminServices_;
	}));

	it('getUniqueIdsList method  test', function() {
		var data = [
			{
				name: "Вася",
				id: 1,
				groupId: 1
			},
			{
				name: "Коля",
				id: 2,
				groupId: 1
			},
			{
				name: "Оля",
				id: 3,
				groupId: 2
			},
			{
				name: "Роман",
				id: 4,
				groupId: 1
			}
		];
		expect(AdminServices.getUniqueIdsList(data, 'groupId')).toEqual([1, 2]);
	})
});

describe('Student Provider', function() {
	var StudentProvider;

	beforeEach(module('ssTest'));

	beforeEach(inject(function(_StudentProvider_) {
		StudentProvider = _StudentProvider_;
	}));

	it('getRating method test', function() {
		var data = {
			1: '20',
			2: '40',
			3: '80',
			4: '20'
		};
		expect(StudentProvider.getRating(data)).toEqual('40');
	})
});
describe('Service: CaseConverter', function() {
	var CaseConverter;

	beforeEach(module('ssTest'));

	beforeEach(inject(function(_CaseConverter_) {
		CaseConverter = _CaseConverter_;
	}));

	it('should transform an input into snake case', function() {
		expect(CaseConverter.toSnake('customCalendar')).toEqual('custom_calendar');
		expect(CaseConverter.toSnake('custom-calendar')).toEqual('custom_calendar');
	});

	it('should transform an input into spinal case', function() {
		expect(CaseConverter.toSpinal('customCalendar')).toEqual('custom-calendar');
		expect(CaseConverter.toSpinal('custom_calendar')).toEqual('custom-calendar');
	});

	it('should transform an input into camel case', function() {
		expect(CaseConverter.toCamel('custom-calendar')).toEqual('customCalendar');
		expect(CaseConverter.toCamel('custom_calendar')).toEqual('customCalendar');
	});
});